﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.DTO.Importancy
{
    public enum ImportancyDTO
    {
        NotApplicable = 0,

        Users = 2,

        Administrators = 4
    }
}
