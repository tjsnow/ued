﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.DTO.Condition;
using TjSnow.DTO.Initiation;
using TjSnow.DTO.MachineImage;
using TjSnow.DTO.MachineVideo;
using TjSnow.DTO.Phase;
using TjSnow.DTO.Status;
using TjSnow.DTO.Voltage;

namespace TjSnow.DTO.Machine
{
    public class NewMachineDTO
    {
        public InitiationDTO? Initiation { get; set; }

        public string Name { get; set; }

        public VoltageDTO? Voltage { get; set; }

        public int? LocationId { get; set; }

        public PhaseDTO? Phase { get; set; }

        public StatusDTO Status { get; set; }

        public ConditionDTO? Condition { get; set; }
        
        public decimal? KVA
        {
            get;
            set;
        }

        public string ThroatDepth
        {
            get;
            set;
        }

        public string Manufacturer
        {
            get;
            set;
        }

        public string SecAmps
        {
            get;
            set;
        }

        public string SecVolts
        {
            get;
            set;
        }

        public string InternalNotes { get; set; }

        public string AirType
        {
            get;
            set;
        }

        public decimal? CylinderDiameter
        {
            get;
            set;
        }

        public int? MaxForce
        {
            get;
            set;
        }

        public decimal? HolderDiameter
        {
            get;
            set;
        }

        public string Control
        {
            get;
            set;
        }

        public string Service
        {
            get;
            set;
        }

        public string PlatenUpper
        {
            get;
            set;
        }

        public string PlatenLower
        {
            get;
            set;
        }

        public string TSlotSpacing
        {
            get;
            set;
        }

        public string TapSwitch
        {
            get;
            set;
        }

        public string CylinderStroke
        {
            get;
            set;
        }

        public string ArmDiaLower
        {
            get;
            set;
        }

        public string ArmDiaUpper
        {
            get;
            set;
        }

        public string HeadType
        {
            get;
            set;
        }

        public string UpperDrive
        {
            get;
            set;
        }

        public string LowerDrive
        {
            get;
            set;
        }

        public string WheelUpper
        {
            get;
            set;
        }

        public string WheelLower
        {
            get;
            set;
        }

        public string Model
        {
            get;
            set;
        }

        public string Serial
        {
            get;
            set;
        }

        public bool? FastShip
        {
            get;
            set;
        }

        public string Transformer
        {
            get;
            set;
        }

        public DateTime? PurchasedDate
        {
            get;
            set;
        }

        public string PurchasedFrom
        {
            get;
            set;
        }

        public decimal? Cost
        {
            get;
            set;
        }

        public string Freight
        {
            get;
            set;
        }

        public string Feautures
        {
            get;
            set;
        }

        public string CheckedBy
        {
            get;
            set;
        }

        public decimal? Price
        {
            get;
            set;
        }

        public string SoldTo
        {
            get;
            set;
        }

        public DateTime? DateSold
        {
            get;
            set;
        }

        public string UEDFilePath
        {
            get;
            set;
        }

        public IEnumerable<ImageFileDTO> ImageFiles { get; set; } 

        public string WebDescription { get; set; }

        public string StockNumber { get; set; }

        public string P21ItemId { get; set; }

        public bool PublishedToWeb { get; set; }

        public int[] Categories { get; set; }

        public string[] Videos { get; set; }

        public bool? ExcludeFromReconciliation { get; set; }
    }
}
