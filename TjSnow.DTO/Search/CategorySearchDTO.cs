﻿namespace TjSnow.DTO.Search
{
    public class CategorySearchDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}