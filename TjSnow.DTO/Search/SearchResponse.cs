﻿using System.Collections.Generic;

namespace TjSnow.DTO.Search
{
    public class SearchResponse
    {
        public SearchRequest SearchRequest { get; set; }
        public int TotalCount { get; set; }
        public List<MachineSearchDTO> Machines { get; set; }
    }
}