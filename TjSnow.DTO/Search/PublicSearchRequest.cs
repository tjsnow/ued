﻿using System.Collections.Generic;
using TjSnow.DTO.Phase;
using TjSnow.DTO.Voltage;
using TjSnow.DTO.Status;

namespace TjSnow.DTO.Search
{
    public class PublicSearchRequest
    {
        public string Keywords { get; set; }

        public IList<VoltageDTO> Voltages { get; set; }
        public int? Category { get; set; }

        public PhaseDTO? Phase { get; set; }
        public double? MinKva { get; set; }
        public double? MaxKva { get; set; }
	public StatusDTO? Status {get; set;}

        public int Skip { get; set; }
        public int Take { get; set; }

        public bool? FastShip { get; set; }

        public PublicSearchRequest()
        {
            this.Voltages = new List<VoltageDTO>();
        }

        public bool Validate(out string errorMessage)
        {
            errorMessage = null;

            if (string.IsNullOrWhiteSpace(Keywords) 
                && !Category.HasValue && !FastShip.HasValue)
            {
                errorMessage = "Please specify at least one search parameter";

                return false;
            }

            return true;
        }
    }
}
