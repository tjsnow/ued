﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TjSnow.DTO.MachineImage;
using TjSnow.DTO.Phase;
using TjSnow.DTO.Voltage;

namespace TjSnow.DTO.Search
{
    public class MachinePublicSearchDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public decimal? KVA { get; set; }
        public VoltageDTO? Voltage { get; set; }
        public PhaseDTO? Phase { get; set; }
        public string VoltageDescription { get; set; }
        public string PhaseDescription { get; set; }

        public string Model { get; set; }
        public string Description { get; set; }
        public string Manufacturer { get; set; }

        public string StockNumber { get; set; }

        public IEnumerable<MachineImageDTO> MachineImages { get; set; }

        public bool? IsFastShip { get; set; }
    }
}