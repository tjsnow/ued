﻿using System.Collections.Generic;

namespace TjSnow.DTO.Search
{
    public class PublicSearchResponse
    {
        public PublicSearchRequest SearchRequest { get; set; }
        public int TotalCount { get; set; }
        public List<MachinePublicSearchDTO> Machines { get; set; }
        public IEnumerable<SearchParamPhaseDTO> Phases { get; set; }
        public IEnumerable<SearchParamVoltageDTO> Voltages { get; set; }
	public IEnumerable<SearchParamStatusDTO> Status { get; set; }
    }
}
