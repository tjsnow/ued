﻿using System.Collections.Generic;
using TjSnow.DTO.Phase;
using TjSnow.DTO.Status;
using TjSnow.DTO.Voltage;

namespace TjSnow.DTO.Search
{
    public class SearchRequest
    {
        public string Keywords { get; set; }
        public IList<VoltageDTO> Voltages { get; set; }
        public PhaseDTO? Phase { get; set; }
        public IList<int> Categories { get; set; }
        public double? MinKva { get; set; }
        public double? MaxKva { get; set; }
        public bool IncludeSold { get; set; }
        public bool IncludeScrapped { get; set; } 
        public bool? PublishedToWeb { get; set; }

        public bool? IsFastShip { get; set; }

        public StatusDTO? Status { get; set; }

        public string OrderBy { get; set; }
        // paging parameters
        public int Skip { get; set; }
        public int Take { get; set; }

        public SearchRequest()
        {
            this.Skip = 0;
            this.Take = int.MaxValue;

            this.Voltages = new List<VoltageDTO>();
            this.Categories = new List<int>();
        }

        public bool Validate(out string errorMessage)
        {
            errorMessage = null;

            //if (string.IsNullOrWhiteSpace(Keywords)
            //        && !Voltage.HasValue
            //            && !Phase.HasValue
            //                && CategoryIds.Count == 0
            //                    && Kva == null
            //                        && !Status.HasValue)
            //{
            //    errorMessage = "Please specify at least one search parameter";

            //    return false;
            //}

            return true;
        }
    }

    public class KVARequestParameters
    {
        public double MinValue { get; private set; }
        public double MaxValue { get; private set; }

        public KVARequestParameters(double minValue, double maxValue)
        {
            this.MinValue = minValue;
            this.MaxValue = maxValue;
        }
    }
}