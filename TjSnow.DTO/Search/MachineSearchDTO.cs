﻿using System.Collections.Generic;
using TjSnow.DTO.Phase;
using TjSnow.DTO.Status;
using TjSnow.DTO.Voltage;

namespace TjSnow.DTO.Search
{
    public class MachineSearchDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public VoltageDTO? Voltage { get; set; }
        public PhaseDTO? Phase { get; set; }
        public StatusDTO Status { get; set; }
        public ICollection<CategorySearchDTO> Categories { get; set; }
        public decimal? KVA { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public string StockNumber { get; set; }
        public string VoltageDescription { get; set; }
        public string PhaseDescription { get; set; }
        public string StatusDescription { get; set; }
        public string SecondaryAmps { get; set; }
        public bool? FastShip { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }

        public bool PublishedToWeb { get; set; }
    }
}