﻿using System.Collections.Generic;

namespace TjSnow.DTO.Search
{
    public class SearchParamsDTO
    {
        public IEnumerable<SearchParamPhaseDTO> Phases { get; set; }
        public IEnumerable<SearchParamVoltageDTO> Voltages { get; set; }
        public IEnumerable<SearchParamCategoryDTO> Categories { get; set; }
        public IEnumerable<SearchParamStatusDTO> Statuses { get; set; }
        public IEnumerable<SearchParamPublishedToWebStatusDTO> PublishedToWebStatuses { get; set; }

        public IEnumerable<SearchParamConditionDTO> Conditions { get; set; }

        public IEnumerable<SearchParamInitiationDTO> Initiations { get; set; }

        public IEnumerable<SearchParamLocationDTO> Locations { get; set; } 
 

    }

    public class SearchParamPhaseDTO
    {
        public string Label { get; set; }
        public int? Value { get; set; } 
    }

    public class SearchParamVoltageDTO
    {
        public string Label { get; set; }
        public int? Value { get; set; } 
    }

    public class SearchParamCategoryDTO
    {
        public string Label { get; set; }
        public int? Value { get; set; }
    }

    public class SearchParamStatusDTO
    {
        public string Label { get; set; }
        public int? Value { get; set; }
    }

    public class SearchParamPublishedToWebStatusDTO
    {
        public string Label { get; set; }
        public bool? Value { get; set; }
    }

    public class SearchParamConditionDTO
    {
        public string Label { get; set; }
        public int? Value { get; set; }
    }

    public class SearchParamInitiationDTO
    {
        public string Label { get; set; }
        public int? Value { get; set; }
    }

    public class SearchParamLocationDTO
    {
        public string Label { get; set; }
        public int? Value { get; set; }
    }
}