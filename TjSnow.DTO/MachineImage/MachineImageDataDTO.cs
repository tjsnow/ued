﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.DTO.MachineImage
{
    public class MachineImageDataDTO
    {
        public byte[] Data { get; set; }

        public string ContentType { get; set; }

        public string FileName { get; set; }

        public bool IsPublished { get; set; }

        public int SortOrder { get; set; }
    }
}
