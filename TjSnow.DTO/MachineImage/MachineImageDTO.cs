﻿namespace TjSnow.DTO.MachineImage
{
    public class MachineImageDTO
    {
        public int Id
        {
            get;
            set;
        }

        public int MachineId
        {
            get;
            set;
        }

        public string Url
        {
            get;
            set;
        }

        public string ContentType
        {
            get;
            set;
        }

        public bool IsPublished { get; set; }

        public int SortOrder { get; set; }

        public string FileName { get; set; }

    }
}
