﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.DTO.MachineImage
{
    public class ImageFileDTO
    {
        public string FileName { get; set; }

        public bool IsPublished { get; set; }

        public int SortOrder { get; set; }
    }
}
