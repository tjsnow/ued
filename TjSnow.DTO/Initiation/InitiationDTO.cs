﻿namespace TjSnow.DTO.Initiation
{
    public enum InitiationDTO
    {
        FootSwitch = 1,

        DualPalmButton = 2,

        FootSwitchAndDualPalmButton = 4
    }
}
