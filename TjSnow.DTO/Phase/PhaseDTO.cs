﻿using System.ComponentModel;

namespace TjSnow.DTO.Phase
{
    public enum PhaseDTO
    {
        [Description("Single")]
        Single = 1,

        [Description("Three")]
        Three = 2
    }
}
