﻿using System.ComponentModel;

namespace TjSnow.DTO.Status
{
    public enum StatusDTO
    {
        [Description("Available")]
        Available = 1,

        [Description("Sold")]
        Sold = 2,

        [Description("Scrapped")]
        Scrapped = 4,

        [Description("Rented")]
        Rented = 8,

        [Description("Consigned")]
        Customer = 16
    }
}
