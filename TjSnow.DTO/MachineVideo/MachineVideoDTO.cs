﻿namespace TjSnow.DTO.MachineVideo
{
    public class MachineVideoDTO
    {
        public int Id
        {
            get;
            set;
        }

        public int MachineId
        {
            get;
            set;
        }

        public string Url
        {
            get;
            set;
        }

        public string ContentType
        {
            get;
            set;
        }

        public string WatchUrl { get; set; }
    }
}
