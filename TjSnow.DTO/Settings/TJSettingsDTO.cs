﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.DTO.Settings
{
    public class TJSettingsDTO
    {
        public string UedMachineBaseFilePath { get; set; }

        public string P21Template { get; set; }
    }
}
