﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.DTO.Email
{
    public class FeedbackRequestDTO
    {
        public string LastName { get; set; }

        public string Company { get; set; }
        
        public string City { get; set; }
       
        public string State { get; set; }
        
        public string Email { get; set; }
        
        public string Phone { get; set; }
       
        public string Message { get; set; }

        public string MachineStockNumber { get; set; }

        public string PageUrl { get; set; }
    }
}
