﻿namespace TjSnow.DTO.Category
{
    public class CategoryShortDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string CategoryImage { get; set; }
    }
}