﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.DTO.Importancy;

namespace TjSnow.DTO.Category
{
    public class CategoryDTO
    {
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public ImportancyDTO Voltage
        {
            get;
            set;
        }

        public ImportancyDTO Phase
        {
            get;
            set;
        }

        public ImportancyDTO KVA
        {
            get;
            set;
        }

        public ImportancyDTO ThroatDepth
        {
            get;
            set;
        }

        public ImportancyDTO Manufacturer
        {
            get;
            set;
        }

        public ImportancyDTO SecAmps
        {
            get;
            set;
        }

        public ImportancyDTO SecVolts
        {
            get;
            set;
        }

        public ImportancyDTO AirType
        {
            get;
            set;
        }

        public ImportancyDTO CylinderDiameter
        {
            get;
            set;
        }

        public ImportancyDTO MaxForce
        {
            get;
            set;
        }

        public ImportancyDTO HolderDiameter
        {
            get;
            set;
        }

        public ImportancyDTO Control
        {
            get;
            set;
        }

        public ImportancyDTO Service
        {
            get;
            set;
        }

        public ImportancyDTO PlatenUpper
        {
            get;
            set;
        }

        public ImportancyDTO PlatenLower
        {
            get;
            set;
        }

        public ImportancyDTO TSlotSpacing
        {
            get;
            set;
        }

        public ImportancyDTO TapSwitch
        {
            get;
            set;
        }

        public ImportancyDTO CylinderStroke
        {
            get;
            set;
        }

        public ImportancyDTO ArmDiaLower
        {
            get;
            set;
        }

        public ImportancyDTO ArmDiaUpper
        {
            get;
            set;
        }

        public ImportancyDTO HeadType
        {
            get;
            set;
        }

        public ImportancyDTO UpperDrive
        {
            get;
            set;
        }

        public ImportancyDTO LowerDrive
        {
            get;
            set;
        }

        public ImportancyDTO WheelUpper
        {
            get;
            set;
        }

        public ImportancyDTO WheelLower
        {
            get;
            set;
        }

        public ImportancyDTO Model
        {
            get;
            set;
        }

        public ImportancyDTO Serial
        {
            get;
            set;
        }

        public ImportancyDTO Initiation
        {
            get;
            set;
        }

        public ImportancyDTO FastShip
        {
            get;
            set;
        }

        public ImportancyDTO Transformer
        {
            get;
            set;
        }

        public ImportancyDTO PurchasedDate
        {
            get;
            set;
        }

        public ImportancyDTO EnteredDate { get; set; }

        public ImportancyDTO PurchasedFrom
        {
            get;
            set;
        }

        public ImportancyDTO Condition
        {
            get;
            set;
        }

        public ImportancyDTO Cost
        {
            get;
            set;
        }

        public ImportancyDTO Freight
        {
            get;
            set;
        }

        public ImportancyDTO Feautures
        {
            get;
            set;
        }

        public ImportancyDTO Location
        {
            get;
            set;
        }

        public ImportancyDTO CheckedBy
        {
            get;
            set;
        }

        public ImportancyDTO Price
        {
            get;
            set;
        }

        public ImportancyDTO SoldTo
        {
            get;
            set;
        }

        public ImportancyDTO DateSold
        {
            get;
            set;
        }

        public ImportancyDTO WebDescription { get; set; }

        public ImportancyDTO UEDFilePath
        {
            get;
            set;
        }

        public ImportancyDTO Photo
        {
            get;
            set;
        }

        public ImportancyDTO Video
        {
            get;
            set;
        }

        public ImportancyDTO StockNumeber { get; set; }

        public ImportancyDTO P21ItemId { get; set; }

        public string CategoryImage { get; set; }
    }
}
