﻿namespace TjSnow.DTO.Condition
{
    public enum ConditionDTO
    {
        New = 1,

        Good = 2,

        Fair = 4,

        Poor = 8
    }
}
