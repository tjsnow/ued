﻿using TjSnow.Model.Entities;

namespace TjSnow.Model.Contracts.Events.MachineAgg
{
    public class MachineCreated : MachineEvent
    {
        public MachineCreated(Machine machine) 
            : base(machine)
        {
        }
    }
}