﻿using TjSnow.Infrastructure.Messaging.Base;
using TjSnow.Model.Entities;

namespace TjSnow.Model.Contracts.Events.MachineAgg
{
    public class MachineDeleted : MachineEvent
    {
        public MachineDeleted(Machine machine) 
            : base(machine)
        {
        }
    }
}