﻿using TjSnow.Model.Entities;

namespace TjSnow.Model.Contracts.Events.MachineAgg
{
    public class MachineUpdated : MachineEvent
    {
        public MachineUpdated(Machine machine) : base(machine)
        {
        }
    }
}