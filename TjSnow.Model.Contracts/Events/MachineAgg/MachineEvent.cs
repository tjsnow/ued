﻿using TjSnow.Infrastructure.Messaging.Base;
using TjSnow.Model.Entities;

namespace TjSnow.Model.Contracts.Events.MachineAgg
{
    abstract public class MachineEvent : IEvent
    {
        public string SourceId { get; set; }
        public Machine Machine { get; private set; }

        protected MachineEvent(Machine machine)
        {
            Machine = machine;
        }
    }
}