﻿namespace TjSnow.Infrastructure.Logging.Base
{
    public class LoggerFactory
    {
        #region Members

        static ILoggerFactory _currentLogFactory;

        #endregion

        #region Public Methods

        /// <summary>
        /// Set the log factory to use
        /// </summary>
        /// <param name="logFactory">Log factory to use</param>
        public static void SetCurrent(ILoggerFactory logFactory)
        {
            _currentLogFactory = logFactory;
        }

        /// <summary>
        /// Creates a new ILogger
        /// </summary>
        /// <returns>Created ILogger</returns>
        public static ILogger CreateLog()
        {
            return (_currentLogFactory != null) ? _currentLogFactory.Create() : null;
        }

        #endregion
    }
}
