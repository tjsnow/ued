﻿namespace TjSnow.Infrastructure.Logging.NLog
{
    using Base;

    public class OnPremisesNLogFactory : ILoggerFactory
    {
        #region Public methods

        public ILogger Create()
        {
            return new OnPremisesNLogLogger();
        }

        #endregion
    }
}
