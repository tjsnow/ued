﻿using System;
using NLog;
using NLog.Config;
using NLog.Extensions.AzureTableStorage;
using TjSnow.Infrastructure.Logging.Base;

namespace TjSnow.Infrastructure.Logging.NLog
{
    public class AzureNLogLogger : ILogger
    {
        #region Fields and properties

        private readonly Logger _logger;

        #endregion

        #region Contructor

        public AzureNLogLogger()
        {
            var config = new LoggingConfiguration();

            var azureTarget = new AzureTableStorageTarget();

            config.AddTarget("AzureStorageTable", azureTarget);

            azureTarget.TableName = "ApplicationLogs";
            azureTarget.ConnectionStringKey = "LogConnection";

            var configuredRule = LogManager.Configuration.LoggingRules[0];
            configuredRule.Targets.Clear();
            configuredRule.Targets.Add(azureTarget);

            config.LoggingRules.Add(configuredRule);

            LogManager.Configuration = config;

            _logger = LogManager.GetLogger("AzureLogger");
        } 

        #endregion

        #region Public methods

        public void LogInfo(string message, params object[] args)
        {
            _logger.Info(message, args);
        }

        public void LogWarning(string message, params object[] args)
        {
            _logger.Warn(message, args);
        }

        public void LogError(string message, params object[] args)
        {
            _logger.Error(message, args);
        }

        public void LogError(string message, Exception exception, params object[] args)
        {
            _logger.Error(string.Format(message, args), exception);
        }

        public void Debug(string message, params object[] args)
        {
            _logger.Debug(message, args);
        }

        #endregion
    }
}