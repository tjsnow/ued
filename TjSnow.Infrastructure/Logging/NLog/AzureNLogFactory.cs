﻿using TjSnow.Infrastructure.Logging.Base;

namespace TjSnow.Infrastructure.Logging.NLog
{
    public class AzureNLogFactory : ILoggerFactory
    {
        #region Public methods

        public ILogger Create()
        {
            return new AzureNLogLogger();
        }

        #endregion
    }
}