﻿namespace TjSnow.Infrastructure.Logging.NLog
{
    using System;
    using Base;
    using global::NLog;

    class OnPremisesNLogLogger : ILogger
    {
        #region Fields and properties

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Public methods

        public void LogInfo(string message, params object[] args)
        {
            _logger.Info(message, args);
        }

        public void LogWarning(string message, params object[] args)
        {
            _logger.Warn(message, args);
        }

        public void LogError(string message, params object[] args)
        {
            _logger.Error(message, args);
        }

        public void LogError(string message, Exception exception, params object[] args)
        {
            _logger.Error(string.Format(message, args), exception);
        }

        public void Debug(string message, params object[] args)
        {
            _logger.Debug(message, args);
        }

        #endregion
    }
}
