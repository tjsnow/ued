﻿using System.Collections.Generic;
using System.Linq;

namespace TjSnow.Infrastructure.Messaging.Base
{
    /// <summary>
    /// Provides usability overloads for <see cref="IEventBus"/>
    /// </summary>
    public static class EventBusExtensions
    {
        public static void Publish(this IEventBus bus, IEvent @event)
        {
            bus.Publish(new Envelope<IEvent>(@event));
        }

        //public static void PublishAsync(this IEventBus bus, IEvent @event)
        //{
        //    Action action = () => bus.Publish(new Envelope<IEvent>(@event));
        //    action.BeginInvoke(action.EndInvoke, null);
        //}

        public static void Publish(this IEventBus bus, IEnumerable<IEvent> events)
        {
            bus.Publish(events.Select(x => new Envelope<IEvent>(x)));
        }
    }
}