﻿namespace TjSnow.Infrastructure.Messaging.Base.Handling
{
    public interface IEventHandlerRegistry
    {
        void Register(IEventHandler handler);
    }
}