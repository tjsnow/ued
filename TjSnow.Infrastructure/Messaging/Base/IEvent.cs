﻿namespace TjSnow.Infrastructure.Messaging.Base
{
    /// <summary>
    /// Represents an event message.
    /// </summary>
    public interface IEvent
    {
        /// <summary>
        /// The identifier of the source originating the event.
        /// </summary>
        string SourceId { get; set; }
    }
}