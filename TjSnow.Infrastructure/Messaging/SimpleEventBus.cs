﻿using System.Collections.Generic;
using System.Linq;
using TjSnow.Infrastructure.Messaging.Base;
using TjSnow.Infrastructure.Messaging.Base.Handling;
using TjSnow.Infrastructure.Messaging.Handling;

namespace TjSnow.Infrastructure.Messaging
{
    public class SimpleEventBus : IEventBus, IEventHandlerRegistry
    {
        #region Fields and properties

        private readonly EventDispatcher _eventDispatcher; 

        #endregion

        #region Constructor

        public SimpleEventBus()
        {
            _eventDispatcher = new EventDispatcher();
        } 

        #endregion

        #region Public methods

        public void Publish(Envelope<IEvent> @event)
        {
            _eventDispatcher.DispatchMessage(@event.Body);
        }

        public void Publish(IEnumerable<Envelope<IEvent>> events)
        {
            _eventDispatcher.DispatchMessages(events.Select(evt => evt.Body));
        }

        public void Register(IEventHandler handler)
        {
            _eventDispatcher.Register(handler);
        }

        public void RegisterAll(IEnumerable<IEventHandler> handlers)
        {
            foreach (var handler in handlers)
            {
                _eventDispatcher.Register(handler);
            }
        } 

        #endregion
    }
}