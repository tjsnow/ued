﻿using System;
using System.Configuration;

namespace TjSnow.Infrastructure.Environment
{
    public class ConfigurationSettings
    {
        // helper to extract a web config value
        public static T Get<T>(string key)
        {
            // try to convert
            try
            {
                return (T)Convert.ChangeType(ConfigurationManager.AppSettings[key], typeof(T));
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }
}