﻿using TjSnow.Application.Exceptions;
using TjSnow.Model;

namespace TjSnow.Application.Extensions
{
    public static class EntityExtensions
    {
        public static T ValidateFound<T>(this T entity, int id) where T : class, IEntity
        {
            if (entity == null)
                throw ExceptionHelper.CreateNotFoundException(id, typeof(T));

            return entity;
        }
    }
}
