﻿using TjSnow.DTO.Email;
using TjSnow.DTO.Machine;

namespace TjSnow.Application.Services.Email
{
    using Microsoft.AspNet.Identity;
    using System.Threading.Tasks;

    public interface IEmailService : IIdentityMessageService {
        Task SendFeedback(FeedbackRequestDTO feedbackRequest);

        Task SharePage(string @from, string email, MachineDTO machine, string additionalInfo, string baseUrl);

        Task SendFeedbackConfiramation(FeedbackRequestDTO feedbackRequest);
    }
}
