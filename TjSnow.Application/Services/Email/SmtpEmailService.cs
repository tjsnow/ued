﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using TjSnow.Application.Extensions;
using TjSnow.DTO.Email;
using TjSnow.DTO.Machine;
using TjSnow.Persistence.Repositories.Machine;
using TjSnow.Infrastructure.Utils;

namespace TjSnow.Application.Services.Email
{
    using System;
    using System.ComponentModel;
    using System.Net;
    using System.Net.Mail;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;
    using Infrastructure.Logging.Base;
    using System.Web.Configuration;

    public class SmtpEmailService : IEmailService
    {
        #region Constants

        private const string MessageEmailSendCanceled = "Email send canceled.";
        private const string MessageEmailIsNotSent = "Email is not sent.";
        private const string MessageEmailSent = "Email sent.";

        #endregion

        #region Fields and properties

        private static readonly ILogger Logger = LoggerFactory.CreateLog();

        private readonly SmtpClient _client;
        private readonly String _fromAddress;

        #endregion

        #region Constructor

        public SmtpEmailService(
            string host,
            int port,
            bool enableSsl,
            string userName,
            string password,
            string fromAddress)
        {
            _client = new SmtpClient(host, port)
            {
                Credentials = new NetworkCredential(userName, password),
                EnableSsl = enableSsl
            };

            _client.SendCompleted += SmtpClientOnSendCompleted;


	    //Old address. Change back if problems
            //_fromAddress = fromAddress;
	    

	    //New address to send from tjsnow
	    _fromAddress = "welders@tjsnow.com";

            
        }

        #endregion

        #region Public methods

        public Task SendAsync(IdentityMessage message)
        {
            var mail = new MailMessage(_fromAddress, message.Destination, message.Subject, message.Body)
            {
                IsBodyHtml = true
            };

            return _client.SendMailAsync(mail);
        }


        public Task SendFeedback(FeedbackRequestDTO feedbackRequest)
        {
            var body = new StringBuilder();

            body.Append("<html><body>");
            body.AppendFormat("Name: {0}<br>", feedbackRequest.LastName);
            body.AppendFormat("Company: {0}<br>", feedbackRequest.Company);
            body.AppendFormat("Phone: {0}<br>", feedbackRequest.Phone);
            body.AppendFormat("Email: {0}<br>", feedbackRequest.Email);
            body.AppendFormat("Location: {0}, {1}<br>", feedbackRequest.City, feedbackRequest.State);
            body.AppendFormat("Stock #: {0}<br>", feedbackRequest.MachineStockNumber);
            body.AppendFormat("Message: {0}<br>", feedbackRequest.Message);
            body.AppendFormat("Url: <a href=\"{0}\">{0}</a>", feedbackRequest.PageUrl);
            body.Append("</body></html>");

            var destinations = WebConfigurationManager.AppSettings["RequestInfoEmailAddress"].Split(new[] {';'},StringSplitOptions.RemoveEmptyEntries).ToArray();
            
            return SendManyRecipientsAsync(feedbackRequest.Email,string.Format("TJS Used Equipment Website RFQ {0: MM/dd/yyyy HH:mm}", DateTime.Now), body.ToString(),destinations);
        }

        public Task SharePage(string @from, string email, MachineDTO machine, string additionalInfo, string baseUrl)
        {
            var subject = string.Format("TJ Snow: {0} - {1}", machine.StockNumber, machine.Name);

            var body = new StringBuilder("<html><body>");

            body.Append(string.Format("<p>{0} has shared this machine with you.</p>", @from));
            
            if (!string.IsNullOrWhiteSpace(additionalInfo))
            {
                body.AppendFormat("<p>Message: {0} </p><br>", additionalInfo);
            }

            if (!string.IsNullOrWhiteSpace(machine.StockNumber))
            {
                body.Append(string.Format("Stock #: {0}", machine.StockNumber)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.Name))
            {
                body.Append(string.Format("Name: {0}", machine.Name)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.WebDescription))
            {
                body.Append(string.Format("Description: {0}", machine.WebDescription)).Append("<br>");
            }

            if (machine.KVA.HasValue)
            {
                body.Append(string.Format("KVA: {0:G29}", machine.KVA.Value)).Append("<br>");
            }

            if (machine.Voltage.HasValue)
            {
                body.Append(string.Format("Voltage: {0}", machine.Voltage.Value.GetEnumDescription())).Append("<br>");
            }

            if (machine.Phase.HasValue)
            {
                body.Append(string.Format("Phase: {0}", machine.Phase.Value.GetEnumDescription())).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.ThroatDepth))
            {
                body.Append(string.Format("Throat Depth: {0} in", machine.ThroatDepth)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.SecAmps))
            {
                body.Append(string.Format("Secondary Amps: {0}", machine.SecAmps)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.SecVolts))
            {
                body.Append(string.Format("Secondary Volts: {0}", machine.SecVolts)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.Manufacturer))
            {
                body.Append(string.Format("Manufacturer: {0}", machine.Manufacturer)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.Model))
            {
                body.Append(string.Format("Model: {0}", machine.Model)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.Serial))
            {
                body.Append(string.Format("Serial: {0}", machine.Serial)).Append("<br>");
            }

            if (machine.Initiation.HasValue)
            {
                body.Append(string.Format("Initiation: {0}", machine.Initiation.Value.GetEnumDescription())).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.AirType))
            {
                body.Append(string.Format("Air Type: {0}", machine.AirType)).Append("<br>");
            }

            if (machine.CylinderDiameter.HasValue)
            {
                body.Append(string.Format("Cylinder Diameter: {0:G29} in", machine.CylinderDiameter.Value)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.CylinderStroke))
            {
                body.Append(string.Format("Cylinder Stroke: {0} in", machine.CylinderStroke)).Append("<br>");
            }

            if (machine.MaxForce.HasValue)
            {
                body.Append(string.Format("Max Force: {0} lbf", machine.MaxForce.Value)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.TapSwitch))
            {
                body.Append(string.Format("Tap Switch: {0}", machine.TapSwitch)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.ArmDiaLower))
            {
                body.Append(string.Format("Arm Diameter Lower: {0} in", machine.ArmDiaLower)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.ArmDiaUpper))
            {
                body.Append(string.Format("Arm Diameter Upper: {0} in", machine.ArmDiaUpper)).Append("<br>");
            }

            if (machine.HolderDiameter.HasValue)
            {
                body.Append(string.Format("Holder Diameter: {0:G29} in", machine.HolderDiameter.Value)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.PlatenUpper))
            {
                body.Append(string.Format("Platen Upper: {0} in", machine.PlatenUpper)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.PlatenLower))
            {
                body.Append(string.Format("Platen Lower: {0} in", machine.PlatenLower)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.TSlotSpacing))
            {
                body.Append(string.Format("T-slot spacing: {0}", machine.TSlotSpacing)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.Control))
            {
                body.Append(string.Format("Control: {0}", machine.Control)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.Service))
            {
                body.Append(string.Format("Service: {0}", machine.Service)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.HeadType))
            {
                body.Append(string.Format("Head type: {0}", machine.HeadType)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.UpperDrive))
            {
                body.Append(string.Format("Upper drive: {0}", machine.UpperDrive)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.LowerDrive))
            {
                body.Append(string.Format("Lower drive: {0}", machine.LowerDrive)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.WheelUpper))
            {
                body.Append(string.Format("Wheel Upper: {0} in DIA", machine.WheelUpper)).Append("<br>");
            }

            if (!string.IsNullOrWhiteSpace(machine.WheelLower))
            {
                body.Append(string.Format("Wheel lower: {0} in DIA", machine.WheelLower)).Append("<br>");
            }

            body.Append(string.Format("<a href=\"{0}/Machines/Index/{1}\">{0}/Machines/Index/{1}</a>", baseUrl, machine.Id));

            body.Append("</body></hmtl>");

            IdentityMessage mail = new IdentityMessage();

            mail.Body = body.ToString();
            mail.Destination = email;
            mail.Subject = subject;
            
            return SendAsync(mail);
        }

        public Task SendFeedbackConfiramation(FeedbackRequestDTO feedbackRequest)
        {
            var body = new StringBuilder();

            body.Append("<html><body>");
            body.Append("<p>Dear Valued Customer,</p>");
            body.Append("<p>Thank you for contacting us about your resistance welding requirements. Our goal is to be in contact with you within a business day.</p>");
            body.Append("<p>If you need immediate assistance, please contact a T. J. Snow Applications Engineering Department at 1-800-669-7669.</p>");
            body.Append("<p>Sincerely,<br>Applications Engineering</p>");
            body.Append("<p><a href=\"http://www.tjsnow.com\">www.tjsnow.com</a></p>");
            body.Append("<p>Your Message:</p>");

            body.AppendFormat("Name: {0}<br>", feedbackRequest.LastName);
            body.AppendFormat("Company: {0}<br>", feedbackRequest.Company);
            body.AppendFormat("Phone: {0}<br>", feedbackRequest.Phone);
            body.AppendFormat("Email: {0}<br>", feedbackRequest.Email);
            body.AppendFormat("Location: {0}, {1}<br>", feedbackRequest.City, feedbackRequest.State);
            body.AppendFormat("Stock #: {0}<br>", feedbackRequest.MachineStockNumber);
            body.AppendFormat("Message: {0}<br>", feedbackRequest.Message);
            body.AppendFormat("Url: <a href=\"{0}\">{0}</a>", feedbackRequest.PageUrl);
            body.Append("</body></html>");

            IdentityMessage mail = new IdentityMessage
            {
                Body = body.ToString(),
                Destination = feedbackRequest.Email,
                Subject = "T. J. Snow equipment inquiry"
            };

            return this.SendAsync(mail);
        }

        #endregion

        #region Private methods

        private static void SmtpClientOnSendCompleted(object sender, AsyncCompletedEventArgs asyncCompletedEventArgs)
        {
            if (asyncCompletedEventArgs.Cancelled)
            {
                Logger.LogWarning(MessageEmailSendCanceled);
            }
            if (asyncCompletedEventArgs.Error != null)
            {
                Logger.LogError(MessageEmailIsNotSent, asyncCompletedEventArgs.Error);
            }
            else
            {
                Logger.LogInfo(MessageEmailSent);
            }
        }

        private Task SendAsync(IdentityMessage message,string fromAddress)
        {
            var mail = new MailMessage(fromAddress, message.Destination, message.Subject, message.Body)
            {
                IsBodyHtml = true
            };

            return _client.SendMailAsync(mail);
        }

        private Task SendManyRecipientsAsync(string from, string subject, string body, string[] recipients)
        {
            var mail = new MailMessage();

            mail.Subject = subject;
            mail.From = new MailAddress(from);
            mail.Body = body;
            mail.IsBodyHtml = true;

            foreach (var recipient in recipients)
            {
                mail.To.Add(recipient);
            }

            return _client.SendMailAsync(mail);
        }

        #endregion
    }
}
