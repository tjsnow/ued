﻿using TjSnow.DTO.Email;
using TjSnow.DTO.Machine;

namespace TjSnow.Application.Services.Email
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;

    public class DummyEmailService : IEmailService
    {
        private static readonly string _path;

        static DummyEmailService()
        {
            _path = Path.Combine(Path.GetPathRoot(Environment.SystemDirectory), "Mail\\");

            if (!Directory.Exists(_path))
                Directory.CreateDirectory(_path);
        }

        private void Send(IdentityMessage message)
        {
            using (var outfile = new StreamWriter(_path + DateTime.Now.Ticks))
            {
                outfile.WriteLine(message.Destination);
                outfile.WriteLine(message.Subject);
                outfile.WriteLine(message.Body);
            }
        }

        public Task SendAsync(IdentityMessage message)
        {
            return Task.Run(() => Send(message));
        }

        public Task SendFeedback(string message)
        {
            throw new NotImplementedException();
        }

        public Task SharePage(string @from, string email, MachineDTO machine, string additionalInfo, string baseUrl)
        {
            throw new NotImplementedException();
        }

        public Task SendFeedbackConfiramation(FeedbackRequestDTO feedbackRequest)
        {
            throw new NotImplementedException();
        }

        public Task SendFeedback(DTO.Email.FeedbackRequestDTO feedbackRequest)
        {
            throw new NotImplementedException();
        }
    }
}
