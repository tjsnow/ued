﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzureSearchClient;
using TjSnow.Application.Exceptions;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.MachineImage;
using TjSnow.DTO.Search;
using TjSnow.Infrastructure.Async;
using TjSnow.Model.Entities;
using TjSnow.Model.Specifications;
using TjSnow.Persistence.Repositories.Machine;
using TjSnow.Persistence.UnitOfWork.Base;
using MachineEntity = TjSnow.Model.Entities.Machine;

namespace TjSnow.Application.Services.Search
{
    public class SearchService : ISearchService
    {
        private const string IndexNameMachine = "machines";

        internal const string FieldId = "Id";
        internal const string FieldDescription = "description";
        internal const string FieldThumbnail = "thumbnail";
        internal const string FieldName = "name";
        internal const string FieldVoltage = "voltage";
        internal const string FieldPhase = "phase";
        internal const string FieldCategory = "category";
        internal const string FieldKva = "kva";
        internal const string FieldStatus = "status";
        internal const string FieldPrice = "price";
        internal const string FieldStockNumber = "stocknumber";
        internal const string FieldPublishedToWeb = "publishedtoweb";
        internal const string FieldSecondaryAmps = "secondaryamps";
        internal const string FieldFastShip = "fastship";
        internal const string FieldManufacturer = "manufacturer";
        internal const string FieldModel = "model";
        internal const string FieldMachineVoltage = "machinevoltage";

        private readonly Service _azureSearchService;
        private readonly IMachineRepository _machineRepository;
        private readonly IObjectMapper _objectMapper;
        private readonly IUnitOfWork _unitOfWork;

        public SearchService(
            IMachineRepository machineRepository,
            IObjectMapper objectMapper,
            IUnitOfWork unitOfWork)
        {
            _machineRepository = machineRepository;
            _objectMapper = objectMapper;
            _unitOfWork = unitOfWork;

            _azureSearchService = new Service(
                Infrastructure.Environment.ConfigurationSettings.Get<string>("SearchServiceName"),
                Infrastructure.Environment.ConfigurationSettings.Get<string>("SearchServiceQueryKey"),
                Infrastructure.Environment.ConfigurationSettings.Get<string>("SearchServiceManagementKey"))
            {
                Settings = { Version = "2015-02-28" }
            };
        }

        public SearchResponse  Search(SearchRequest request)
        {
            if (request == null)
            {
                throw new BadRequestException("Please specify at least one parameter");
            }

            //is our request valid?
            string errorMessage;

            if (!request.Validate(out errorMessage))
            {
                throw new BadRequestException(errorMessage);
            }

            var result = AsyncHelper.RunSync(() => _azureSearchService.Indexes[IndexNameMachine].QueryAsync<MachineDocument>(BuildQueryParameters(request)));

            var machines = new List<MachineSearchDTO>(result.Documents.Select(elem => _objectMapper.Map<MachineDocument, MachineSearchDTO>(elem)));
            
            return new SearchResponse
            {
                SearchRequest = request,
                TotalCount = result.TotalCount,
                Machines = machines
            };
        }

        public PublicSearchResponse PublicSearch(PublicSearchRequest request)
        {
            if (request == null)
            {
                throw new BadRequestException("Please specify at least one parameter");
            }

            //is our request valid?
            string errorMessage;

            if (!request.Validate(out errorMessage))
            {
                throw new BadRequestException(errorMessage);
            }

            var result = AsyncHelper.RunSync(() => _azureSearchService.Indexes[IndexNameMachine].QueryAsync<MachineDocument>(BuildPublicQueryParameters(request)));

            var machines = new List<MachinePublicSearchDTO>(result.Documents.Select(elem => _objectMapper.Map<MachineDocument, MachinePublicSearchDTO>(elem)));
          
            foreach (var machine in machines)
            {
                var images = _unitOfWork.MachineImageRepository.Find(MachineImageSpecifications.PublishedByMachineId(machine.Id))
                                                               .OrderBy(image=>image.SortOrder)
                                                               .ToList();

                machine.MachineImages = images.Select(image => _objectMapper.Map<MachineImage, MachineImageDTO>(image));
            }

            return new PublicSearchResponse
            {
                SearchRequest = request,
                TotalCount = result.TotalCount,
                Machines = machines
            };
        }

        public void Add(MachineEntity machine)
        {
            var result = AsyncHelper.RunSync(() => _azureSearchService.Indexes[IndexNameMachine].CreateDocumentAsync(new MachineDocument(machine)));

            if (!result)
            {
                ExceptionHelper.CreateAppException("Failed to populate data to Search service");
            }
        }

        public void Add(IEnumerable<MachineEntity> machines)
        {
            var result = AsyncHelper.RunSync(() => _azureSearchService.Indexes[IndexNameMachine].CreateDocumentsAsync(machines.Select(elem => new MachineDocument(elem)).ToList()));

            if (!result)
            {
                ExceptionHelper.CreateAppException("Failed to populate data to Search service");
            }
        }

        public void Update(MachineEntity machine)
        {
            var result = AsyncHelper.RunSync(() => _azureSearchService.Indexes[IndexNameMachine].UpdateDocumentAsync(new MachineDocument(machine)));

            if (!result)
            {
                ExceptionHelper.CreateAppException("Failed to update data in Search service");
            }
        }

        public void Delete(MachineEntity machine)
        {
            var result = AsyncHelper.RunSync(() => _azureSearchService.Indexes[IndexNameMachine].DeleteDocumentAsync(new MachineDocument(machine)));

            if (!result)
            {
                ExceptionHelper.CreateAppException("Failed to delete data in Search service");
            }
        }

        public async Task EnsureSearchConfigured()
        {
            await LoadIndexes();

            if (_azureSearchService.Indexes.FirstOrDefault(index => index.Name == IndexNameMachine) == null)
            {
                await CreateMachineIndex();

                PopulateData();
            }
        }

        public void Drop()
        {
            AsyncHelper.RunSync(LoadIndexes);

            if (_azureSearchService.Indexes.FirstOrDefault(index => index.Name == IndexNameMachine) != null)
            {
                var result = AsyncHelper.RunSync(() => _azureSearchService.Indexes.DeleteIndexAsync(IndexNameMachine));

                if (!result)
                {
                    ExceptionHelper.CreateAppException("Failed to drop index in Search service");
                }
            }
        }

        internal static IEnumerable<Field> GetSearchableFields()
        {
            return new List<Field>
            {
                new Field { Name = FieldId, Type = FieldDataTypes.String, IsKey = true, IsSearchable = false },
                new Field { Name = FieldThumbnail, Type = FieldDataTypes.String, IsSearchable = false },
                new Field { Name = FieldName, Type = FieldDataTypes.String },
                new Field { Name = FieldVoltage, Type = FieldDataTypes.StringCollection, IsSearchable = false },
                new Field { Name = FieldPhase, Type = FieldDataTypes.Int32, IsSearchable = false },
                new Field { Name = FieldDescription, Type = FieldDataTypes.String },
                new Field { Name = FieldCategory, Type = FieldDataTypes.StringCollection, IsSearchable = false },
                new Field { Name = FieldKva, Type = FieldDataTypes.Double, IsSearchable = false },
                new Field { Name = FieldStatus, Type = FieldDataTypes.Int32, IsSearchable = false },
                new Field { Name = FieldPrice, Type = FieldDataTypes.Double, IsSearchable = false },
                new Field { Name = FieldStockNumber, Type = FieldDataTypes.String },
                new Field { Name = FieldPublishedToWeb, Type = FieldDataTypes.Boolean, IsSearchable = false },
                new Field { Name = FieldSecondaryAmps, Type = FieldDataTypes.String, IsSearchable = false },
                new Field { Name = FieldFastShip, Type = FieldDataTypes.Boolean, IsSearchable = false },
                new Field { Name = FieldManufacturer, Type = FieldDataTypes.String },
                new Field { Name = FieldModel, Type = FieldDataTypes.String },
                new Field { Name = FieldMachineVoltage,Type = FieldDataTypes.Int32,IsSearchable = false}
            };
        }

        private async Task LoadIndexes()
        {
            await _azureSearchService.LoadIndexesAsync();
        }

        private async Task CreateMachineIndex()
        {
            var searchableFields = GetSearchableFields();
            var machineIndex = new Index(searchableFields, IndexNameMachine)
            {
                CorsOptions = new CorsOptions { AllowedOrigins = new[] { "*" } }
            };

            await _azureSearchService.Indexes.CreateIndexAsync(machineIndex);
        }

        /// <summary>
        /// Responsible for reloading all machine data after the index has been rebuilt. 
        /// </summary>
        private void PopulateData()
   {
            var machines = _machineRepository.RetrieveMachines().ToList();
            if (machines.Count > 0)
            {
                //azure only allows up to 1,000 items to be indexed at a time. 
                int maxItemsPerCall = 1000;
                int takeSize = maxItemsPerCall;
                int iteration = 1;
                IEnumerable<MachineEntity> machinesToIndex = null; 
                if (machines.Count < takeSize)
                {
                    //if there are machines to index, get up to the first maxItemsPerCall. 
                    takeSize = machines.Count; 
                }
                machinesToIndex = machines.Take(takeSize);
                 
                while (machinesToIndex != null)
                {
                    Add(machinesToIndex);

                    //are there more? If so, take up to the next maxItemsPerCall machines to index. 
                    if (machines.Count > (iteration * maxItemsPerCall))
                    {
                        takeSize = machines.Count - (iteration * maxItemsPerCall);
                        if (takeSize > maxItemsPerCall)
                        {
                            takeSize = maxItemsPerCall;
                        }
                        machinesToIndex = machines.Skip(iteration * maxItemsPerCall).Take(takeSize);
                        iteration++; 
                    }
                    else
                    {
                        //all done!
                        machinesToIndex = null;
                    }
                    
                }
            }
        }

        private static QueryParameters BuildQueryParameters(SearchRequest request)
        {
            var queryParameters = new QueryParameters
            {
                Count = true,
                Skip = request.Skip,
                Top = request.Take,
                QueryText = request.Keywords,
                TextMode = TextModeTypes.Any
            };

            if (!string.IsNullOrWhiteSpace(request.OrderBy))
            {
                if (request.OrderBy.ToLower() == FieldKva)
                {
                    queryParameters.OrderBy = new[] {new FieldOrderByItem{FieldName = FieldKva}};
                }

                if (request.OrderBy.ToLower() == FieldVoltage)
                {
                    queryParameters.OrderBy = new[] {new FieldOrderByItem {FieldName = FieldMachineVoltage}};
                }
            }

            var filterBuilder = new StringBuilder();

            if (request.Voltages.Count > 0)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }

                filterBuilder.Append("(");

                for (var i = 0; i < request.Voltages.Count; i++)
                {
                    filterBuilder.Append(FieldVoltage + "/any(volt:+");
                    filterBuilder.Append("volt+eq+'" + ((int)request.Voltages[i]).ToString(CultureInfo.InvariantCulture) + "'");
                    filterBuilder.Append(")");

                    if (i + 1 < request.Voltages.Count)
                    {
                        filterBuilder.Append("+or+");
                    }
                }

                filterBuilder.Append(")");
            }

            if (request.Phase.HasValue)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }
                filterBuilder.Append(FieldPhase + "+eq+" + (int)request.Phase);
            }

            if (request.Categories.Count > 0)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }

                filterBuilder.Append("(");

                for (var i = 0; i < request.Categories.Count; i++)
                {
                    filterBuilder.Append(FieldCategory + "/any(cat:+");
                    filterBuilder.Append("cat+eq+'" + request.Categories[i] + "'");
                    filterBuilder.Append(")");

                    if (i + 1 < request.Categories.Count)
                    {
                        filterBuilder.Append("+or+");
                    }
                }

                filterBuilder.Append(")");
            }

            if (request.MinKva.HasValue)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }

                filterBuilder.Append(FieldKva + "+ge+" + request.MinKva.Value);
            }

            if (request.MaxKva.HasValue)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }

                filterBuilder.Append(FieldKva + "+le+" + request.MaxKva.Value);
            }

            if (request.IncludeScrapped && request.IncludeSold)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }

                filterBuilder.Append("(" + FieldStatus + "+eq+" + (int) Status.Scrapped + "+or+" + FieldStatus + "+eq+" +(int) Status.Sold + ")");
            }
            else
            {
                if (!request.IncludeScrapped)
                {
                    if (filterBuilder.Length > 0)
                    {
                        filterBuilder.Append("+and+");
                    }

                    filterBuilder.Append(FieldStatus + "+ne+" + (int)Status.Scrapped);
                }
                else
                {
                    if (filterBuilder.Length > 0)
                    {
                        filterBuilder.Append("+and+");

                    }

                    filterBuilder.Append(FieldStatus + "+eq+" + (int)Status.Scrapped);
                }

                if (!request.IncludeSold)
                {
                    if (filterBuilder.Length > 0)
                    {
                        filterBuilder.Append("+and+");
                    }

                    filterBuilder.Append(FieldStatus + "+ne+" + (int)Status.Sold);
                }
                else
                {
                    if (filterBuilder.Length > 0)
                    {
                        filterBuilder.Append("+and+");
                    }

                    filterBuilder.Append(FieldStatus + "+eq+" + (int)Status.Sold);
                }
            }

            if (request.Status.HasValue && !request.IncludeScrapped && !request.IncludeSold)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }

                filterBuilder.Append(FieldStatus + "+eq+" + (int) request.Status.Value);
            }

            if (request.PublishedToWeb.HasValue)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }

                filterBuilder.Append(FieldPublishedToWeb + "+eq+" + request.PublishedToWeb.Value.ToString().ToLower());
            }

            if (request.IsFastShip.HasValue && request.IsFastShip.Value)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }

                filterBuilder.Append(FieldFastShip + "+eq+true");
            }

            queryParameters.Filter = filterBuilder.ToString();

            return queryParameters;
        }

        private static QueryParameters BuildPublicQueryParameters(PublicSearchRequest request)
        {
            var queryParameters = new QueryParameters
            {
                Count = true,
                Skip = request.Skip,
                Top = request.Take,
                QueryText = request.Keywords,
                TextMode = TextModeTypes.Any,
                OrderBy = new []
                {
                    new FieldOrderByItem{FieldName = FieldKva,OrderByMode = OrderByModeTypes.Ascending},
                    new FieldOrderByItem{FieldName = FieldStockNumber,OrderByMode = OrderByModeTypes.Ascending}
                }
            };

            var filterBuilder = new StringBuilder();

            if (request.Voltages.Count > 0)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }

                filterBuilder.Append("(");

                for (var i = 0; i < request.Voltages.Count; i++)
                {
                    filterBuilder.Append(FieldVoltage + "/any(volt:+");
                    filterBuilder.Append("volt+eq+'" + ((int)request.Voltages[i]).ToString(CultureInfo.InvariantCulture) + "'");
                    filterBuilder.Append(")");

                    if (i + 1 < request.Voltages.Count)
                    {
                        filterBuilder.Append("+or+");
                    }
                }

                filterBuilder.Append(")");
            }

            if (request.Phase.HasValue)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }
                filterBuilder.Append(FieldPhase + "+eq+" + (int)request.Phase);
            }

            if (request.MinKva.HasValue)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }

                filterBuilder.Append(FieldKva + "+ge+" + request.MinKva.Value);
            }

            if (request.MaxKva.HasValue)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }

                filterBuilder.Append(FieldKva + "+le+" + request.MaxKva.Value);
            }

            if (request.Category.HasValue)
            {
                if (filterBuilder.Length > 0)
                {
                    filterBuilder.Append("+and+");
                }

                filterBuilder.Append(FieldCategory + "/any(cat:+");
                filterBuilder.Append("cat+eq+'" + request.Category.Value + "'");
                filterBuilder.Append(")");
            }
            
            if (filterBuilder.Length > 0)
            {
                filterBuilder.Append("+and+");
            }

            filterBuilder.Append(FieldPublishedToWeb + "+eq+true");
            filterBuilder.Append("+and+");
            filterBuilder.Append(FieldStatus + "+ne+" + (int)Status.Scrapped);
            filterBuilder.Append("+and+");
            filterBuilder.Append(FieldStatus + "+ne+" + (int)Status.Sold);
            filterBuilder.Append("+and+");
            filterBuilder.Append(FieldStatus + "+ne+" + (int)Status.Rented);

            if (request.FastShip.HasValue && request.FastShip.Value)
            {
                filterBuilder.Append("+and+");
                filterBuilder.Append(FieldFastShip + "+eq+true");
            }
            
            queryParameters.Filter = filterBuilder.ToString();

            return queryParameters;
        }
    }
}