﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AzureSearchClient;
using TjSnow.DTO.Phase;
using TjSnow.DTO.Status;
using TjSnow.DTO.Voltage;
using TjSnow.Model.Entities;
using MachineEntity = TjSnow.Model.Entities.Machine;
using CategoryEntity = TjSnow.Model.Entities.Category;

namespace TjSnow.Application.Services.Search
{
    public class MachineDocument : Document
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string[] Voltage { get; set; }
        public PhaseDTO? Phase { get; set; }
        public string[] Category { get; set; }
        public double? KVA { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public StatusDTO Status { get; set; }
        public double? Price { get; set; }
        public string StockNumber { get; set; }
        public bool PublishedToWeb { get; set; }
        public string SecondaryAmps { get; set; }
        public bool? FastShip { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }

        public VoltageDTO? MachineVoltage { get; set; }
        
        public MachineDocument()
        {
            
        }

        public MachineDocument(MachineEntity machine)
        {
            this.Id = machine.Id.ToString(CultureInfo.InvariantCulture);
            this.Name = machine.Name;

            var images = machine.Images != null ? machine.Images.ToList() : new List<MachineImage>();

            if (images.Count > 0)
            {
                this.Thumbnail = images.OrderBy(image=>image.SortOrder).First().Url;
            }

            this.Voltage = machine.Voltage.HasValue ? new[] { ((int)machine.Voltage.Value).ToString(CultureInfo.InvariantCulture) } : new string[] { };

            if (machine.Voltage.HasValue)
            {
                MachineVoltage = (VoltageDTO) machine.Voltage.Value;
            }

            if (machine.Phase.HasValue)
            {
                this.Phase = (PhaseDTO) machine.Phase.Value;
            }

            var categories = machine.Categories != null ? machine.Categories.ToList() : new List<CategoryEntity>();
            Category = new List<string>(categories.Select(elem => elem.Id.ToString(CultureInfo.InvariantCulture))).ToArray();

            this.KVA = (double?) machine.KVA;
            this.Description = machine.WebDescription;
            this.Status = (StatusDTO) machine.Status;
            this.Price = (double?) machine.Price;
            this.StockNumber = machine.StockNumber;
            this.PublishedToWeb = machine.PublishedToWeb;
            this.SecondaryAmps = machine.SecAmps;
            this.FastShip = machine.FastShip;
            this.Manufacturer = machine.Manufacturer;
            this.Model = machine.Model;
        }

        public override IEnumerable<Field> GetFields()
        {
            var fields = SearchService.GetSearchableFields();

            return fields;
        }

        public override object this[string fieldName]
        {
            get
            {
                switch (fieldName)
                {
                    case SearchService.FieldId:             return this.Id;
                    case SearchService.FieldName:           return this.Name;
                    case SearchService.FieldThumbnail:      return this.Thumbnail;
                    case SearchService.FieldDescription:    return this.Description;
                    case SearchService.FieldVoltage:        return this.Voltage.ToArray();

                    case SearchService.FieldPhase:
                        if (this.Phase.HasValue)
                        {
                            return (int) this.Phase.Value;
                        }
                        return null;

                    case SearchService.FieldCategory:       return this.Category.ToArray();
                    case SearchService.FieldKva:            return this.KVA.HasValue ? this.KVA.Value.ToString(CultureInfo.InvariantCulture) : null;
                    case SearchService.FieldStatus:         return (int)this.Status;
                    case SearchService.FieldPrice:          return this.Price.HasValue ? this.Price.Value.ToString(CultureInfo.InvariantCulture) : null;
                    case SearchService.FieldStockNumber:    return this.StockNumber;
                    case SearchService.FieldPublishedToWeb: return this.PublishedToWeb;
                    case SearchService.FieldSecondaryAmps:  return this.SecondaryAmps;
                    case SearchService.FieldFastShip:       return this.FastShip;
                    case SearchService.FieldManufacturer:   return this.Manufacturer;
                    case SearchService.FieldModel:          return this.Model;
                    case SearchService.FieldMachineVoltage: return MachineVoltage.HasValue?(int)MachineVoltage.Value:0;
                        
                    default:
                        throw new NotSupportedException("Field name is not supported for indexing");
                }
            }
            set
            {
               throw new NotSupportedException();
            }
        }
    }
}