﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Phase;
using TjSnow.DTO.Search;
using TjSnow.DTO.Status;
using TjSnow.DTO.Voltage;
using TjSnow.Infrastructure.Utils;
using TjSnow.Model.Entities;
using TjSnow.Persistence.UnitOfWork.Base;

namespace TjSnow.Application.Services.Search
{
    public class DummySearchService : ISearchService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IObjectMapper _mapper;

        public DummySearchService(IUnitOfWork unitOfWork, IObjectMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public Task EnsureSearchConfigured()
        {
            return Task.Run(() => { });
        }

        public void Drop()
        {
            
        }

        public SearchResponse Search(SearchRequest request)
        {
            var categories = _unitOfWork.CategoryRepository.GetAll().Select(category =>
            {
                var categorySearchDto = new CategorySearchDTO
                {
                    Id = category.Id,
                    Name = category.Name
                };

                return categorySearchDto;
            }).ToList();

            var machines = new List<MachineSearchDTO>
            {
                new MachineSearchDTO
                {
                    Id = 1,
                    Description = "T. J. Snow introduces the new low profile bench type spot welder with a compact and sturdy, machined 'C' type frame. The robust frame is designed to minimize… ",
                    Status = StatusDTO.Available,
                    Thumbnail = "http://catalog.wlimg.com/1/1121464/full-images/standalone-impact-press-machine-1049101.jpg",
                    KVA = 12,
                    Phase = PhaseDTO.Single,
                    PhaseDescription = PhaseDTO.Single.GetEnumDescription(),
                    Voltage = VoltageDTO.V110,
                    VoltageDescription = VoltageDTO.V110.GetEnumDescription(),
                    Price = (decimal?) 1245.99,
                    Name = "TEst machine1",
                    Categories = categories.Take(5).ToList(),
                    StatusDescription = StatusDTO.Available.GetEnumDescription()   
                },
                new MachineSearchDTO
                {
                    Id = 2,
                    Description = "Machine description. Test machine description 2",
                    Status = StatusDTO.Customer,
                    Thumbnail = "http://img.diytrade.com/cdimg/668468/5152474/0/1201489698/Hydraulic_press_machine.jpg",
                    KVA = 17,
                    Phase = PhaseDTO.Three,
                    Price = (decimal?) 1345.99,
                    Name = "TEst machine2",
                    Categories = categories.Take(5).ToList(),
                    PhaseDescription = PhaseDTO.Three.GetEnumDescription(),
                    Voltage = VoltageDTO.V220,
                    VoltageDescription = VoltageDTO.V220.GetEnumDescription(),
                    StatusDescription = StatusDTO.Customer.GetEnumDescription()

                },
                new MachineSearchDTO
                {
                    Id = 3,
                    Description = "Test machine. press machine",
                    Status = StatusDTO.Rented,
                    Thumbnail = "http://img.diytrade.com/cdimg/668468/5152474/0/1201489698/Hydraulic_press_machine.jpg",
                    KVA = 15,
                    Phase = PhaseDTO.Single,
                    Price = (decimal?) 1945.99,
                    Name = "TEst machine3",
                    Categories = categories.Skip(4).Take(5).ToList(),
                    PhaseDescription = PhaseDTO.Single.GetEnumDescription(),
                    Voltage = VoltageDTO.V440,
                    VoltageDescription = VoltageDTO.V440.GetEnumDescription(),
                    StatusDescription = StatusDTO.Rented.GetEnumDescription()
                },
                new MachineSearchDTO
                {
                    Id = 4,
                    Description = "Machine description. Test machine description. Very good machine",
                    Status = StatusDTO.Available,
                    Thumbnail = "http://img.diytrade.com/cdimg/668468/5152474/0/1201489698/Hydraulic_press_machine.jpg",
                    KVA = 12,
                    Phase = PhaseDTO.Single,
                    Price = (decimal?) 34.99,
                    Name = "TEst machine4",
                    Categories = categories.Take(5).ToList(),
                    PhaseDescription = PhaseDTO.Single.GetEnumDescription(),
                    Voltage = VoltageDTO.V380,
                    VoltageDescription = VoltageDTO.V380.GetEnumDescription(),
                    StatusDescription = StatusDTO.Available.GetEnumDescription()
                },
                new MachineSearchDTO
                {
                    Id = 5,
                    Description = "Machine description. Test machine description",
                    Status = StatusDTO.Scrapped,
                    Thumbnail = "http://img.diytrade.com/cdimg/668468/5152474/0/1201489698/Hydraulic_press_machine.jpg",
                    KVA = 12,
                    Phase = PhaseDTO.Three,
                    Price = (decimal?) 2345.99,
                    Name = "TEst machine5",
                    Categories = categories.Skip(11).Take(2).ToList(),
                    PhaseDescription = PhaseDTO.Three.GetEnumDescription(),
                    Voltage = VoltageDTO.V550,
                    VoltageDescription = VoltageDTO.V550.GetEnumDescription(),
                    StatusDescription = StatusDTO.Scrapped.GetEnumDescription()
                }

            };
            
            return new SearchResponse
            {
                SearchRequest = request,
                TotalCount = machines.Count,
                Machines = machines
            };
        }

        public PublicSearchResponse PublicSearch(PublicSearchRequest request)
        {
            
            var categories = _unitOfWork.CategoryRepository.GetAll().Select(category =>
            {
                var categorySearchDto = new CategorySearchDTO
                {
                    Id = category.Id,
                    Name = category.Name
                };

                return categorySearchDto;
            }).ToList();
            
            var machines = new List<MachinePublicSearchDTO>
            {
                new MachinePublicSearchDTO
                {
                    Id = 1009,
                    Description = "T. J. Snow introduces the new low profile bench type spot welder with a compact and sturdy, machined 'C' type frame. The robust frame is designed to minimize… ",
                    Thumbnail = "http://catalog.wlimg.com/1/1121464/full-images/standalone-impact-press-machine-1049101.jpg",
                    KVA = 12,
                    Phase = PhaseDTO.Single,
                    PhaseDescription = PhaseDTO.Single.GetEnumDescription(),
                    Voltage = VoltageDTO.V110,
                    VoltageDescription = VoltageDTO.V110.GetEnumDescription(),
                    Name = "TEst machine1",
                    Model = "45"
                },
                new MachinePublicSearchDTO
                {
                    Id = 2,
                    Description = "Machine description. Test machine description 2",
                    Thumbnail = "http://img.diytrade.com/cdimg/668468/5152474/0/1201489698/Hydraulic_press_machine.jpg",
                    KVA = 17,
                    Phase = PhaseDTO.Three,
                    Name = "TEst machine2",
                    PhaseDescription = PhaseDTO.Three.GetEnumDescription()
                },
                new MachinePublicSearchDTO
                {
                    Id = 3,
                    Description = "Test machine. press machine",
                    Thumbnail = "http://img.diytrade.com/cdimg/668468/5152474/0/1201489698/Hydraulic_press_machine.jpg",
                    KVA = 15,
                    Name = "TEst machine3",
                    Voltage = VoltageDTO.V440,
                    VoltageDescription = VoltageDTO.V440.GetEnumDescription()
                },
                new MachinePublicSearchDTO
                {
                    Id = 4,
                    Description = "Machine description. Test machine description. Very good machine",
                    Thumbnail = "http://img.diytrade.com/cdimg/668468/5152474/0/1201489698/Hydraulic_press_machine.jpg",
                    KVA = 12,
                    Phase = PhaseDTO.Single,
                    Name = "TEst machine4",
                    PhaseDescription = PhaseDTO.Single.GetEnumDescription(),
                    Voltage = VoltageDTO.V380,
                    VoltageDescription = VoltageDTO.V380.GetEnumDescription()
                },
                new MachinePublicSearchDTO{
                    Id = 5,
                    Description = "Machine description. Test machine description",
                    Thumbnail = "http://img.diytrade.com/cdimg/668468/5152474/0/1201489698/Hydraulic_press_machine.jpg",
                    KVA = 12,
                    Name = "TEst machine5"
                    
                    
                }

            };


            var response = new PublicSearchResponse
            {
                Machines = machines,
                TotalCount = 5,
            };

            return response;
        }

        public void Add(Model.Entities.Machine machine)
        {
            
        }

        public void Add(IEnumerable<Model.Entities.Machine> machines)
        {
            
        }

        public void Update(Model.Entities.Machine machine)
        {
            
        }

        public void Delete(Model.Entities.Machine machine)
        {
            
        }
    }
}