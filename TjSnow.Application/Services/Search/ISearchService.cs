﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TjSnow.DTO.Search;
using MachineEntity = TjSnow.Model.Entities.Machine;

namespace TjSnow.Application.Services.Search
{
    public interface ISearchService
    {
        Task EnsureSearchConfigured();
        void Drop();

        SearchResponse Search(SearchRequest request);
        PublicSearchResponse PublicSearch(PublicSearchRequest request);

        void Add(MachineEntity machine);
        void Add(IEnumerable<MachineEntity> machines);
        void Update(MachineEntity machine);
        void Delete(MachineEntity machine);
    }
}