﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.DTO.Settings;

namespace TjSnow.Application.Services.Settings
{
    public interface ISettingsService
    {
        TJSettingsDTO GetTjSettings();
    }
}
