﻿using System.Web.Configuration;
using TjSnow.DTO.Settings;

namespace TjSnow.Application.Services.Settings
{
    public class SettingsService : ISettingsService
    {
        public TJSettingsDTO GetTjSettings()
        {
            var settings = new TJSettingsDTO
            {
                UedMachineBaseFilePath = WebConfigurationManager.AppSettings["UEDMachineFileBasePath"],
                P21Template = WebConfigurationManager.AppSettings["P21Template"]
            };

            return settings;
        }
    }
}