﻿using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.User;
using TjSnow.Persistence.Repositories.User;

namespace TjSnow.Application.Services.User
{
    using Exceptions;
    using Model.Entities.UserAgg;
    using System;

    public class UserService : IUserService
    {
        private readonly IUserRepository<User, int> _userRepository;

        private readonly IObjectMapper _mapper;

        public UserService(
            IUserRepository<User, int> userRepository,
            IObjectMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<UserDTO> FindByNameAsync(string email)
        {
            var user = await _userRepository.FindUserAsync(email);

            if (user == null)
                return null;

            var dto = _mapper.Map<User, UserDTO>(user);
            return dto;
        }

        public Task<string> GeneratePasswordResetTokenAsync(int userId)
        {
            return _userRepository.GeneratePasswordResetTokenAsync(userId);
        }

        public Task SendEmailForPasswordReset(int userId, string callbackUrl)
        {
            // todo email template
            return _userRepository.SendEmailAsync(userId, "Reset Password",
                "Please reset your password by clicking here: <a href=\"" + callbackUrl + "\">link</a>");
        }

        public async Task<UserDTO> CreateUserAsync(CreateUserDTO dto)
        {
            var appUser = new User(dto.Email, dto.FirstName, dto.LastName)
            {
                EmailConfirmed = true,
            };
            var result = await _userRepository.RegisterUserAsync(appUser, dto.Password);

            if (!result.Succeeded)
                throw new AppException(result.Errors.ToString());

            //await _securityService.AddToRoleAsync(appUser.Id, dto.Role);

            var created = _mapper.Map<User, UserDTO>(appUser);
            return created;
        }

        public async Task<UserDTO> FindUserAsync(int userId)
        {
            var user = await _userRepository.FindUserAsync(userId);

            if (user == null)
                return null;

            var dto = _mapper.Map<User, UserDTO>(user);
            return dto;
        }

        public async Task<bool> IsEmailAlreadyTakenAsync(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException(email);

            var user = await _userRepository.FindUserAsync(email);
            return user != null;
        }

    }
}
