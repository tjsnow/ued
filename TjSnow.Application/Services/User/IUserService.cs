﻿using System.Threading.Tasks;
using TjSnow.DTO.User;

namespace TjSnow.Application.Services.User
{
    public interface IUserService
    {
        Task<UserDTO> CreateUserAsync(CreateUserDTO dto);

        Task<UserDTO> FindUserAsync(int userId);

        Task<UserDTO> FindByNameAsync(string email);

        Task<string> GeneratePasswordResetTokenAsync(int userId);

        Task SendEmailForPasswordReset(int userId, string callbackUrl);

        Task<bool> IsEmailAlreadyTakenAsync(string email);
    }
}
