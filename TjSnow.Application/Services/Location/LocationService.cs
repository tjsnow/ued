﻿using System.Collections.Generic;
using System.Linq;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Location;
using TjSnow.Persistence.Repositories.Location;

namespace TjSnow.Application.Services.Location
{
    public class LocationService : ILocationService
    {
        private readonly ILocationRepository _locationRepository;
        private readonly IObjectMapper _objectMapper;

        public LocationService(ILocationRepository locationRepository, IObjectMapper objectMapper)
        {
            _locationRepository = locationRepository;
            _objectMapper = objectMapper;
        }

        public IEnumerable<LocationDTO> GetLocations()
        {
            return
                _locationRepository.RetrieveLocations()
                    .Select(location => _objectMapper.Map<Model.Entities.Location, LocationDTO>(location));
        }
    }
}