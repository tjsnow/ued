﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TjSnow.DTO.Location;

namespace TjSnow.Application.Services.Location
{
    public interface ILocationService
    {
        IEnumerable<LocationDTO> GetLocations();
    }
}
