﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TjSnow.DTO.Machine;

namespace TjSnow.Application.Services.Machine
{
    public interface IMachinesService
    {
        IEnumerable<MachineDTO> RetrieveMachines();

        MachineDTO CreateMachine(NewMachineWithMediaDTO machine);

        void DeleteMachine(int machineId);

        void UpdateMachine(MachineDTO machine);

        MachineDTO GetMachineById(int id);

        MachineDTO GetMachineByStockNumber(string stockNumber);

        IEnumerable<MachineDTO> GetLastRelatedMachines(int machineId);

        IEnumerable<MachineDTO> GetMachineByIds(int[] ids);

        void UpdateMachineImages(MachineDTO machine);
    }
}
