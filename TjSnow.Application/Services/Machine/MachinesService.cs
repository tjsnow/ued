﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using TjSnow.Application.Exceptions;
using TjSnow.Application.Extensions;
using TjSnow.Application.Mappers.Base;
using TjSnow.Application.Services.AzureStorage;
using TjSnow.DTO.Category;
using TjSnow.DTO.Condition;
using TjSnow.DTO.Importancy;
using TjSnow.DTO.Initiation;
using TjSnow.DTO.Location;
using TjSnow.DTO.Machine;
using TjSnow.DTO.MachineImage;
using TjSnow.DTO.MachineVideo;
using TjSnow.DTO.Phase;
using TjSnow.DTO.Status;
using TjSnow.DTO.Voltage;
using TjSnow.Infrastructure.Logging.Base;
using TjSnow.Infrastructure.Messaging.Base;
using TjSnow.Model.Contracts.Events.MachineAgg;
using TjSnow.Model.Entities;
using TjSnow.Model.Specifications;
using TjSnow.Persistence.UnitOfWork.Base;
using TjSnow.Infrastructure.Utils;
using MachineEntity = TjSnow.Model.Entities.Machine;
using CategoryEntity = TjSnow.Model.Entities.Category;

namespace TjSnow.Application.Services.Machine
{
    public class MachinesService : IMachinesService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IObjectMapper _mapper;
        private readonly IEventBus _eventBus;
        private readonly IAzureStorageService _azureStorageService;

        private static readonly ILogger Logger = LoggerFactory.CreateLog();

        public MachinesService(
            IUnitOfWork unitOfWork, 
            IObjectMapper mapper,
            IEventBus eventBus,
            IAzureStorageService azureStorageService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _eventBus = eventBus;
            _azureStorageService = azureStorageService;
        }

        private MachineDTO ConvertMachineToMachineDto(MachineEntity machine)
        {
            var machineDTO = _mapper.Map<MachineEntity, MachineDTO>(machine);

            var categoryDtos = machine.Categories.Select(category =>
            {
                var categoryDto = _mapper.Map<CategoryEntity, CategoryDTO>(category);

                categoryDto.AirType = _mapper.Map<Importancy, ImportancyDTO>(category.AirType);
                categoryDto.ArmDiaLower = _mapper.Map<Importancy, ImportancyDTO>(category.ArmDiaLower);
                categoryDto.ArmDiaUpper = _mapper.Map<Importancy, ImportancyDTO>(category.ArmDiaUpper);
                categoryDto.CheckedBy = _mapper.Map<Importancy, ImportancyDTO>(category.CheckedBy);
                categoryDto.Condition = _mapper.Map<Importancy, ImportancyDTO>(category.Condition);
                categoryDto.Control = _mapper.Map<Importancy, ImportancyDTO>(category.Control);
                categoryDto.Cost = _mapper.Map<Importancy, ImportancyDTO>(category.Cost);
                categoryDto.CylinderDiameter = _mapper.Map<Importancy, ImportancyDTO>(category.CylinderDiameter);
                categoryDto.CylinderStroke = _mapper.Map<Importancy, ImportancyDTO>(category.CylinderStroke);
                categoryDto.DateSold = _mapper.Map<Importancy, ImportancyDTO>(category.DateSold);
                categoryDto.EnteredDate = _mapper.Map<Importancy, ImportancyDTO>(category.EnteredDate);
                categoryDto.FastShip = _mapper.Map<Importancy, ImportancyDTO>(category.FastShip);
                categoryDto.Feautures = _mapper.Map<Importancy, ImportancyDTO>(category.Feautures);
                categoryDto.Freight = _mapper.Map<Importancy, ImportancyDTO>(category.Freight);
                categoryDto.HeadType = _mapper.Map<Importancy, ImportancyDTO>(category.HeadType);
                categoryDto.HolderDiameter = _mapper.Map<Importancy, ImportancyDTO>(category.HolderDiameter);
                categoryDto.Initiation = _mapper.Map<Importancy, ImportancyDTO>(category.Initiation);
                categoryDto.KVA = _mapper.Map<Importancy, ImportancyDTO>(category.KVA);
                categoryDto.Location = _mapper.Map<Importancy, ImportancyDTO>(category.Location);
                categoryDto.LowerDrive = _mapper.Map<Importancy, ImportancyDTO>(category.LowerDrive);
                categoryDto.Manufacturer = _mapper.Map<Importancy, ImportancyDTO>(category.Manufacturer);
                categoryDto.MaxForce = _mapper.Map<Importancy, ImportancyDTO>(category.MaxForce);
                categoryDto.Model = _mapper.Map<Importancy, ImportancyDTO>(category.Model);
                categoryDto.P21ItemId = _mapper.Map<Importancy, ImportancyDTO>(category.P21ItemId);
                categoryDto.Phase = _mapper.Map<Importancy, ImportancyDTO>(category.Phase);
                categoryDto.Photo = _mapper.Map<Importancy, ImportancyDTO>(category.Photo);
                categoryDto.PlatenLower = _mapper.Map<Importancy, ImportancyDTO>(category.PlatenLower);
                categoryDto.PlatenUpper = _mapper.Map<Importancy, ImportancyDTO>(category.PlatenUpper);
                categoryDto.Price = _mapper.Map<Importancy, ImportancyDTO>(category.Price);
                categoryDto.PurchasedDate = _mapper.Map<Importancy, ImportancyDTO>(category.PurchasedDate);
                categoryDto.PurchasedFrom = _mapper.Map<Importancy, ImportancyDTO>(category.PurchasedFrom);
                categoryDto.SecAmps = _mapper.Map<Importancy, ImportancyDTO>(category.SecAmps);
                categoryDto.SecVolts = _mapper.Map<Importancy, ImportancyDTO>(category.SecVolts);
                categoryDto.Serial = _mapper.Map<Importancy, ImportancyDTO>(category.Serial);
                categoryDto.Service = _mapper.Map<Importancy, ImportancyDTO>(category.Service);
                categoryDto.SoldTo = _mapper.Map<Importancy, ImportancyDTO>(category.SoldTo);
                categoryDto.StockNumeber = _mapper.Map<Importancy, ImportancyDTO>(category.StockNumber);
                categoryDto.TSlotSpacing = _mapper.Map<Importancy, ImportancyDTO>(category.TSlotSpacing);
                categoryDto.TapSwitch = _mapper.Map<Importancy, ImportancyDTO>(category.TapSwitch);
                categoryDto.ThroatDepth = _mapper.Map<Importancy, ImportancyDTO>(category.ThroatDepth);
                categoryDto.Transformer = _mapper.Map<Importancy, ImportancyDTO>(category.Transformer);
                categoryDto.UEDFilePath = _mapper.Map<Importancy, ImportancyDTO>(category.UEDFilePath);
                categoryDto.UpperDrive = _mapper.Map<Importancy, ImportancyDTO>(category.UpperDrive);
                categoryDto.Video = _mapper.Map<Importancy, ImportancyDTO>(category.Video);
                categoryDto.Voltage = _mapper.Map<Importancy, ImportancyDTO>(category.Voltage);
                categoryDto.WebDescription = _mapper.Map<Importancy, ImportancyDTO>(category.WebDescription);
                categoryDto.WheelLower = _mapper.Map<Importancy, ImportancyDTO>(category.WheelLower);
                categoryDto.WheelUpper = _mapper.Map<Importancy, ImportancyDTO>(category.WheelUpper);

                return categoryDto;
            });

            machineDTO.Categories = categoryDtos;

            machineDTO.Condition = machine.Condition == null
                ? (ConditionDTO?) null
                : _mapper.Map<Condition, ConditionDTO>(machine.Condition.Value);

            machineDTO.ConditionDescription = machine.Condition.HasValue
                ? machine.Condition.Value.GetEnumDescription()
                : null;

            machineDTO.Initiation = machine.Initiation == null
                ? (InitiationDTO?) null
                : _mapper.Map<Initiation, InitiationDTO>(machine.Initiation.Value);

            machineDTO.InitiationDescription = !machine.Initiation.HasValue
                ? null
                : machine.Initiation.Value.GetEnumDescription();

            machineDTO.Location = machine.Location == null
                ? null
                : _mapper.Map<TjSnow.Model.Entities.Location, LocationDTO>(machine.Location);

            machineDTO.Images = machine.Images.Select(image => _mapper.Map<MachineImage, MachineImageDTO>(image)).OrderBy(image=>image.SortOrder);

            machineDTO.Videos = machine.Videos.Select(video => _mapper.Map<MachineVideo, MachineVideoDTO>(video));

            machineDTO.Status = _mapper.Map<Status, StatusDTO>(machine.Status);

            machineDTO.Phase = machine.Phase == null ? (PhaseDTO?) null : _mapper.Map<Phase, PhaseDTO>(machine.Phase.Value);

            machineDTO.PhaseDescription = machine.Phase != null ? machine.Phase.GetEnumDescription() : "";

            machineDTO.Voltage = machine.Voltage == null
                ? (VoltageDTO?) null
                : _mapper.Map<Voltage, VoltageDTO>(machine.Voltage.Value);

            machineDTO.VoltageDescription = machine.Voltage != null?machine.Voltage.GetEnumDescription():"";

            return machineDTO;
        }

        public IEnumerable<MachineDTO> RetrieveMachines()
        {
            try
            {
                var machines = _unitOfWork.MachineRepository.RetrieveMachines();

                return machines.Select(ConvertMachineToMachineDto);
            }
            catch (Exception e)
            {
                Logger.LogError("Error occurs during retrieve machines", e);

                throw new TJSApplicationException("Error occurs during retrieve machines",e);
            }
        }

        public MachineDTO CreateMachine(NewMachineWithMediaDTO machine)
        {
            try
            {
                var duplicateMachine = _unitOfWork.MachineRepository.GetMachineByStockNumber(machine.StockNumber);

                if (duplicateMachine != null)
                {
                    throw new TJSApplicationException(string.Format("Machine with StockNumber='{0}' already exists",machine.StockNumber));
                }

                
                var machineEntity = _mapper.Map<NewMachineWithMediaDTO, MachineEntity>(machine);

                var categories = new List<CategoryEntity>();

                if (machine.Categories != null)
                {
                    foreach (var categoryId in machine.Categories)
                    {
                        var category = _unitOfWork.CategoryRepository.GetById(categoryId);

                        category.ValidateFound(categoryId);

                        categories.Add(category);
                    }
                }

                machineEntity.Categories = categories;

                if (machine.Condition != null)
                {
                    if (!Enum.IsDefined(typeof (Condition), (int)machine.Condition.Value))
                    {
                        throw new TJSApplicationException(string.Format("Condition enum member for value = {0:d} not found",machine.Condition));
                    }

                    machineEntity.Condition = _mapper.Map<ConditionDTO, Condition>(machine.Condition.Value);
                }

                if (machine.Initiation != null)
                {
                    if (!Enum.IsDefined(typeof(Initiation), (int)machine.Initiation.Value))
                    {
                        throw new TJSApplicationException(string.Format("Initiation enum member for value = {0:d} not found", machine.Initiation));
                    }

                    machineEntity.Initiation = _mapper.Map<InitiationDTO, Initiation>(machine.Initiation.Value);
                }

                if (machine.LocationId != null)
                {
                    var location = _unitOfWork.LocationRepository.FirstOrDefault(LocationSpecifications.ById(machine.LocationId.Value));

                    location.ValidateFound(machine.LocationId.Value);

                    machineEntity.Location = location;
                    machineEntity.LocationId = location.Id;
                }

                if (machine.Phase != null)
                {
                    if (!Enum.IsDefined(typeof(Phase), (int)machine.Phase.Value))
                    {
                        throw new TJSApplicationException(string.Format("Phase enum member for value = {0:d} not found", machine.Phase));
                    }

                    machineEntity.Phase = _mapper.Map<PhaseDTO, Phase>(machine.Phase.Value);
                }

                if (!Enum.IsDefined(typeof(Status), (int)machine.Status))
                {
                    throw new TJSApplicationException(string.Format("Status enum member for value = {0:d} not found", machine.Status));
                }

                machineEntity.Status = _mapper.Map<StatusDTO, Status>(machine.Status);

                if (machine.Voltage != null)
                {
                    if (!Enum.IsDefined(typeof(Voltage), (int)machine.Voltage.Value))
                    {
                        throw new TJSApplicationException(string.Format("Voltage enum member for value = {0:d} not found", machine.Voltage));
                    }

                    machineEntity.Voltage = _mapper.Map<VoltageDTO, Voltage>(machine.Voltage.Value);
                }

                if(machine.Images!=null)
                {
                    machineEntity.Images = machine.Images.Select(image => _mapper.Map<MachineImageDTO, MachineImage>(image)).ToList();
                }

                if (machine.Videos!=null)
                {
                    machineEntity.Videos = machine.Videos.Select(video => _mapper.Map<MachineVideoDTO, MachineVideo>(video)).ToList();
                }
                
                _unitOfWork.MachineRepository.InsertMachine(machineEntity);

                _unitOfWork.Save();

                _eventBus.Publish(new MachineCreated(machineEntity));

                return ConvertMachineToMachineDto(machineEntity);
            }
            catch (TJSApplicationException e)
            {
                Logger.LogError("Error occurs during create machine", e);

                throw;
            }
            catch (Exception e)
            {
                //var e = error as System.Data.Entity.Validation.DbEntityValidationException;

                //var sb = new StringBuilder();

                //if (e != null)
                //{

                //    foreach (var eve in e.EntityValidationErrors)
                //    {
                //        sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State));

                //        foreach (var ve in eve.ValidationErrors)
                //        {
                //            sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                //        }
                //    }

                    
                //}
                
                Logger.LogError("Error occurs during create machine", e);

                throw new TJSApplicationException("Error occurs during create machine");
            }
        }

        public void DeleteMachine(int machineId)
        {
            try
            {
                var machine = _unitOfWork.MachineRepository.GetMachineById(machineId);

                machine.ValidateFound(machineId);

                var images = _unitOfWork.MachineImageRepository.Find(MachineImageSpecifications.ByMachineId(machineId));

                foreach (var machineImage in images)
                {
                    _azureStorageService.RemoveImageFromStorage(machineImage.Url);
                }

                _unitOfWork.MachineRepository.DeleteMachine(machineId);

                _unitOfWork.Save();

                _eventBus.Publish(new MachineDeleted(machine));
            }
            catch (NotFoundException)
            {
                throw;
            }
            catch (Exception e)
            {
                Logger.LogError(string.Format("Error occurs during delete machine with Id={0}", machineId), e);

                throw new TJSApplicationException(string.Format("Error occurs during delete machine with Id={0}",machineId),e);
            }
        }

        public void UpdateMachine(MachineDTO machine)
        {
            try
            {
                var machineEntity = _unitOfWork.MachineRepository.GetMachineById(machine.Id);

                machineEntity.ValidateFound(machine.Id);

                machineEntity.AirType = machine.AirType;
                machineEntity.InternalNotes = machine.InternalNotes;
                machineEntity.Name = machine.Name;
                machineEntity.ArmDiaLower = machine.ArmDiaLower;
                machineEntity.ArmDiaUpper = machine.ArmDiaUpper;
                machineEntity.CheckedBy = machine.CheckedBy;
                machineEntity.Control = machine.Control;
                machineEntity.Cost = machine.Cost;
                machineEntity.CylinderDiameter = machine.CylinderDiameter;
                machineEntity.CylinderStroke = machine.CylinderStroke;
                machineEntity.DateSold = machine.DateSold;
                machineEntity.EnteredDate = machine.EnteredDate;
                machineEntity.FastShip = machine.FastShip;
                machineEntity.Feautures = machine.Feautures;
                machineEntity.Freight = machine.Freight;
                machineEntity.HeadType = machine.HeadType;
                machineEntity.HolderDiameter = machine.HolderDiameter;
                machineEntity.KVA = machine.KVA;
                machineEntity.LocationId = machine.LocationId;
                machineEntity.LowerDrive = machine.LowerDrive;
                machineEntity.Manufacturer = machine.Manufacturer;
                machineEntity.MaxForce = machine.MaxForce;
                machineEntity.Model = machine.Model;
                machineEntity.P21ItemId = machine.P21ItemId;
                machineEntity.PlatenLower = machine.PlatenLower;
                machineEntity.PlatenUpper = machine.PlatenUpper;
                machineEntity.Price = machine.Price;
                machineEntity.PublishedToWeb = machine.PublishedToWeb;
                machineEntity.PurchasedDate = machine.PurchasedDate;
                machineEntity.PurchasedFrom = machine.PurchasedFrom;
                machineEntity.SecAmps = machine.SecAmps;
                machineEntity.SecVolts = machine.SecVolts;
                machineEntity.Serial = machine.Serial;
                machineEntity.Service = machine.Service;
                machineEntity.SoldTo = machine.SoldTo;
                machineEntity.StockNumber = machine.StockNumber;
                machineEntity.TSlotSpacing = machine.TSlotSpacing;
                machineEntity.TapSwitch = machine.TapSwitch;
                machineEntity.ThroatDepth = machine.ThroatDepth;
                machineEntity.Transformer = machine.Transformer;
                machineEntity.UEDFilePath = machine.UEDFilePath;
                machineEntity.UpperDrive = machine.UpperDrive;
                machineEntity.WebDescription = machine.WebDescription;
                machineEntity.WheelLower = machine.WheelLower;
                machineEntity.WheelUpper = machine.WheelUpper;
                machineEntity.ExcludeFromReconciliation = machine.ExcludeFromReconciliation;

                foreach (var category in machineEntity.Categories.ToList())
                {
                    machineEntity.Categories.Remove(category);
                }

                var categories = new List<CategoryEntity>();

                if (machine.Categories != null)
                {
                    foreach (var categoryDto in machine.Categories)
                    {
                        var category = _unitOfWork.CategoryRepository.GetById(categoryDto.Id);

                        category.ValidateFound(categoryDto.Id);

                        categories.Add(category);
                    }
                }

                machineEntity.Categories = categories;

                machineEntity.Condition = machine.Condition == null
                ? (Condition?)null
                : _mapper.Map<ConditionDTO, Condition>(machine.Condition.Value);

                machineEntity.Initiation = machine.Initiation == null
                    ? (Initiation?)null
                    : _mapper.Map<InitiationDTO, Initiation>(machine.Initiation.Value);
                
                if (machine.LocationId != null)
                {
                    var location = _unitOfWork.LocationRepository.FirstOrDefault(LocationSpecifications.ById(machine.LocationId.Value));

                    location.ValidateFound(machine.LocationId.Value);

                    machineEntity.Location = location;
                }
                else
                {
                    machineEntity.Location = null;
                }

                if (machine.Images == null)
                {
                    foreach (var image in machineEntity.Images.ToList())
                    {
                        machineEntity.Images.Remove(image);

                        _azureStorageService.RemoveImageFromStorage(image.Url);

                        _unitOfWork.MachineImageRepository.Delete(image);
                    }
                }
                else
                {
                    foreach (var image in machineEntity.Images.ToList())
                    {
                        if (!machine.Images.Select(i => i.Id).Contains(image.Id))
                        {
                            machineEntity.Images.Remove(image);

                            _azureStorageService.RemoveImageFromStorage(image.Url);

                            _unitOfWork.MachineImageRepository.Delete(image);
                        }
                    }

                    foreach (var image in machine.Images.Where(i => i.Id == 0))
                    {
                        machineEntity.Images.Add(_mapper.Map<MachineImageDTO, MachineImage>(image));
                    }

                    foreach (var image in machine.Images.Where(i => i.Id != 0))
                    {
                        machineEntity.Images.First(img => img.Id == image.Id).IsPublished = image.IsPublished;
                        machineEntity.Images.First(img => img.Id == image.Id).SortOrder = image.SortOrder;
                    }
                }

                if (machine.Videos == null)
                {
                    foreach (var video in machineEntity.Videos.ToList())
                    {
                        machineEntity.Videos.Remove(video);

                        _unitOfWork.MachineVideoRepository.Delete(video);
                    }
                }
                else
                {
                    foreach (var video in machineEntity.Videos.ToList())
                    {
                        if (!machine.Videos.Select(i => i.Id).Contains(video.Id))
                        {
                            machineEntity.Videos.Remove(video);

                            _unitOfWork.MachineVideoRepository.Delete(video);
                        }
                    }

                    foreach (var video in machine.Videos.Where(i => i.Id == 0))
                    {
                        machineEntity.Videos.Add(_mapper.Map<MachineVideoDTO, MachineVideo>(video));
                    }
                }

                machineEntity.Status = _mapper.Map<StatusDTO, Status>(machine.Status);

                machineEntity.Phase = machine.Phase == null ? (Phase?)null : _mapper.Map<PhaseDTO, Phase>(machine.Phase.Value);

                machineEntity.Voltage = machine.Voltage == null
                    ? (Voltage?)null
                    : _mapper.Map<VoltageDTO, Voltage>(machine.Voltage.Value);
                
                _unitOfWork.MachineRepository.UpdateMachine(machineEntity);

                _unitOfWork.Save();

                if (machineEntity.Images != null)
                {
                    machineEntity.Images = machineEntity.Images.OrderBy(image => image.SortOrder).ToArray();
                }
                else
                {
                    machineEntity.Images = new List<MachineImage>();
                }

                _eventBus.Publish(new MachineUpdated(machineEntity));
            }
            catch (NotFoundException)
            {
                throw;
            }
            catch (Exception e)
            {
                Logger.LogError(string.Format("Error occurs during update machine with Id={0}", machine.Id), e);

                throw new TJSApplicationException(string.Format("Error occurs during update machine with Id={0}", machine.Id), e);
            }
        }
        
        public MachineDTO GetMachineById(int id)
        {
            try
            {
                var machine = _unitOfWork.MachineRepository.GetMachineById(id);

                machine.ValidateFound(id);

                if (machine.Categories != null)
                {
                    machine.Categories = new List<CategoryEntity>(machine.Categories.OrderBy(category => category.Id));
                }

                return ConvertMachineToMachineDto(machine);
            }
            catch (Exception e)
            {
                Logger.LogError(string.Format("Error occurs retrieve  machine with Id={0}", id), e);

                throw new TJSApplicationException(string.Format("Error occurs retrieve  machine with Id={0}", id), e);
            }
        }

        public MachineDTO GetMachineByStockNumber(string stockNumber)
        {
            var machine = _unitOfWork.MachineRepository.GetMachineByStockNumber(stockNumber);

            if (machine == null)
            {
                return null;
            }

            return ConvertMachineToMachineDto(machine);
        }

        public IEnumerable<MachineDTO> GetLastRelatedMachines(int machineId)
        {
            try
            {
                var machine = GetMachineById(machineId);

                var categoryIds = machine.Categories.Select(category => category.Id).ToArray();

                return _unitOfWork.MachineRepository.GetMachinesByCategoryIds(categoryIds).Where(m=>m.Id!=machineId).Select(ConvertMachineToMachineDto);
            }
            catch (TJSApplicationException e)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new TJSApplicationException(string.Format("Error occurs during extract related machines for machine with Id={0}",machineId),e);
            }
        }

        public IEnumerable<MachineDTO> GetMachineByIds(int[] ids)
        {
            try
            {
                return _unitOfWork.MachineRepository.GetMachineByIds(ids).Select(ConvertMachineToMachineDto);
            }
            catch (Exception e)
            {
                throw new TJSApplicationException("Error occurs during extract machines", e);
            }
        }

        public void UpdateMachineImages(MachineDTO machine)
        {
            try
            {
                var machineEntity = _unitOfWork.MachineRepository.GetMachineById(machine.Id);

                machineEntity.ValidateFound(machine.Id);

                if (machine.Images == null)
                {
                    foreach (var image in machineEntity.Images.ToList())
                    {
                        //machineEntity.Images.Remove(image);

                        _azureStorageService.RemoveImageFromStorage(image.Url);

                        _unitOfWork.MachineImageRepository.Delete(image);
                    }
                }
                else
                {
                    foreach (var image in machineEntity.Images.ToList())
                    {
                        if (!machine.Images.Select(i => i.Id).Contains(image.Id))
                        {
                            //machineEntity.Images.Remove(image);
                            _azureStorageService.RemoveImageFromStorage(image.Url);

                            _unitOfWork.MachineImageRepository.Delete(image);
                        }
                    }

                    foreach (var image in machine.Images.Where(i => i.Id == 0))
                    {
                        //machineEntity.Images.Add(_mapper.Map<MachineImageDTO, MachineImage>(image));
                        var machineImage = _mapper.Map<MachineImageDTO, MachineImage>(image);

                        machineImage.MachineId = machine.Id;

                        _unitOfWork.MachineImageRepository.Insert(machineImage);
                    }

                    foreach (var image in machine.Images.Where(i => i.Id != 0))
                    {
                        var machineImage = machineEntity.Images.First(img => img.Id == image.Id);

                        machineImage.IsPublished = image.IsPublished;
                        machineImage.SortOrder = image.SortOrder;

                        _unitOfWork.MachineImageRepository.Update(machineImage);
                    }
                }

                //_unitOfWork.MachineRepository.UpdateMachine(machineEntity);

                _unitOfWork.Save();

                var updatedMachine = _unitOfWork.MachineRepository.GetMachineById(machine.Id);

                _eventBus.Publish(new MachineUpdated(updatedMachine));
            }
            catch (DbUpdateConcurrencyException e)
            {
                var entry = e.Entries.Single();

                entry.OriginalValues.SetValues(entry.CurrentValues);
                entry.CurrentValues.SetValues(entry.CurrentValues);

                _unitOfWork.Save();
            }
            catch (Exception e)
            {
                Logger.LogError(string.Format("Error occurs during update machine with Id={0}", machine.Id), e);

                throw new TJSApplicationException(string.Format("Error occurs during update machine with Id={0}", machine.Id), e);
            }
        }
    }
}