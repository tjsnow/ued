﻿using System.Collections.Generic;
using System.Linq;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Category;
using TjSnow.Persistence.Repositories.Category;

namespace TjSnow.Application.Services.Category
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IObjectMapper _objectMapper;

        public CategoryService(
            ICategoryRepository categoryRepository,
            IObjectMapper objectMapper)
        {
            _categoryRepository = categoryRepository;
            _objectMapper = objectMapper;
        }

        public IEnumerable<CategoryShortDTO> RetrieveCategories()
        {
            return
                _categoryRepository.GetAll()
                    .Select(elem => _objectMapper.Map<Model.Entities.Category, CategoryShortDTO>(elem));
        }
    }
}