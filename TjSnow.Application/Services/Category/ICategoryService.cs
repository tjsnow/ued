﻿using System.Collections.Generic;
using TjSnow.DTO.Category;

namespace TjSnow.Application.Services.Category
{
    public interface ICategoryService
    {
        IEnumerable<CategoryShortDTO> RetrieveCategories();
    }
}