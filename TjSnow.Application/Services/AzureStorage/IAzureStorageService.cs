﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.DTO.MachineImage;
using TjSnow.DTO.MachineVideo;

namespace TjSnow.Application.Services.AzureStorage
{
    public interface IAzureStorageService
    {
        IEnumerable<MachineImageDTO> UploadImagesToStorage(IEnumerable<MachineImageDataDTO> images);

        void RemoveImageFromStorage(string url);
    }
}
