﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using TjSnow.Application.Exceptions;
using TjSnow.DTO.MachineImage;
using TjSnow.DTO.MachineVideo;

namespace TjSnow.Application.Services.AzureStorage
{
    public class AzureStorageService : IAzureStorageService
    {
        public IEnumerable<MachineImageDTO> UploadImagesToStorage(IEnumerable<MachineImageDataDTO> images)
        {
            try
            {
                var containerName = WebConfigurationManager.AppSettings["ImageContainerName"];

                var azureStorageConnectionString = WebConfigurationManager.AppSettings["AzureStorageConnectionString"];

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(azureStorageConnectionString);

                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                CloudBlobContainer blobContainer = blobClient.GetContainerReference(containerName);

                if (blobContainer.CreateIfNotExists())
                {
                    blobContainer.SetPermissions(new BlobContainerPermissions(){PublicAccess = BlobContainerPublicAccessType.Blob});
                }

                var machineImages = new List<MachineImageDTO>();

                foreach (var image in images)
                {
                    var uniqueFileName = Guid.NewGuid().ToString() + image.FileName.Substring(image.FileName.LastIndexOf('.'));
                    
                    CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(uniqueFileName);

                    blockBlob.UploadFromByteArray(image.Data,0,image.Data.Length);

                    blockBlob.Properties.ContentType = image.ContentType;

                    blockBlob.SetProperties();

                    machineImages.Add(new MachineImageDTO
                    {
                        ContentType = image.ContentType,
                        Url = blockBlob.Uri.ToString(),
                        FileName = image.FileName
                    });
                }

                return machineImages;
            }
            catch (Exception e)
            {
                throw new TJSApplicationException("Error occurs during upload image to Azure Storage",e);
            }
        }

        public void RemoveImageFromStorage(string url)
        {
            try
            {
                var containerName = WebConfigurationManager.AppSettings["ImageContainerName"];

                var azureStorageConnectionString = WebConfigurationManager.AppSettings["AzureStorageConnectionString"];

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(azureStorageConnectionString);

                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                CloudBlobContainer blobContainer = blobClient.GetContainerReference(containerName);

                var blob = blobContainer.GetBlockBlobReference(url.Substring(url.LastIndexOf('/') + 1));

                blob.DeleteIfExists();
            }
            catch (Exception e)
            {
                throw new TJSApplicationException("Error occurs during remove image from Azure Storage", e);
            }
        }
    }
}