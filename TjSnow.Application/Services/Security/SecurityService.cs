﻿namespace TjSnow.Application.Services.Security
{
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin;
    using Microsoft.Owin.Security;
    using System.Web;
    using System.Threading.Tasks;
    using Model.Entities.UserAgg;
    using Persistence.Identity;

    public class SecurityService : ISecurityService
    {
        #region Fields and properties

        private readonly HttpContext _context;

        private readonly IOwinContext _owinContext;

        private readonly ApplicationUserManager _userManager;

        private int _currentUserId = -1;

        public bool IsAuthenticated
        {
            get { return _context.User.Identity.IsAuthenticated; }
        }

        private string _currentUserName;

        public string CurrentUserName
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentUserName))
                {
                    return _currentUserName;
                }

                if (_context.User.Identity.IsAuthenticated)
                {
                    _currentUserName = _context.User.Identity.Name;
                }

                return _currentUserName;
            }
        }

        public int CurrentUserId
        {
            get
            {
                if (_currentUserId != -1)
                {
                    return _currentUserId;
                }

                if (_context.User.Identity.IsAuthenticated)
                {
                    _currentUserId = _context.User.Identity.GetUserId<int>();
                }

                return _currentUserId;
            }
        }

        #endregion

        #region Constructor

        public SecurityService(
            HttpContext context,
            IOwinContext owinContext,
            ApplicationUserManager userManager)
        {
            _context = context;
            _owinContext = owinContext;
            _userManager = userManager;
        }

        #endregion

        public async Task<bool> SignInAsync(string username, string password, bool isPersistent)
        {
            var user = await _userManager.FindAsync(username, password);
            if (user == null)
                return false;

            var authenticationManager = _owinContext.Authentication;

            authenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            var identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);

            authenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, identity);

            return true;
        }

        public void SignOut()
        {
            _owinContext.Authentication.SignOut();
        }
    }
}
