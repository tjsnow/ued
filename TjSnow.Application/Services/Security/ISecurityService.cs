﻿namespace TjSnow.Application.Services.Security
{
    using System.Threading.Tasks;

    public interface ISecurityService
    {
        bool IsAuthenticated { get; }

        string CurrentUserName { get; }

        int CurrentUserId { get; }

        Task<bool> SignInAsync(string username, string password, bool isPersistent);

        void SignOut();
    }
}
