﻿namespace TjSnow.Application.Mappers.User
{
    using Base;
    using DTO.User;
    using Model.Entities.UserAgg;

    public class UserToDtoConverter : IObjectConverter<User, UserDTO>
    {
        public UserDTO Convert(User source, object param = null)
        {
            var dto = new UserDTO
            {
                UserId = source.Id,
                Email = source.Email,
                PhoneNumber = source.PhoneNumber,
                FirstName = source.Profile.FirstName,
                LastName = source.Profile.LastName,
            };
            return dto;
        }
    }
}
