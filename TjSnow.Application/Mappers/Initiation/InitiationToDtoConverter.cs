﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Category;
using TjSnow.DTO.Initiation;
using InitiationEntity = TjSnow.Model.Entities.Initiation;

namespace TjSnow.Application.Mappers.Initiation
{
    public class InitiationToDtoConverter : IObjectConverter<InitiationEntity, InitiationDTO>
    {
        public InitiationDTO Convert(InitiationEntity source, object param = null)
        {
            return (InitiationDTO) source;
        }
    }
}
