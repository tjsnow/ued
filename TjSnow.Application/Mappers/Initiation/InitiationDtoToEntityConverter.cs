﻿using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Initiation;
using InitiationEntity = TjSnow.Model.Entities.Initiation;

namespace TjSnow.Application.Mappers.Initiation
{
    public class InitiationDtoToEntityConverter : IObjectConverter<InitiationDTO, InitiationEntity>
    {
        public InitiationEntity Convert(InitiationDTO source, object param = null)
        {
            return (InitiationEntity) source;
        }
    }
}
