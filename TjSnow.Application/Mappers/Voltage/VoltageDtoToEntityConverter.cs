﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Voltage;
using VoltageEntity=TjSnow.Model.Entities.Voltage;

namespace TjSnow.Application.Mappers.Voltage
{
    public class VoltageDtoToEntityConverter : IObjectConverter<VoltageDTO, VoltageEntity>
    {
        public VoltageEntity Convert(VoltageDTO source, object param = null)
        {
            return (VoltageEntity) source;
        }
    }
}
