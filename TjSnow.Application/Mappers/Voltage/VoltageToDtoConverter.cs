﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Voltage;
using VoltageEntity=TjSnow.Model.Entities.Voltage;

namespace TjSnow.Application.Mappers.Voltage
{
    public class VoltageToDtoConverter:IObjectConverter<VoltageEntity,VoltageDTO>
    {
        public VoltageDTO Convert(VoltageEntity source, object param = null)
        {
            return (VoltageDTO) source;
        }
    }
}
