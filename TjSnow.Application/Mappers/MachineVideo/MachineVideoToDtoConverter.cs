﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.MachineVideo;
using MachineVideoEntity = TjSnow.Model.Entities.MachineVideo;

namespace TjSnow.Application.Mappers.MachineVideo
{
    public class MachineVideoToDtoConverter:IObjectConverter<MachineVideoEntity,MachineVideoDTO>
    {
        private const string YouTubeEmbedUrlPrefix = @"//www.youtube.com/embed/";
        private const string YouTubeWatchUrlPrefix = @"//http://www.youtube.com/watch?v=";
        
        public MachineVideoDTO Convert(MachineVideoEntity source, object param = null)
        {
            return new MachineVideoDTO
            {
                Id = source.Id,
                MachineId = source.MachineId,
                Url = source.Url,
                WatchUrl = YouTubeWatchUrlPrefix+source.Url.Replace(YouTubeEmbedUrlPrefix,string.Empty)
            };
        }
    }
}
