﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.MachineVideo;
using MachineVideoEntity = TjSnow.Model.Entities.MachineVideo;

namespace TjSnow.Application.Mappers.MachineVideo
{
    public class MachineVideoDtoToEntityConverter : IObjectConverter<MachineVideoDTO,MachineVideoEntity>
    {
        public MachineVideoEntity Convert(MachineVideoDTO source, object param = null)
        {
            return new MachineVideoEntity
            {
                Id = source.Id,
                MachineId = source.MachineId,
                Url = source.Url
            };
        }
    }
}
