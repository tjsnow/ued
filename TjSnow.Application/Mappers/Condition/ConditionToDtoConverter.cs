﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Category;
using TjSnow.DTO.Condition;
using ConditionEntity = TjSnow.Model.Entities.Condition;

namespace TjSnow.Application.Mappers.Condition
{
    public class ConditionToDtoConverter : IObjectConverter<ConditionEntity, ConditionDTO>
    {
        public ConditionDTO Convert(ConditionEntity source, object param = null)
        {
            return (ConditionDTO) source;
        }
    }
}
