﻿using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Category;
using TjSnow.DTO.Condition;
using ConditionEntity = TjSnow.Model.Entities.Condition;

namespace TjSnow.Application.Mappers.Condition
{
    public class ConditionDtoToEntityConverter : IObjectConverter<ConditionDTO, ConditionEntity>
    {
        public ConditionEntity Convert(ConditionDTO source, object param = null)
        {
            return (ConditionEntity) source;
        }
    }
}
