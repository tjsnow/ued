﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Phase;
using PhaseEntity = TjSnow.Model.Entities.Phase;

namespace TjSnow.Application.Mappers.Phase
{
    public class PhaseDtoToEntityConverter : IObjectConverter<PhaseDTO, PhaseEntity>
    {
        public PhaseEntity Convert(PhaseDTO source, object param = null)
        {
            return (PhaseEntity) source;
        }
    }
}
