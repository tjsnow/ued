﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Category;
using TjSnow.DTO.Location;
using LocationEntity = TjSnow.Model.Entities.Location;

namespace TjSnow.Application.Mappers.Location
{
    public class LocationDtoToEntityConverter : IObjectConverter<LocationDTO, LocationEntity>
    {
        public LocationEntity Convert(LocationDTO source, object param = null)
        {
            return new LocationEntity
            {
                Id = source.Id,
                Name = source.Name
            };
        }
    }
}
