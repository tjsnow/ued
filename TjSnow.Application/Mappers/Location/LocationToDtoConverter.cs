﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Category;
using TjSnow.DTO.Location;
using LocationEntity = TjSnow.Model.Entities.Location;

namespace TjSnow.Application.Mappers.Category
{
    public class LocationToDtoConverter : IObjectConverter<LocationEntity, LocationDTO>
    {
        public LocationDTO Convert(LocationEntity source, object param = null)
        {
            return new LocationDTO
            {
                Id = source.Id,
                Name = source.Name
            };
        }
    }
}
