﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Category;
using CategoryEntity=TjSnow.Model.Entities.Category;

namespace TjSnow.Application.Mappers.Category
{
    public class CategoryToDtoConverter:IObjectConverter<CategoryEntity,CategoryDTO>
    {
        public CategoryDTO Convert(CategoryEntity source, object param = null)
        {
            return new CategoryDTO
            {
                Id = source.Id,
                Name = source.Name,
                CategoryImage = source.CategoryImage
            };
        }
    }
}
