﻿using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Category;
using CategoryEntity = TjSnow.Model.Entities.Category;

namespace TjSnow.Application.Mappers.Category
{
    public class CategoryToShortDtoConverter : IObjectConverter<CategoryEntity, CategoryShortDTO>
    {
        public CategoryShortDTO Convert(CategoryEntity source, object param = null)
        {
            return new CategoryShortDTO
            {
                Id = source.Id,
                Name = source.Name,
                CategoryImage = source.CategoryImage
            };
        }
    }
}