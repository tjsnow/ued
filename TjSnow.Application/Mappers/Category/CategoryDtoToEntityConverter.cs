﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Category;
using CategoryEntity=TjSnow.Model.Entities.Category;

namespace TjSnow.Application.Mappers.Category
{
    public class CategoryDtoToEntityConverter : IObjectConverter<CategoryDTO, CategoryEntity>
    {
        public CategoryEntity Convert(CategoryDTO source, object param = null)
        {
            return new CategoryEntity
            {
                Id = source.Id,
                Name = source.Name
            };
        }
    }
}
