﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Status;
using StatusEntity=TjSnow.Model.Entities.Status;

namespace TjSnow.Application.Mappers.Status
{
    public class StatusToDtoConverter:IObjectConverter<StatusEntity,StatusDTO>
    {
        public StatusDTO Convert(StatusEntity source, object param = null)
        {
            return (StatusDTO) source;
        }
    }
}
