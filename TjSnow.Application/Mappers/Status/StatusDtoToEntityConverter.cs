﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Status;
using StatusEntity=TjSnow.Model.Entities.Status;

namespace TjSnow.Application.Mappers.Status
{
    public class StatusDtoToEntityConverter : IObjectConverter<StatusDTO, StatusEntity>
    {
        public StatusEntity Convert(StatusDTO source, object param = null)
        {
            return (StatusEntity) source;
        }
    }
}
