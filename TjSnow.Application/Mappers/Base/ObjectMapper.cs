﻿namespace TjSnow.Application.Mappers.Base
{
    using System;
    using System.Collections.Generic;

    public class ObjectMapper : IObjectMapper
    {
        #region Fields and properties

        private readonly Dictionary<Tuple<Type, Type>, IConverter> _converters;

        #endregion

        #region Constructors

        public ObjectMapper()
        {
            _converters = new Dictionary<Tuple<Type, Type>, IConverter>();
        }

        #endregion

        #region Public methods

        public TDest Map<TSource, TDest>(TSource source, object param = null)
        {
            return FindConverter<TSource, TDest>().Convert(source, param);
        }

        public void RegisterConverter<TSource, TDest>(IObjectConverter<TSource, TDest> converter)
        {
            _converters.Add(new Tuple<Type, Type>(typeof(TSource), typeof(TDest)), converter);
        }

        #endregion

        #region Private methods

        private IObjectConverter<TSource, TDest> FindConverter<TSource, TDest>()
        {
            var key = new Tuple<Type, Type>(typeof(TSource), typeof(TDest));

            return (IObjectConverter<TSource, TDest>)_converters[key];
        }

        #endregion
    }
}
