﻿namespace TjSnow.Application.Mappers.Base
{
    public interface IObjectMapper
    {
        TDest Map<TSource, TDest>(TSource source, object param = null);
        
        void RegisterConverter<TSource, TDest>(IObjectConverter<TSource, TDest> converter);
    }
}
