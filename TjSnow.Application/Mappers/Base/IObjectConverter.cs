﻿namespace TjSnow.Application.Mappers.Base
{
    public interface IObjectConverter<TSource, TDest> : IConverter
    {
        TDest Convert(TSource source, object param = null);
    }
}
