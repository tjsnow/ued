﻿namespace TjSnow.Application.Mappers.Base
{
    /// <summary>
    /// Mark-up interface for object-to-object converter
    /// </summary>
    public interface IConverter { }
}
