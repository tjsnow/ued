﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Machine;
using MachineEntity = TjSnow.Model.Entities.Machine;

namespace TjSnow.Application.Mappers.Machine
{
    public class MachineToDtoConverter:IObjectConverter<MachineEntity,MachineDTO>
    {
        public MachineDTO Convert(MachineEntity source, object param = null)
        {
            return new MachineDTO
            {
                Id = source.Id,
                Name = source.Name,
                LocationId = source.LocationId,
                LocationName = source.Location != null ? source.Location.Name : null,
                KVA = source.KVA,
                ThroatDepth = source.ThroatDepth,
                Manufacturer = source.Manufacturer,
                SecAmps = source.SecAmps,
                SecVolts = source.SecVolts,
                AirType = source.AirType,
                CylinderDiameter = source.CylinderDiameter,
                MaxForce = source.MaxForce,
                HolderDiameter = source.HolderDiameter,
                Control = source.Control,
                Service = source.Service,
                PlatenUpper = source.PlatenUpper,
                PlatenLower = source.PlatenLower,
                TSlotSpacing = source.TSlotSpacing,
                TapSwitch = source.TapSwitch,
                CylinderStroke = source.CylinderStroke,
                ArmDiaLower = source.ArmDiaLower,
                ArmDiaUpper = source.ArmDiaUpper,
                HeadType = source.HeadType,
                UpperDrive = source.UpperDrive,
                LowerDrive = source.LowerDrive,
                WheelUpper = source.WheelUpper,
                WheelLower = source.WheelLower,
                Model = source.Model,
                Serial = source.Serial,
                FastShip = source.FastShip,
                Transformer = source.Transformer,
                PurchasedDate = source.PurchasedDate,
                PurchasedFrom = source.PurchasedFrom,
                Cost = source.Cost,
                Freight = source.Freight,
                Feautures = source.Feautures,
                CheckedBy = source.CheckedBy,
                Price = source.Price,
                SoldTo = source.SoldTo,
                DateSold = source.DateSold,
                UEDFilePath = source.UEDFilePath,
                WebDescription = source.WebDescription,
                EnteredDate = source.EnteredDate,
                StockNumber = source.StockNumber,
                P21ItemId = source.P21ItemId,
                PublishedToWeb = source.PublishedToWeb,
                InternalNotes = source.InternalNotes,
                ExcludeFromReconciliation = source.ExcludeFromReconciliation
                
            };
        }
    }
}
