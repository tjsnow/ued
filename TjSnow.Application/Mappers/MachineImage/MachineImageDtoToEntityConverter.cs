﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.MachineImage;
using MachineImageEntity = TjSnow.Model.Entities.MachineImage;

namespace TjSnow.Application.Mappers.MachineImage
{
    public class MachineImageDtoToEntityConverter : IObjectConverter<MachineImageDTO,MachineImageEntity>
    {
        public MachineImageEntity Convert(MachineImageDTO source, object param = null)
        {
            return new MachineImageEntity
            {
                Id = source.Id,
                MachineId = source.MachineId,
                ContentType = source.ContentType,
                Url = source.Url,
                IsPublished = source.IsPublished,
                SortOrder = source.SortOrder,
                FileName = source.FileName
            };
        }
    }
}
