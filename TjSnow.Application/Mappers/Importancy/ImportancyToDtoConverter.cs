﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Importancy;
using ImportancyEntity = TjSnow.Model.Entities.Importancy;

namespace TjSnow.Application.Mappers.Importancy
{
    public class ImportancyToDtoConverter:IObjectConverter<ImportancyEntity,ImportancyDTO>
    {
        public ImportancyDTO Convert(ImportancyEntity source, object param = null)
        {
            return (ImportancyDTO) source;
        }
    }
}
