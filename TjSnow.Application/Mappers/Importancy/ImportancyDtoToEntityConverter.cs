﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Application.Mappers.Base;
using TjSnow.DTO.Importancy;
using ImportancyEntity = TjSnow.Model.Entities.Importancy;

namespace TjSnow.Application.Mappers.Importancy
{
    public class ImportancyDtoToEntityConverter : IObjectConverter<ImportancyDTO, ImportancyEntity>
    {
        public ImportancyEntity Convert(ImportancyDTO source, object param = null)
        {
            return (ImportancyEntity) source;
        }
    }
}
