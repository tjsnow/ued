﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TjSnow.Application.Mappers.Base;
using TjSnow.Application.Services.Search;
using TjSnow.DTO.Search;
using TjSnow.DTO.Voltage;
using TjSnow.Infrastructure.Utils;
using TjSnow.Persistence.Repositories.Category;

namespace TjSnow.Application.Mappers.Search
{
    public class MachineDocumentToDtoConverter : IObjectConverter<MachineDocument, MachineSearchDTO>
    {
        private readonly ICategoryRepository _categoryRepository;

        public MachineDocumentToDtoConverter(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public MachineSearchDTO Convert(MachineDocument source, object param = null)
        {
            var dto = new MachineSearchDTO
            {
                Id = int.Parse(source.Id),
                Thumbnail = source.Thumbnail,
                Name = source.Name,
                Voltage = source.Voltage.Length > 0 ? (VoltageDTO)int.Parse(source.Voltage[0]) : (VoltageDTO?)null,
                Phase = source.Phase,
                Description = source.Description,
                KVA = (decimal?) source.KVA,
                Categories = new List<CategorySearchDTO>(source.Category.Select(elem =>
                {
                    var category = _categoryRepository.GetById(int.Parse(elem));
                    return new CategorySearchDTO {Id = category.Id, Name = category.Name};
                })),
                Status = source.Status,
                Price = (decimal?) source.Price,
                StockNumber = source.StockNumber,
                PhaseDescription = source.Phase.HasValue ? source.Phase.Value.GetEnumDescription() : null,
                StatusDescription = source.Status.GetEnumDescription(),
                SecondaryAmps = source.SecondaryAmps,
                FastShip = source.FastShip,
                Manufacturer = source.Manufacturer,
                Model = source.Model,
                PublishedToWeb = source.PublishedToWeb
            };

            dto.VoltageDescription = dto.Voltage.HasValue ? dto.Voltage.GetEnumDescription() : null;

            return dto;
        }
    }
}