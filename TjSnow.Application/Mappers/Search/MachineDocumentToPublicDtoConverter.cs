﻿using TjSnow.Application.Mappers.Base;
using TjSnow.Application.Services.Search;
using TjSnow.DTO.Search;
using TjSnow.DTO.Voltage;
using TjSnow.Infrastructure.Utils;

namespace TjSnow.Application.Mappers.Search
{
    public class MachineDocumentToPublicDtoConverter : IObjectConverter<MachineDocument, MachinePublicSearchDTO>
    {
        public MachinePublicSearchDTO Convert(MachineDocument source, object param = null)
        {
            var dto = new MachinePublicSearchDTO
            {
                Id = int.Parse(source.Id),
                Thumbnail = source.Thumbnail,
                Name = source.Name,
                Voltage = source.Voltage.Length > 0 ? (VoltageDTO)int.Parse(source.Voltage[0]) : (VoltageDTO?)null,
                Phase = source.Phase,
                KVA = (decimal?)source.KVA,
                PhaseDescription = source.Phase.HasValue ? source.Phase.Value.GetEnumDescription() : null,
                Model = source.Model,
                Description = source.Description,
                Manufacturer = source.Manufacturer,
                StockNumber = source.StockNumber,
                IsFastShip = source.FastShip
            };

            dto.VoltageDescription = dto.Voltage.HasValue ? dto.Voltage.GetEnumDescription() : null;

            return dto;
        }
    }
}