﻿namespace TjSnow.Application.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    public class BadRequestException : AppException
    {
        public BadRequestException() { }

        public BadRequestException(string message)
            : base(message) { }

        public BadRequestException(string message, Exception innerException)
            : base(message, innerException) { }

        protected BadRequestException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
