﻿namespace TjSnow.Application.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    public class AppException : ApplicationException
    {
        public AppException() { }

        public AppException(string message)
            : base(message) { }

        public AppException(string message, Exception innerException)
            : base(message, innerException) { }

        protected AppException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
