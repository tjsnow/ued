﻿
namespace TjSnow.Application.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    public class NotFoundException : AppException
    {
        public NotFoundException() { }

        public NotFoundException(string message)
            : base(message) { }

        public NotFoundException(string message, Exception innerException)
            : base(message, innerException) { }

        protected NotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
