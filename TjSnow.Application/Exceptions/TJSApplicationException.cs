﻿using System;

namespace TjSnow.Application.Exceptions
{
    public class TJSApplicationException:ApplicationException
    {
        public TJSApplicationException(string message) 
            : base(message)
        {

        }

        public TJSApplicationException(string message, Exception e)
            : base(message, e)
        {

        }
    }
}
