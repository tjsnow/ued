﻿namespace TjSnow.Application.Exceptions
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    using Infrastructure.Logging.Base;

    public class ExceptionHelper
    {
        #region Constants

        private const string MessageObjectNotFound = "Object id {0} of type {1} not found";

        //private const string ErrorInsufficientPrivilegesForType = "Requester {0} does not have {1} privilege for object {2} type";
        //private const string ErrorInsufficientPrivilegesForObject =
        //    "Requester {0} does not have {1} privilege for object {2} of {3} type";

        #endregion

        #region Fields and properties

        private static readonly ILogger Logger = LoggerFactory.CreateLog();

        #endregion

        #region Public methods

        public static NotFoundException CreateNotFoundException(int key, Type type)
        {
            var message = string.Format(MessageObjectNotFound, key, type.Name);

            Logger.LogError(message);

            return new NotFoundException(message);
        }

        public static NotFoundException CreateNotFoundException(string message)
        {
            Logger.LogError(message);

            return new NotFoundException(message);
        }

        public static AppException CreateAppException(string message)
        {
            Logger.LogError(message);

            return new AppException(message);
        }

        public static AppException CreateAppException(string message, Exception innerException)
        {
            Logger.LogError(message, innerException);

            return new AppException(message, innerException);
        }

        public static BadRequestException CreateBadRequestException(string message)
        {
            Logger.LogError(message);

            return new BadRequestException(message);
        }

        public static void ThrowHttpResponseException(HttpStatusCode httpStatusCode, string reasonPhrase, string content)
        {
            var response = new HttpResponseMessage(httpStatusCode)
            {
                Content = new StringContent(content),
                ReasonPhrase = reasonPhrase
            };

            Logger.LogError(content);

            throw new HttpResponseException(response);
        }

        //public static AppSecurityException CreateSecurityException(
        //    int requesterId,
        //    object targetId,
        //    Privilege privilege,
        //    Type entityType)
        //{
        //    var message = string.Format(
        //        ErrorInsufficientPrivilegesForObject,
        //        requesterId,
        //        targetId,
        //        privilege.Name,
        //        entityType.Name);

        //    return new AppSecurityException(message,
        //        null,
        //        requesterId,
        //        targetId,
        //        privilege.Name,
        //        entityType.Name);
        //}

        //public static AppSecurityException CreateSecurityException(
        //    int requesterId,
        //    Privilege privilege,
        //    Type entityType)
        //{
        //    var message = string.Format(
        //        ErrorInsufficientPrivilegesForType,
        //        requesterId,
        //        privilege.Name,
        //        entityType.Name);

        //    return new AppSecurityException(
        //        message,
        //        null,
        //        requesterId,
        //        null,
        //        privilege.Name,
        //        entityType.Name);
        //}

        #endregion
    }
}
