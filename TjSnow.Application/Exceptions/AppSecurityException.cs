﻿namespace TjSnow.Application.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    public class AppSecurityException : AppException
    {
        public AppSecurityException() { }

        public AppSecurityException(string message)
            : base(message) { }

        public AppSecurityException(string message, Exception innerException)
            : base(message, innerException) { }

        protected AppSecurityException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
