﻿CREATE TABLE [dbo].[UserProfiles]
(
	[UserId]		INT				NOT NULL,
	[FirstName]		VARCHAR(32)		NOT NULL, 
    [LastName]		VARCHAR(32)		NOT NULL, 

    CONSTRAINT [PK_UserProfiles] PRIMARY KEY CLUSTERED ([UserId] ASC), 
    CONSTRAINT [FK_UserProfiles_Users] FOREIGN KEY ([UserId]) REFERENCES [Users]([Id]) ON DELETE CASCADE
)
