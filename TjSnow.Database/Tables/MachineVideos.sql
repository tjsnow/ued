﻿CREATE TABLE [dbo].[MachineVideos]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[MachineId] INT NOT NULL,
	[Url] VARCHAR(128) NOT NULL, 
    CONSTRAINT [FK_MachineVideos_ToMachines] FOREIGN KEY ([MachineId]) REFERENCES [dbo].[Machines]([Id])
)
