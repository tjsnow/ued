﻿CREATE TABLE [dbo].[MachineCategory]
(
	[MachineId] INT NOT NULL, 
    [CategoryId] INT NOT NULL, 
    CONSTRAINT [PK_MachineCategory] PRIMARY KEY ([MachineId], [CategoryId]), 
    CONSTRAINT [FK_MachineCategory_ToMachines] FOREIGN KEY ([MachineId]) REFERENCES [dbo].[Machines]([Id]),
	CONSTRAINT [FK_MachineCategory_ToCategories] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Categories]([Id])
)
