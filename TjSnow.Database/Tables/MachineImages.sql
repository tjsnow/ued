﻿CREATE TABLE [dbo].[MachineImages]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[MachineId] INT NOT NULL,
	[Url] VARCHAR(128) NOT NULL, 
    [ContentType] VARCHAR(64) NOT NULL, 
    [IsPublished] BIT NOT NULL DEFAULT 0, 
    [SortOrder] INT NOT NULL DEFAULT 0, 
    [FileName] VARCHAR(128) NOT NULL DEFAULT '', 
    CONSTRAINT [FK_MachineImages_ToMachines] FOREIGN KEY ([MachineId]) REFERENCES [dbo].[Machines]([Id])
)
