﻿CREATE TABLE [dbo].[Users]
(
	[Id]							INT IDENTITY	NOT NULL,
	[PasswordHash]					NVARCHAR(max)	NULL,
	[SecurityStamp]					NVARCHAR(max)	NULL,
	[TwoFactorEnabled]				BIT				NOT NULL,
	[LockoutEndDateUtc]				DATETIME		NULL,
	[LockoutEnabled]				BIT				NOT NULL,
	[AccessFailedCount]				INT				NOT NULL,
    [Email]							VARCHAR(64)		NOT NULL, 
	[EmailConfirmed]				BIT				NOT NULL,
    [PhoneNumber]					VARCHAR(16)		NULL, 
    [PhoneNumberConfirmed]			BIT				NOT NULL DEFAULT 0, 
    [UserName]						NVARCHAR(256)	NOT NULL, 
	[IsActive]						BIT				NOT NULL DEFAULT 1,

	CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UK_Users_UserName] UNIQUE NONCLUSTERED ([UserName] ASC),
)
