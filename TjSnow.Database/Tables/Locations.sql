﻿CREATE TABLE [dbo].[Locations]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] VARCHAR(64) NOT NULL
)

GO

CREATE UNIQUE INDEX [IX_Locations_Name] ON [dbo].[Locations] ([Name])
