﻿CREATE TABLE [dbo].[UserClaims]
(
	[Id]			INT IDENTITY	NOT NULL, 
    [UserId]		INT				NOT NULL, 
    [ClaimType]		NVARCHAR(MAX)	NULL, 
    [ClaimValue]	NVARCHAR(MAX)	NULL,

	CONSTRAINT [PK_UserClaims] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserClaims_User] FOREIGN KEY ([UserId]) REFERENCES [Users]([Id]) ON DELETE CASCADE,
)
