﻿CREATE TABLE [dbo].[UserLogins]
(
	[UserId]		INT				NOT NULL, 
    [LoginProvider]	NVARCHAR(128)	NOT NULL, 
    [ProviderKey]	NVARCHAR(128)	NOT NULL, 

    CONSTRAINT [PK_UserLogins] PRIMARY KEY CLUSTERED ([UserId] ASC, [LoginProvider] ASC, [ProviderKey] ASC), 
    CONSTRAINT [FK_UserLogins_Users] FOREIGN KEY ([UserId]) REFERENCES [Users]([Id]) ON DELETE CASCADE,
)

