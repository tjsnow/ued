﻿GO

UPDATE [dbo].[Machines] SET Voltage = 32 where Voltage = 64;

GO

UPDATE [dbo].[Machines] SET Voltage = 4 where Voltage = 8;
GO