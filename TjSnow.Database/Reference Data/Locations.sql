﻿-- Reference Data for Locations 

SET IDENTITY_INSERT [dbo].[Locations] ON 
GO

MERGE INTO [dbo].[Locations] AS Target 
USING (VALUES 
  (1, 'High Bay'),
  (2, 'Other'),
  (3, 'Outside 1')
) 
AS Source (Id, Value) 
ON Target.Id = Source.Id 
-- update matched rows
WHEN MATCHED THEN 
UPDATE SET Name = Source.Value 
-- insert new rows 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (Id, Name) 
VALUES (Id, Value) 
-- delete rows that are in the target but not the source 
WHEN NOT MATCHED BY SOURCE THEN 
DELETE;
GO

SET IDENTITY_INSERT [dbo].[Locations] OFF 
GO

/*INSERT INTO [dbo].[Locations]
           ([Name])
     VALUES
           ('Location1'),
           ('Location2'),
           ('Location3')
GO*/
