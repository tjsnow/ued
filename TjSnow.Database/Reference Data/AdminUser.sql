﻿--	Adding admin account, password: Qwerty1 

SET IDENTITY_INSERT [dbo].[Users] ON 
GO

MERGE INTO [dbo].[Users] AS Target 
USING (VALUES 
  (1, 'AI46JG4ERbfzhQhnJcAGe5phm5GrHJtSa4cCgDnwMvEh0AIXQYK4crm96a4I/QoIbA==', 'd607210b-53c4-49b4-8cc3-c673d8b0d8b3', 0, 1, 0, 'admin@tjsnow.com', 1, 1, 'admin@tjsnow.com')
) 
AS Source (Id, PasswordHash, SecurityStamp, TwoFactorEnabled, LockoutEnabled, AccessFailedCount, Email, EmailConfirmed, PhoneNumberConfirmed, UserName) 
ON Target.Id = Source.Id OR Target.UserName = Source.Username
-- update matched rows
WHEN MATCHED THEN 
UPDATE SET 
  PasswordHash = Source.PasswordHash,
  SecurityStamp = Source.SecurityStamp,
  EmailConfirmed = Source.EmailConfirmed
-- insert new rows 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (Id, PasswordHash, SecurityStamp, TwoFactorEnabled, LockoutEnabled, AccessFailedCount, Email, EmailConfirmed, PhoneNumberConfirmed, UserName) 
VALUES (Id, PasswordHash, SecurityStamp, TwoFactorEnabled, LockoutEnabled, AccessFailedCount, Email, EmailConfirmed, PhoneNumberConfirmed, UserName);
-- delete rows that are in the target but not the source 
--WHEN NOT MATCHED BY SOURCE THEN 
--DELETE;
GO

SET IDENTITY_INSERT [dbo].[Users] OFF 
GO

INSERT INTO [dbo].[UserProfiles] (UserId, FirstName, LastName)
  SELECT Id, '', ''
  FROM [dbo].[Users] u
  WHERE UserName = 'admin@tjsnow.com' AND NOT EXISTS (
	SELECT * FROM [dbo].[UserProfiles] p
	WHERE u.Id = p.UserId
  )
GO 

/*SET IDENTITY_INSERT [dbo].[Users].[Id] ON 
GO 
INSERT INTO [dbo].[Users] (Id, PasswordHash, SecurityStamp, TwoFactorEnabled, LockoutEnabled, AccessFailedCount, Email, EmailConfirmed, PhoneNumberConfirmed, UserName)
	VALUES(1, 'AI46JG4ERbfzhQhnJcAGe5phm5GrHJtSa4cCgDnwMvEh0AIXQYK4crm96a4I/QoIbA==', 'd607210b-53c4-49b4-8cc3-c673d8b0d8b3', 0, 1, 0, 'admin@tjsnow.com', 1, 1, 'admin@tjsnow.com');
GO
SET IDENTITY_INSERT [dbo].[Users].[Id] OFF 
GO 
INSERT INTO [dbo].[UserProfiles] (UserId, FirstName, LastName)
	VALUES(1, '', '');
GO*/