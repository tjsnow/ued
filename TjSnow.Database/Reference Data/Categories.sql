﻿-- Reference Data for Categories
SET IDENTITY_INSERT [dbo].[Categories] ON 
GO
 
MERGE INTO [dbo].[Categories] AS Target 
USING (VALUES 
  (1, 'Press Spot', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 2,'ued_pressspot_heavy.jpg'),
  (2, 'Projection/ Combination', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'ued_projection.jpg'),
  (3, 'Seam', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 0, 2, 2, 0, 0, 0, 2, 4, 0, 0, 2, 2, 2, 2, 2, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'ued_seam.jpg'),
  (4, 'Rocker Arm', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'ued_rocker.jpg'),
  (5, 'Three Phase', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'ued_3phase.jpg'),
  (6, 'Bench', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'ued_bench.jpg'),
  (7, 'Micro', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 0, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'ued_micro.jpg'),
  (8, 'Special Design', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'ued_special.jpg'),
  (9, 'Butt Welders', 2, 2, 2, 0, 2, 2, 2, 2, 2, 4, 0, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'ued_buttwelder.jpg'),
  (10, 'Dial Indexer', 2, 2, 2, 0, 2, 2, 2, 2, 2, 4, 0, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'ued_dial.jpg'),
  (11, 'Flash Butt', 2, 2, 2, 0, 2, 2, 2, 2, 2, 4, 0, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'ued_flash-butt.jpg'),
  (12, 'Portable Gun', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'uedportable_gun.jpg'),
  (13, 'Robot & Fixture Guns', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'ued_transgun.jpg'),
  (14, 'Coil Joining Welders', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'ued_coiljoining.jpg'),
  (15, 'Miscellaneous', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4,'misc_ued.jpg')
) 
AS Source (Id, Name, Voltage, Phase, KVA, ThroatDepth, Manufacturer, SecAmps, SecVolts, AirType, CylinderDiameter, MaxForce, HolderDiameter, Control, Service, PlatenUpper, PlatenLower, TSlotSpacing, TapSwitch, CylinderStroke, ArmDiaLower, ArmDiaUpper, HeadType, UpperDrive, LowerDrive, WheelUpper, WheelLower, Model, Serial, Initiation, FastShip, Transformer, PurchasedDate, PurchasedFrom, Condition, Cost, Freight, Feautures, Location, CheckedBy, Price, SoldTo, DateSold, UEDFilePath, Photo, Video, EnteredDate, WebDescription, StockNumber, P21ItemId,CategoryImage) 
ON Target.Id = Source.Id 
-- update matched rows
WHEN MATCHED THEN 
UPDATE SET 
  Name = Source.Name,				
  Voltage = Source.Voltage,
  Phase = Source.Phase,
  KVA = Source.KVA,
  ThroatDepth = Source.ThroatDepth,
  Manufacturer = Source.Manufacturer,
  SecAmps = Source.SecAmps,
  SecVolts = Source.SecVolts,
  AirType = Source.AirType,
  CylinderDiameter = Source.CylinderDiameter,
  MaxForce = Source.MaxForce,
  HolderDiameter = Source.HolderDiameter,
  Control = Source.Control,
  Service = Source.Service,
  PlatenUpper = Source.PlatenUpper,
  PlatenLower = Source.PlatenLower,
  TSlotSpacing = Source.TSlotSpacing,
  TapSwitch = Source.TapSwitch,
  CylinderStroke = Source.CylinderStroke,
  ArmDiaLower = Source.ArmDiaLower,
  ArmDiaUpper = Source.ArmDiaUpper,
  HeadType = Source.HeadType,
  UpperDrive = Source.UpperDrive,
  LowerDrive = Source.LowerDrive,
  WheelUpper = Source.WheelUpper,
  WheelLower = Source.WheelLower,
  Model = Source.Model,
  Serial = Source.Serial,
  Initiation = Source.Initiation,
  FastShip = Source.FastShip,
  Transformer = Source.Transformer,
  PurchasedDate = Source.PurchasedDate,
  PurchasedFrom = Source.PurchasedFrom,
  Condition = Source.Condition,
  Cost = Source.Cost,
  Freight = Source.Freight,
  Feautures = Source.Feautures,
  Location = Source.Location,
  CheckedBy = Source.CheckedBy,
  Price = Source.Price,
  SoldTo = Source.SoldTo,
  DateSold = Source.DateSold,
  UEDFilePath = Source.UEDFilePath,
  Photo = Source.Photo,
  Video = Source.Video,
  EnteredDate = Source.EnteredDate,
  WebDescription = Source.WebDescription,
  StockNumber = Source.StockNumber,
  P21ItemId = Source.P21ItemId,
  CategoryImage = Source.CategoryImage
-- insert new rows 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (Id, Name, Voltage, Phase, KVA, ThroatDepth, Manufacturer, SecAmps, SecVolts, AirType, CylinderDiameter, MaxForce, HolderDiameter, Control, Service, PlatenUpper, PlatenLower, TSlotSpacing, TapSwitch, CylinderStroke, ArmDiaLower, ArmDiaUpper, HeadType, UpperDrive, LowerDrive, WheelUpper, WheelLower, Model, Serial, Initiation, FastShip, Transformer, PurchasedDate, PurchasedFrom, Condition, Cost, Freight, Feautures, Location, CheckedBy, Price, SoldTo, DateSold, UEDFilePath, Photo, Video, EnteredDate, WebDescription, StockNumber, P21ItemId,CategoryImage) 
VALUES (Id, Name, Voltage, Phase, KVA, ThroatDepth, Manufacturer, SecAmps, SecVolts, AirType, CylinderDiameter, MaxForce, HolderDiameter, Control, Service, PlatenUpper, PlatenLower, TSlotSpacing, TapSwitch, CylinderStroke, ArmDiaLower, ArmDiaUpper, HeadType, UpperDrive, LowerDrive, WheelUpper, WheelLower, Model, Serial, Initiation, FastShip, Transformer, PurchasedDate, PurchasedFrom, Condition, Cost, Freight, Feautures, Location, CheckedBy, Price, SoldTo, DateSold, UEDFilePath, Photo, Video, EnteredDate, WebDescription, StockNumber, P21ItemId,CategoryImage) 
-- delete rows that are in the target but not the source 
WHEN NOT MATCHED BY SOURCE THEN 
DELETE;
GO

SET IDENTITY_INSERT [dbo].[Categories] OFF 
GO

/*
INSERT INTO [dbo].[Categories]
           ([Name]
           ,[Voltage]
           ,[Phase]
           ,[KVA]
           ,[ThroatDepth]
           ,[Manufacturer]
           ,[SecAmps]
           ,[SecVolts]
           ,[AirType]
           ,[CylinderDiameter]
           ,[MaxForce]
           ,[HolderDiameter]
           ,[Control]
           ,[Service]
           ,[PlatenUpper]
           ,[PlatenLower]
           ,[TSlotSpacing]
           ,[TapSwitch]
           ,[CylinderStroke]
           ,[ArmDiaLower]
           ,[ArmDiaUpper]
           ,[HeadType]
           ,[UpperDrive]
           ,[LowerDrive]
           ,[WheelUpper]
           ,[WheelLower]
           ,[Model]
           ,[Serial]
           ,[Initiation]
           ,[FastShip]
           ,[Transformer]
           ,[PurchasedDate]
           ,[PurchasedFrom]
           ,[Condition]
           ,[Cost]
           ,[Freight]
           ,[Feautures]
           ,[Location]
           ,[CheckedBy]
           ,[Price]
           ,[SoldTo]
           ,[DateSold]
           ,[UEDFilePath]
           ,[Photo]
           ,[Video]
           ,[EnteredDate]
           ,[WebDescription]
           ,[StockNumber]
           ,[P21ItemId])
     VALUES
           ('Press Spot', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 2),
           ('Projections & Projection Spot Welders', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4),
           ('Seam', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 0, 2, 2, 0, 0, 0, 2, 4, 0, 0, 2, 2, 2, 2, 2, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4),
           ('Rocker Arm', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4),
           ('Three Phase', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4),
           ('Bench', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4),
           ('Micro', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 0, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4),
           ('Special Design', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4),
           ('Butt Welders', 2, 2, 2, 0, 2, 2, 2, 2, 2, 4, 0, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4),
           ('Dial Indexer', 2, 2, 2, 0, 2, 2, 2, 2, 2, 4, 0, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4),
           ('Flash Butt', 2, 2, 2, 0, 2, 2, 2, 2, 2, 4, 0, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4),
           ('Portable Gun', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4),
           ('Robot Trans Gun &Fixture Guns', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 0, 0, 0, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4),
           ('Coil Joining Welders', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4),
           ('Miscellaneous', 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 4, 2, 2, 0, 0, 0, 0, 0, 2, 4, 2, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 4, 2, 2, 4)
GO
*/