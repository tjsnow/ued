﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Model.Entities;
using TjSnow.Model.Specifications.Base;

namespace TjSnow.Model.Specifications
{
    public static class MachineImageSpecifications
    {
        public static Specification<MachineImage> ByMachineId(int machineId)
        {
            return new DirectSpecification<MachineImage>(mi => mi.MachineId == machineId);
        }

        public static Specification<MachineImage> PublishedByMachineId(int machineId)
        {
            return new DirectSpecification<MachineImage>(mi=>mi.IsPublished && mi.MachineId == machineId);
        } 
    }
}
