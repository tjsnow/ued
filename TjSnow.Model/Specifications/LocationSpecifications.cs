﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TjSnow.Model.Entities;
using TjSnow.Model.Specifications.Base;

namespace TjSnow.Model.Specifications
{
    public static class LocationSpecifications
    {
        public static Specification<Location> ById(int id)
        {
            return new DirectSpecification<Location>(c => c.Id == id);
        }
    }
}
