﻿using TjSnow.Model.Entities;
using TjSnow.Model.Specifications.Base;

namespace TjSnow.Model.Specifications
{
    public static class CategorySpecifications
    {
        public static Specification<Category> ById(int id)
        {
            return new DirectSpecification<Category>(p => p.Id == id);
        }
    }
}