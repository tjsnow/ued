﻿using System;
using System.Linq.Expressions;

namespace TjSnow.Model.Specifications.Base
{
    /// <summary>
    /// Base contract for Specification pattern
    /// </summary>
    /// <typeparam name="TEntity">Type of entity</typeparam>
    public interface ISpecification<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Check if this specification is satisfied by a 
        /// specific expression lambda
        /// </summary>
        /// <returns></returns>
        Expression<Func<TEntity, bool>> SatisfiedBy();
    }
}