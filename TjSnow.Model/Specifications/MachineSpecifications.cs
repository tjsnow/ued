﻿using TjSnow.Model.Entities;
using TjSnow.Model.Specifications.Base;

namespace TjSnow.Model.Specifications
{
    public class MachineSpecifications
    {
        public static Specification<Machine> ByStockNumber(string stockNumber)
        {
            return new DirectSpecification<Machine>(elem => elem.StockNumber == stockNumber);
        } 
    }
}