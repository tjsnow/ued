﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.Model.Entities
{
    public enum Importancy
    {
        NotApplicable = 0,

        Users = 2,

        Administrators = 4
    }
}
