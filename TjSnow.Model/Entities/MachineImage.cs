﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.Model.Entities
{
    public class MachineImage
    {
        public int Id
        {
            get;
            set;
        }

        public int MachineId
        {
            get;
            set;
        }

        public string Url
        {
            get;
            set;
        }

        public string ContentType
        {
            get;
            set;
        }

        public bool IsPublished { get; set; }

        public int SortOrder { get; set; }

        public string FileName { get; set; }

        public virtual Machine Machine { get; set; }

    }
}
