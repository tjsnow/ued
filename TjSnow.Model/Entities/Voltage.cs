﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.Model.Entities
{
    public enum Voltage
    {
        [Description("120")]
        V110 = 1,

        [Description("208")]
        V208 = 2,

        [Description("220/230/240")]
        V220 = 4,

        [Description("380")]
        V380 = 16,

        [Description("440/460/480")]
        V440 = 32,
        
        [Description("Dual 220/440")]
        Dual220And440 = 128,

        [Description("550")]
        V550 = 256,

        [Description("600")]
        V600 = 512
    }
}
