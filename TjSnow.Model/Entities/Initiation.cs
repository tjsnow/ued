﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.Model.Entities
{
    public enum Initiation
    {
        [Description("Foot Switch")]
        FootSwitch = 1,

        [Description("Dual Palm Button")]
        DualPalmButton = 2,

        [Description("Foot Switch & Dual Palm Button")]
        FootSwitchAndDualPalmButton = 4
    }
}
