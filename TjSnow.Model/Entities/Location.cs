﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.Model.Entities
{
    public class Location:IEntity
    {
        public Location()
        {
            Machines = new List<Machine>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Machine> Machines { get; set; } 
    }
}
