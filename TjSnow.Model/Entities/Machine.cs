﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.Model.Entities
{
    public class Machine : IEntity
    {
        public Machine()
        {
            Categories = new Collection<Category>();
            Images = new Collection<MachineImage>();
            Videos = new Collection<MachineVideo>();
        }

        public int Id
        {
            get;
            set;
        }

        public string Name { get; set; }

        public int? LocationId { get; set; }
       
        public Voltage? Voltage
        {
            get;
            set;
        }

        public Phase? Phase
        {
            get;
            set;
        }

        public Status Status
        {
            get;
            set;
        }

        public decimal? KVA
        {
            get;
            set;
        }

        public string ThroatDepth
        {
            get;
            set;
        }

        public string Manufacturer
        {
            get;
            set;
        }

        public string SecAmps
        {
            get;
            set;
        }

        public string SecVolts
        {
            get;
            set;
        }

        public string AirType
        {
            get;
            set;
        }

        public decimal? CylinderDiameter
        {
            get;
            set;
        }

        public int? MaxForce
        {
            get;
            set;
        }

        public decimal? HolderDiameter
        {
            get;
            set;
        }

        public string Control
        {
            get;
            set;
        }

        public string Service
        {
            get;
            set;
        }

        public string PlatenUpper
        {
            get;
            set;
        }

        public string PlatenLower
        {
            get;
            set;
        }

        public string TSlotSpacing
        {
            get;
            set;
        }

        public string TapSwitch
        {
            get;
            set;
        }

        public string CylinderStroke
        {
            get;
            set;
        }

        public string ArmDiaLower
        {
            get;
            set;
        }

        public string ArmDiaUpper
        {
            get;
            set;
        }

        public string HeadType
        {
            get;
            set;
        }

        public string UpperDrive
        {
            get;
            set;
        }

        public string LowerDrive
        {
            get;
            set;
        }

        public string WheelUpper
        {
            get;
            set;
        }

        public string WheelLower
        {
            get;
            set;
        }

        public string Model
        {
            get;
            set;
        }

        public string Serial
        {
            get;
            set;
        }

        public Initiation? Initiation
        {
            get;
            set;
        }

        public bool? FastShip
        {
            get;
            set;
        }

        public string Transformer
        {
            get;
            set;
        }

        public DateTime? PurchasedDate
        {
            get;
            set;
        }

        public string PurchasedFrom
        {
            get;
            set;
        }

        public Condition? Condition
        {
            get;
            set;
        }

        public decimal? Cost
        {
            get;
            set;
        }

        public string Freight
        {
            get;
            set;
        }

        public string Feautures
        {
            get;
            set;
        }

        public Location Location
        {
            get;
            set;
        }

        public string CheckedBy
        {
            get;
            set;
        }

        public decimal? Price
        {
            get;
            set;
        }

        public string SoldTo
        {
            get;
            set;
        }

        public DateTime? DateSold
        {
            get;
            set;
        }

        public string UEDFilePath
        {
            get;
            set;
        }

        public string WebDescription { get; set; }

        public DateTime? EnteredDate
        {
            get;
            set;
        }

        public string StockNumber { get; set; }

        public string P21ItemId { get; set; }

        public bool PublishedToWeb { get; set; }

        public string InternalNotes { get; set; }

        public bool? ExcludeFromReconciliation { get; set; }

        public ICollection<Category> Categories { get; set; }

        public ICollection<MachineImage> Images { get; set; }

        public ICollection<MachineVideo> Videos { get; set; }

    }
}
