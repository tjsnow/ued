﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.Model.Entities
{
    public enum Phase
    {
        [Description("Single")]
        Single = 1,

        [Description("Three")]
        Three = 2
    }
}
