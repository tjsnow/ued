﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.Model.Entities
{
    public class Category : IEntity
    {
        public Category()
        {
            Machines = new Collection<Machine>();
        }

        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public Importancy Voltage
        {
            get;
            set;
        }

        public Importancy Phase
        {
            get;
            set;
        }

        public Importancy KVA
        {
            get;
            set;
        }

        public Importancy ThroatDepth
        {
            get;
            set;
        }

        public Importancy Manufacturer
        {
            get;
            set;
        }

        public Importancy SecAmps
        {
            get;
            set;
        }

        public Importancy SecVolts
        {
            get;
            set;
        }

        public Importancy AirType
        {
            get;
            set;
        }

        public Importancy CylinderDiameter
        {
            get;
            set;
        }

        public Importancy MaxForce
        {
            get;
            set;
        }

        public Importancy HolderDiameter
        {
            get;
            set;
        }

        public Importancy Control
        {
            get;
            set;
        }

        public Importancy Service
        {
            get;
            set;
        }

        public Importancy PlatenUpper
        {
            get;
            set;
        }

        public Importancy PlatenLower
        {
            get;
            set;
        }

        public Importancy TSlotSpacing
        {
            get;
            set;
        }

        public Importancy TapSwitch
        {
            get;
            set;
        }

        public Importancy CylinderStroke
        {
            get;
            set;
        }

        public Importancy ArmDiaLower
        {
            get;
            set;
        }

        public Importancy ArmDiaUpper
        {
            get;
            set;
        }

        public Importancy HeadType
        {
            get;
            set;
        }

        public Importancy UpperDrive
        {
            get;
            set;
        }

        public Importancy LowerDrive
        {
            get;
            set;
        }

        public Importancy WheelUpper
        {
            get;
            set;
        }

        public Importancy WheelLower
        {
            get;
            set;
        }

        public Importancy Model
        {
            get;
            set;
        }

        public Importancy Serial
        {
            get;
            set;
        }

        public Importancy Initiation
        {
            get;
            set;
        }

        public Importancy FastShip
        {
            get;
            set;
        }

        public Importancy Transformer
        {
            get;
            set;
        }

        public Importancy PurchasedDate
        {
            get;
            set;
        }

        public Importancy EnteredDate { get; set; }

        public Importancy PurchasedFrom
        {
            get;
            set;
        }

        public Importancy Condition
        {
            get;
            set;
        }

        public Importancy Cost
        {
            get;
            set;
        }

        public Importancy Freight
        {
            get;
            set;
        }

        public Importancy Feautures
        {
            get;
            set;
        }

        public Importancy Location
        {
            get;
            set;
        }

        public Importancy CheckedBy
        {
            get;
            set;
        }

        public Importancy Price
        {
            get;
            set;
        }

        public Importancy SoldTo
        {
            get;
            set;
        }

        public Importancy DateSold
        {
            get;
            set;
        }

        public Importancy WebDescription { get; set; }

        public Importancy UEDFilePath
        {
            get;
            set;
        }

        public Importancy Photo
        {
            get;
            set;
        }

        public Importancy Video
        {
            get;
            set;
        }

        public Importancy StockNumber { get; set; }

        public Importancy P21ItemId { get; set; }

        public string CategoryImage { get; set; }

        public virtual ICollection<Machine> Machines { get; set; }

    }
}
