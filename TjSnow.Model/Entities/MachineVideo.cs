﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.Model.Entities
{
    public class MachineVideo:IEntity
    {
        public int Id
        {
            get;
            set;
        }

        public int MachineId
        {
            get;
            set;
        }

        public string Url
        {
            get;
            set;
        }

        public virtual Machine Machine { get; set; }
    }
}
