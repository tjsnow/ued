﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace TjSnow.Model.Entities.UserAgg
{
    public class Role : IdentityRole<int, UserRole>
    {
        public Role() { }

        public Role(string name)
        {
            Name = name;
        }
    }
}