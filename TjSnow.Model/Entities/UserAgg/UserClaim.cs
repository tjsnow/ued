﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace TjSnow.Model.Entities.UserAgg
{
    public class UserClaim : IdentityUserClaim<int> { }
}