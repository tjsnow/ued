﻿using System.ComponentModel.DataAnnotations;

namespace TjSnow.Model.Entities.UserAgg
{
    public class UserProfile
    {
        [Key]
        public int UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

    }
}