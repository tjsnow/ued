﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace TjSnow.Model.Entities.UserAgg
{
    public class User : IdentityUser<int, UserLogin, UserRole, UserClaim>, IEntity
    {
        public virtual UserProfile Profile { get; set; }

        public User() { }

        public User(string email, string firstname, string lastname)
        {
            Email =
                UserName = email;
            Profile = new UserProfile
            {
                FirstName = firstname,
                LastName = lastname
            };
        }
    }
}