﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace TjSnow.Model.Entities.UserAgg
{
    public class UserLogin : IdentityUserLogin<int> { }
}