﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.Model.Entities
{
    public enum Condition
    {
        [Description("New")]
        New = 1,

        [Description("Good")]
        Good = 2,

        [Description("Fair")]
        Fair = 4,

        [Description("Poor")]
        Poor = 8
    }
}
