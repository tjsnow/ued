﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.Model.Entities
{
    public enum Status
    {
        [Description("Available")]
        Available = 1,

        [Description("Sold")]
        Sold = 2,

        [Description("Scrapped")]
        Scrapped = 4,

        [Description("Rented")]
        Rented = 8,

        [Description("Consigned")]
        Customer = 16
    }
}
