﻿using System.Web.Mvc;
using TjSnow.Application.Services.Search;
using TjSnow.Infrastructure.Messaging.Base.Handling;
using TjSnow.Model.Contracts.Events.MachineAgg;

namespace TjSnow.Web.EventHandlers
{
    public class MachineEventsHandler : 
        IEventHandler<MachineCreated>,
        IEventHandler<MachineUpdated>,
        IEventHandler<MachineDeleted>
    {
        public void Handle(MachineCreated @event)
        {
            GetSearchService().Add(@event.Machine);
        }

        public void Handle(MachineUpdated @event)
        {
            GetSearchService().Update(@event.Machine);
        }

        public void Handle(MachineDeleted @event)
        {
            GetSearchService().Delete(@event.Machine);
        }

        private ISearchService GetSearchService()
        {
            return (ISearchService) DependencyResolver.Current.GetService(typeof (ISearchService));
        }
    }
}