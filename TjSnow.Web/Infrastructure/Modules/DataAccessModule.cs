﻿using TjSnow.Persistence;
using TjSnow.Persistence.Repositories.Category;
using TjSnow.Persistence.Repositories.Location;
using TjSnow.Persistence.Repositories.Machine;
using TjSnow.Persistence.UnitOfWork;
using TjSnow.Persistence.UnitOfWork.Base;

namespace TjSnow.Web.Infrastructure.Modules
{
    using Autofac;
    using Persistence.Repositories.User;

    public class DataAccessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new UnitOfWork(c.Resolve<EFDbContext>())).As<IUnitOfWork>();

            // The app should create only one instance of IdentityDbContext for UnitOfWork and UserManager for common transactions, they also should be within a single ILifetimeScope.
            builder.RegisterType<EFDbContext>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<EFDbContext>().As<IDbContext>().InstancePerLifetimeScope();

            // Repositories
            builder.RegisterType<CategoryRepository>().As<ICategoryRepository>().InstancePerRequest();
            builder.RegisterType<MachineRepository>().As<IMachineRepository>().InstancePerRequest();
            builder.RegisterType<LocationRepository>().As<ILocationRepository>().InstancePerRequest();
        }
    }
}