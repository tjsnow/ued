﻿using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Owin;
using TjSnow.Model.Entities.UserAgg;
using TjSnow.Persistence;
using TjSnow.Persistence.Identity;
using TjSnow.Persistence.Repositories.User;

namespace TjSnow.Web.Infrastructure.Modules
{
    using System;
    using Application.Services.Email;
    using Autofac;
    using Autofac.Core;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security.DataProtection;
    using Owin;
    using Persistence.Repositories;

    public class IdentityModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // User Manager
            builder.Register(c => new ApplicationUserStore(c.Resolve<EFDbContext>())).As<IUserStore<User, int>>();
            builder.RegisterType<ApplicationUserManager>().OnActivated(CreateUserManager).InstancePerRequest();
            builder.RegisterType<ApplicationSignInManager>().InstancePerRequest();
            builder.RegisterType<UserRepository>().As<IUserRepository<User, int>>();
        }

        private void CreateUserManager(IActivatedEventArgs<ApplicationUserManager> eventArgs)
        {
            var context = eventArgs.Context;
            var userManager = eventArgs.Instance;

            // Configure user lockout defaults
            userManager.UserLockoutEnabledByDefault = true;
            userManager.MaxFailedAccessAttemptsBeforeLockout = 5;
            userManager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(20);

            // Configure validation logic for usernames
            userManager.UserValidator = new UserValidator<User, int>(userManager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            userManager.PasswordValidator = new PasswordValidator
            {
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonLetterOrDigit = false,
                RequireUppercase = true,
                RequiredLength = 6
            };

            //  Configure email service for manager
            userManager.EmailService = context.Resolve<IEmailService>();

            var dataProtectionProvider = context.Resolve<IAppBuilder>().GetDataProtectionProvider();
            if (dataProtectionProvider != null)
            {
                userManager.UserTokenProvider = new DataProtectorTokenProvider<User, int>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<User, int>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager)
            : base(userManager, HttpContext.Current.GetOwinContext().Authentication)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(User user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>());
        }
    }
}