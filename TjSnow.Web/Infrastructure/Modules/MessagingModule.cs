﻿using Autofac;
using TjSnow.Infrastructure.Messaging;
using TjSnow.Infrastructure.Messaging.Base;
using TjSnow.Infrastructure.Messaging.Base.Handling;
using TjSnow.Web.EventHandlers;

namespace TjSnow.Web.Infrastructure.Modules
{
    public class MessagingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // simple event bus made on IoC container
            builder.Register(c => new SimpleEventBus()).As<SimpleEventBus>().SingleInstance();

            builder.Register(c => c.Resolve<SimpleEventBus>()).As<IEventBus>();
            builder.Register(c => c.Resolve<SimpleEventBus>()).As<IEventHandlerRegistry>();  
            
            // event handlers
            builder.RegisterType<MachineEventsHandler>().As<IEventHandler>();

            base.Load(builder);
        }
    }
}