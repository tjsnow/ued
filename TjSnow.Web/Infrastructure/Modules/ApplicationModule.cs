﻿using TjSnow.Application.Mappers.Category;
using TjSnow.Application.Mappers.Condition;
using TjSnow.Application.Mappers.Importancy;
using TjSnow.Application.Mappers.Initiation;
using TjSnow.Application.Mappers.Location;
using TjSnow.Application.Mappers.Machine;
using TjSnow.Application.Mappers.MachineImage;
using TjSnow.Application.Mappers.MachineVideo;
using TjSnow.Application.Mappers.Phase;
using TjSnow.Application.Mappers.Search;
using TjSnow.Application.Mappers.Status;
using TjSnow.Application.Mappers.Voltage;
using TjSnow.Application.Services.AzureStorage;
using TjSnow.Application.Services.Category;
using TjSnow.Application.Services.Email;
using TjSnow.Application.Services.Location;
using TjSnow.Application.Services.Machine;
using TjSnow.Application.Services.Search;
using TjSnow.Application.Services.Settings;
using TjSnow.Application.Services.User;
using TjSnow.Infrastructure.Environment;
using TjSnow.Persistence.Repositories.Category;

namespace TjSnow.Web.Infrastructure.Modules
{
    using Application.Mappers.Base;
    using Application.Mappers.User;
    using Application.Services.Security;
    using Autofac;

    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Services
            builder.RegisterType<SecurityService>().As<ISecurityService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<SearchService>().As<ISearchService>();
            //builder.RegisterType<DummySearchService>().As<ISearchService>();
            builder.RegisterType<MachinesService>().As<IMachinesService>();
            builder.RegisterType<AzureStorageService>().As<IAzureStorageService>();
            builder.RegisterType<CategoryService>().As<ICategoryService>();
            builder.RegisterType<LocationService>().As<ILocationService>();
            builder.RegisterType<SettingsService>().As<ISettingsService>();

            // SMTP
            builder.Register<SmtpEmailService>(c => new SmtpEmailService(
                ConfigurationSettings.Get<string>("Smtp.Server"),
                ConfigurationSettings.Get<int>("Smtp.Port"),
                ConfigurationSettings.Get<bool>("Smtp.UseSsl"),
                ConfigurationSettings.Get<string>("Smtp.UserName"),
                ConfigurationSettings.Get<string>("Smtp.Password"),
                ConfigurationSettings.Get<string>("FromAddress"))).As<IEmailService>();

            builder.RegisterType<ObjectMapper>().As<IObjectMapper>().OnActivated(c =>
            {
                c.Instance.RegisterConverter(new CategoryDtoToEntityConverter());
                c.Instance.RegisterConverter(new CategoryToDtoConverter());
                c.Instance.RegisterConverter(new CategoryToShortDtoConverter());

                c.Instance.RegisterConverter(new ConditionDtoToEntityConverter());
                c.Instance.RegisterConverter(new ConditionToDtoConverter());

                c.Instance.RegisterConverter(new ImportancyToDtoConverter());
                c.Instance.RegisterConverter(new ImportancyDtoToEntityConverter());

                c.Instance.RegisterConverter(new InitiationDtoToEntityConverter());
                c.Instance.RegisterConverter(new InitiationToDtoConverter());

                c.Instance.RegisterConverter(new LocationToDtoConverter());
                c.Instance.RegisterConverter(new LocationDtoToEntityConverter());

                c.Instance.RegisterConverter(new MachineToDtoConverter());
                c.Instance.RegisterConverter(new MachineDtoToEntityConverter());
                c.Instance.RegisterConverter(new NewMachineWithMediaDtoToEntityConverter());

                c.Instance.RegisterConverter(new MachineImageDtoToEntityConverter());
                c.Instance.RegisterConverter(new MachineImageToDtoConverter());

                c.Instance.RegisterConverter(new MachineVideoToDtoConverter());
                c.Instance.RegisterConverter(new MachineVideoDtoToEntityConverter());

                c.Instance.RegisterConverter(new PhaseToDtoConverter());
                c.Instance.RegisterConverter(new PhaseDtoToEntityConverter());

                c.Instance.RegisterConverter(new StatusDtoToEntityConverter());
                c.Instance.RegisterConverter(new StatusToDtoConverter());

                c.Instance.RegisterConverter(new VoltageDtoToEntityConverter());
                c.Instance.RegisterConverter(new VoltageToDtoConverter());

                c.Instance.RegisterConverter(new MachineDocumentToDtoConverter(c.Context.Resolve<ICategoryRepository>()));
                c.Instance.RegisterConverter(new MachineDocumentToPublicDtoConverter());

                c.Instance.RegisterConverter(new UserToDtoConverter());
            });
            
            base.Load(builder);
        }
    }
}