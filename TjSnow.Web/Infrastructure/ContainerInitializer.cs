﻿using System.Collections.Generic;
using System.Web.Http;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using TjSnow.Infrastructure.Environment;
using TjSnow.Infrastructure.Logging.Base;
using TjSnow.Infrastructure.Logging.NLog;
using TjSnow.Infrastructure.Messaging.Base.Handling;

namespace TjSnow.Web.Infrastructure
{
    using System.Reflection;
    using System.Web;
    using System.Web.Mvc;
    using Autofac;
    using Microsoft.Owin;
    using Modules;
    using Owin;

    public class ContainerInitializer
    {
        #region Public methods

        public static IContainer Initialize(IAppBuilder app)
        {
            RegisterFactories();

            var builder = new ContainerBuilder();

            builder.RegisterInstance(app).As<IAppBuilder>().ExternallyOwned();

            // Modules
            builder.RegisterModule(new DataAccessModule());
            builder.RegisterModule(new IdentityModule());
            builder.RegisterModule(new ApplicationModule());
            builder.RegisterModule(new MessagingModule());

            // HTTP context
            builder.Register<HttpContext>(c => HttpContext.Current);
            builder.Register<IOwinContext>(c => HttpContext.Current.GetOwinContext());

            // -> MVC controllers
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            // ->  Web API controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // -> filters
            builder.RegisterFilterProvider();

            var container = builder.Build();

            InitializeMessaging(container);

            // set dependency resolver for WebApi
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // set dependency resolver for MVC
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            return container;
        }

        #endregion

        #region Private methods

        private static void InitializeMessaging(IContainer container)
        {
            var eventHandlers = container.Resolve<IEnumerable<IEventHandler>>();
            var eventHandlerRegistry = container.Resolve<IEventHandlerRegistry>();

            // TODO to refactor event handler registry to support factories if needed
            foreach (var eventHandler in eventHandlers)
            {
                eventHandlerRegistry.Register(eventHandler);
            }
        }

        private static void RegisterFactories()
        {
            if (ConfigurationSettings.Get<string>("LoggingMode") == "local")
            {
                LoggerFactory.SetCurrent(new OnPremisesNLogFactory());
            }
            else
            {
                LoggerFactory.SetCurrent(new AzureNLogFactory());
            }
        }

        #endregion
    }
}