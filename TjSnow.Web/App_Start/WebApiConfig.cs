﻿namespace TjSnow.Web.App_Start
{
    using System.Net;
    using System.Web.Http;
    using Application.Exceptions;
    using Filters;

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Filters.Add(new ApiExceptionFilter(typeof(AppException), HttpStatusCode.InternalServerError));
            config.Filters.Add(new ApiExceptionFilter(typeof(BadRequestException), HttpStatusCode.BadRequest));
            config.Filters.Add(new ApiExceptionFilter(typeof(NotFoundException), HttpStatusCode.NotFound));
            config.Filters.Add(new ApiExceptionFilter(typeof(AppSecurityException), HttpStatusCode.Forbidden));

            config.Filters.Add(new ApiResponseFilter());
        }
    }
}