﻿using System.Web;
using System.Web.Optimization;

namespace TjSnow.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/vendor/jquery-{version}.js",
                        "~/Scripts/spin.min.js",
                        "~/Scripts/jquery.spin.js",
                        "~/Scripts/vendor/jquery-ui.min.js",
                        "~/Scripts/DataTables/jquery.dataTables.js",
                        "~/Scripts/DataTables/dataTables.bootstrap.js",
                        "~/Scripts/DataTables/dataTables.fixedColumns.js",
                        "~/Scripts/vendor/jquery.jcarousel.min.js",
                        "~/Scripts/jquery.dataTables.columnFilter.js",
                        "~/Scripts/dataTables.tableTools.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/vendor/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/vendor/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/vendor/bootstrap.js",
                      "~/Scripts/vendor/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/Scripts/vendor/angular.js",
                        "~/Scripts/vendor/angular-route.js",
                        "~/Scripts/ng-breadcrumbs.js",
                        "~/Scripts/ui-bootstrap-0.12.1.min.js",
                        "~/Scripts/angular-datatables.js",
                        "~/Scripts/angular-datatables.bootstrap.js",
                        "~/Scripts/angular-datatables.fixedcolumns.js",
                        "~/Scripts/angular-datatables.columnfilter.min.js",
                        "~/Scripts/angular-datatables.tabletools.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                        "~/Scripts/app/config.js",
                        "~/Scripts/app/app.js",
                        "~/Scripts/app/services/api-service.js",
                        "~/Scripts/app/controllers/dashboardController.js",
                        "~/Scripts/app/controllers/machineController.js",
                        "~/Scripts/app/main/main-controller.js",
                        "~/Scripts/app/controllers/ResultsController.js",
                        "~/Scripts/app/users/add-user-controller.js",
                        "~/Scripts/app/controllers/EditMachineController.js",
                        "~/Scripts/app/machines/grid-view-controller.js",

                        "~/Scripts/app/services/file-service.js",
                        "~/Scripts/app/services/search-service.js",
                        "~/Scripts/app/services/common-service.js",
                        "~/Scripts/app/services/searchresult-service.js",
                
                        "~/Scripts/app/directives/dropdown.js",
                        "~/Scripts/app/directives/tags-dropdown.js",
                        "~/Scripts/app/directives/isnumber.js",
                        "~/Scripts/app/directives/isdecimal.js",
                        "~/Scripts/app/directives/sortable.js",
                        "~/Scripts/app/directives/keypressed.js",
                        "~/Scripts/app/directives/remote-validate.js",
                        "~/Scripts/app/directives/datepickerpattern.js",
                        "~/Scripts/app/services/settings-service.js"));

            bundles.Add(new ScriptBundle("~/bundles/front").Include(
                            "~/Scripts/vendor/jquery-{version}.js",
                            "~/Scripts/app/front/vendor/jquery.ikSelect.js",
                            "~/Scripts/app/front/app.js",
                            "~/Scripts/app/front/script.js",
                            "~/Scripts/app/front/vendor/jquery.jcarousel.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/front2").Include(
                            "~/Scripts/app/front/init.js"
                            ));


            bundles.Add(new StyleBundle("~/Content/css/Front").Include(
                        "~/Content/css/style.css"
                        ));




            bundles.Add(new StyleBundle("~/Content/bootstrap").Include("~/Content/bootstrap.min.css"));
      

            bundles.Add(new StyleBundle("~/Content/cssAdmin").Include(
                        "~/Content/site.css",
                        "~/Content/css/tjsnow_bootstrap_theme.css",
                        "~/Content/css/jcarousel.responsive.css",
                        "~/Content/DataTables/css/dataTables.tableTools.css"
                        ));


        }
    }
}
