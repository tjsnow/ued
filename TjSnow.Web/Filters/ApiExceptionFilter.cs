﻿using System.Web.Http;
using TjSnow.Infrastructure.Logging.Base;

namespace TjSnow.Web.Filters
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Web.Http.Filters;

    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        private static ILogger _logger = LoggerFactory.CreateLog();

        public Type ExceptionType { get; private set; }

        public HttpStatusCode StatusCode { get; private set; }

        public ApiExceptionFilter(Type exceptionType, HttpStatusCode statusCode)
        {
            this.ExceptionType = exceptionType;
            this.StatusCode = statusCode;
        }

        public override void OnException(HttpActionExecutedContext context)
        {
            if (_logger == null)
            {
                _logger = LoggerFactory.CreateLog();
            }

            if (ExceptionType.IsInstanceOfType(context.Exception))
            {
                _logger.LogError(context.Exception.Message, context.Exception);

                context.Response = new HttpResponseMessage(this.StatusCode)
                {
                    Content = new ObjectContent<ExceptionResult>(
                        new ExceptionResult { success = false, message = context.Exception.Message },
                        new JsonMediaTypeFormatter())
                    //Content = new StringContent(context.Exception.ToString()), // TODO to be updated on production
                    //ReasonPhrase = context.Exception.Message
                };

                //base.OnException(context);
                throw new HttpResponseException(context.Response);
            }
        }
    }

    public class ExceptionResult
    {
        public bool success { get; set; }

        public string message { get; set; }
    }
}