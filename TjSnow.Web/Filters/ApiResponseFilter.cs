﻿namespace TjSnow.Web.Filters
{
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Web.Http.Filters;
    using Newtonsoft.Json.Serialization;

    public class ApiResponseFilter : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext context)
        {
            if (context.ActionContext.ActionDescriptor.ActionName.StartsWith("Validate"))
            {
                return;
            }

            if (context.Exception == null && context.Response != null &&
                context.Response.IsSuccessStatusCode)
            {
                var content = context.Response.Content as ObjectContent;

                if (content != null)
                {
                    context.Response.Content = new ObjectContent<ResponseResult>(
                        new ResponseResult { success = true, data = content.Value },
                        new JsonMediaTypeFormatter { SerializerSettings = { ContractResolver = new LowercaseContractResolver() } });
                }
            }

            base.OnActionExecuted(context);
        }
    }

    public class LowercaseContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToLower();
        }
    }

    public class ResponseResult
    {
        public bool success { get; set; }

        public object data { get; set; }
    }
}