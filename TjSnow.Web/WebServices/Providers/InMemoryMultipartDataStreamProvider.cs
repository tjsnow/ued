﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TjSnow.Application.Exceptions;


namespace TjSnow.Web.WebServices.Providers
{
    public class InMemoryMultipartFormDataStreamProvider : MultipartStreamProvider
    {
        private const long MaxImageFileSize = 62914560;

        private readonly string[] _allowedImageExtension = new[] {"jpeg", "jpg", "png"};
        
        private FormCollection _formData = new FormCollection();
        private List<HttpContent> _fileContents = new List<HttpContent>();

        private Collection<bool> _isFormData = new Collection<bool>();

        private void ValidateFileRestrictions(HttpContent file)
        {
            if (file == null)
            {
                throw new BadRequestException("File not setted");
            }

            var fileExtension = GetFileExtension(file);

            if (!_allowedImageExtension.Contains(fileExtension))
            {
                throw new BadRequestException(string.Format("Files with extension '{0}' not supported",fileExtension));
            }

            if (file.Headers.ContentLength > MaxImageFileSize)
            {
                throw new BadRequestException(string.Format("File size should be less or equal {0} bytes",MaxImageFileSize));
            }
        }

        private string GetFileExtension(HttpContent file)
        {
            var fileName = file.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);

            var fileExtension = string.IsNullOrWhiteSpace(fileName) ? null : fileName.Split('.').Last().ToLower();
            return fileExtension;
        }

        private static string UnquoteToken(string token)
        {
            if (String.IsNullOrWhiteSpace(token))
            {
                return token;
            }

            if (token.StartsWith("\"", StringComparison.Ordinal) && token.EndsWith("\"", StringComparison.Ordinal) && token.Length > 1)
            {
                return token.Substring(1, token.Length - 2);
            }

            return token;
        }
        
        public FormCollection FormData
        {
            get { return _formData; }
        }
        
        public List<HttpContent> Files
        {
            get { return _fileContents; }
        }
        
        public async Task<FileData[]> GetFiles()
        {
            Files.ForEach(file=>ValidateFileRestrictions(file));

            return await Task.WhenAll(Files.Select(f => FileData.ReadFile(f)));
        }

        public override Stream GetStream(HttpContent parent, HttpContentHeaders headers)
        {
            ContentDispositionHeaderValue contentDisposition = headers.ContentDisposition;

            if (contentDisposition != null)
            {
                _isFormData.Add(String.IsNullOrEmpty(contentDisposition.FileName));

                return new MemoryStream();
            }
            
            throw new InvalidOperationException(string.Format("Did not find required '{0}' header field in MIME multipart body part..", "Content-Disposition"));
        }
        
        public override async Task ExecutePostProcessingAsync()
        {
            
            for (int index = 0; index < Contents.Count; index++)
            {
                if (_isFormData[index])
                {
                    HttpContent formContent = Contents[index];
                    
                    ContentDispositionHeaderValue contentDisposition = formContent.Headers.ContentDisposition;

                    string formFieldName = UnquoteToken(contentDisposition.Name) ?? String.Empty;
                    
                    string formFieldValue = await formContent.ReadAsStringAsync();

                    FormData.Add(formFieldName, formFieldValue);
                }
                else
                {
                    _fileContents.Add(Contents[index]);
                }
            }
        }
    }
}