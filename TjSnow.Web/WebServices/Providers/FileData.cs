using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace TjSnow.Web.WebServices.Providers
{
    public class FileData
    {
        public string FileName { get; set; }
        
        public string ContentType { get; set; }
        
        public byte[] Data { get; set; }
        
        public long Size { get { return (Data != null ? Data.LongLength : 0L); } }

        public static async Task<FileData> ReadFile(HttpContent file)
        {
            var data = await file.ReadAsByteArrayAsync();

            var result = new FileData()
            {
                FileName = FixFilename(file.Headers.ContentDisposition.FileName),
                ContentType = file.Headers.ContentType.ToString(),
                Data = data
            };

            return result;
        }
        
        private static string FixFilename(string original)
        {
            var result = original.Trim();
            
            if (result.StartsWith("\""))
            {
                result = result.TrimStart('"').TrimEnd('"');
            }
           
            if (result.Contains("\\"))

            {
                result = new FileInfo(result).Name;
            }

            return result;
        }
    }
}