﻿using TjSnow.Application.Services.User;

namespace TjSnow.Web.WebServices
{
    using System.Threading.Tasks;
    using System.Web.Http;
    using Application.Exceptions;
    using Application.Services;
    using Application.Services.Security;
    using DTO.User;
    using Models.Api.Request;
    using Resources;
    using TjSnow.Infrastructure.Logging.Base;

    public class UserController : ApiController
    {
        //private static readonly ILogger _logger = LoggerFactory.CreateLog();

        private readonly IUserService _userService;
        private readonly ISecurityService _securityService;

        public UserController(
            IUserService userService, 
            ISecurityService securityService)
        {
            _userService = userService;
            _securityService = securityService;
        }

        #region Authentication

        [HttpPost, AllowAnonymous]
        [Route("api/user/login")]
        public async Task Login(LoginRequest request)
        {
            if (!ModelState.IsValid)
                throw ExceptionHelper.CreateBadRequestException("Model not valid.");

            var success = await _securityService.SignInAsync(request.Username, request.Password, false);

            if (!success)
                throw ExceptionHelper.CreateBadRequestException("Failed to authenticate the user.");

            //return new WebServiceDefaultResponse(success);
        }

        [HttpPost, AllowAnonymous]
        [Route("api/user/logout")]
        public void Logout()
        {
            _securityService.SignOut();
        }

        #endregion

        #region Password management

        [HttpPost, AllowAnonymous]
        [Route("api/user/password/send")]
        public async Task ResetPassword(ResetPasswordRequest request)
        {
            if (!ModelState.IsValid)
                throw ExceptionHelper.CreateBadRequestException("Model not valid.");

            var user = await _userService.FindByNameAsync(request.Email);

            if (user == null)
                throw ExceptionHelper.CreateNotFoundException("User with specified email address not found.");

            var code = await _userService.GeneratePasswordResetTokenAsync(user.UserId);
            var callbackUrl = Url.Link("Default", new { controller = "Home", action = "ResetPassword", userId = user.UserId, code = code });

            await _userService.SendEmailForPasswordReset(user.UserId, callbackUrl);
        }

        /*[HttpPut, Authorize]
        [Route("api/user/password")]
        public async Task<WebServiceDefaultResponse> SetPassword(SetPasswordRequest request)
        {
            if (!ModelState.IsValid)
                return new WebServiceDefaultResponse(false);

            var userDTO = await _userService.FindUserAsync(_securityService.CurrentUserName, request.OldPassword);
            if (userDTO == null)
                return new WebServiceDefaultResponse(false);

            var result = await _userService.SetPasswordAsync(userDTO.UserId, request.NewPassword);
            return new WebServiceDefaultResponse(result.Succeeded);
        }*/

        #endregion
        
        [HttpPost]
        [Route("api/user")]
        public async Task<UserDTO> CreateUser(CreateUserRequest request)
        {
            if (!ModelState.IsValid)
                throw ExceptionHelper.CreateBadRequestException(MessageHelper.ModelNotValid);

            if (await _userService.IsEmailAlreadyTakenAsync(request.Email))
                throw ExceptionHelper.CreateBadRequestException(MessageHelper.EmailIsAlreadyTaken);

            var dto = new CreateUserDTO
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Password = request.Password,
            };

            var result = await _userService.CreateUserAsync(dto);
            return result;
        }
    }
}