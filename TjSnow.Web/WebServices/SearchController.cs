﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TjSnow.Application.Services.Category;
using TjSnow.Application.Services.Location;
using TjSnow.Application.Services.Search;
using TjSnow.DTO.Search;
using TjSnow.Infrastructure.Utils;
using TjSnow.Model.Entities;

namespace TjSnow.Web.WebServices
{
    public class SearchController : ApiController
    {
        private readonly ISearchService _searchService;
        private readonly ICategoryService _categoryService;
        private readonly ILocationService _locationService;

        public SearchController(
            ISearchService searchService,
            ICategoryService categoryService, ILocationService locationService)
        {
            _searchService = searchService;
            _categoryService = categoryService;
            _locationService = locationService;
        }

        [HttpGet]
        [Route("api/search")]
        public SearchResponse Search([FromUri]SearchRequest request)
        {
            if (request == null)
            {
                request = new SearchRequest(); // allow user not to specify at leat one search parameter
            }
            
            return _searchService.Search(request);
        }

        [HttpGet]
        [Route("api/publicsearch")]
        public PublicSearchResponse Search([FromUri]PublicSearchRequest request)
        {
            var response = _searchService.PublicSearch(request);

            var phases = GetSearchParamPhases();
            var voltages = GetSearchParamVoltages();
	    var status = GetSearchParamVoltages();

            response.Phases = phases;
            response.Voltages = voltages;
	    response.Status = status.Select(elem => new SearchParamStatusDTO { Label="Available", Value=1});

            return response;
        }

        [HttpGet]
        [Route("api/search/params")]
        public SearchParamsDTO SearchParams()
        {
            var phases = GetSearchParamPhases();
            var voltages = GetSearchParamVoltages();
            
            var statuses =((Status[]) Enum.GetValues(typeof(Status))).Select(
                elem => new SearchParamStatusDTO {Label = elem.GetEnumDescription(), Value = 1}).ToList();
            //statuses.Insert(0, new SearchParamStatusDTO {Label = "All statuses", Value = null});

            var categories =
                _categoryService.RetrieveCategories()
                    .Select(elem => new SearchParamCategoryDTO { Label = elem.Name, Value = elem.Id }).OrderBy(elem => elem.Label).ToList();

            var locations = _locationService.GetLocations().Select(elem => new SearchParamLocationDTO { Label = elem.Name, Value = elem.Id }).OrderBy(elem => elem.Label).ToList();

            var publishedToWebStatuses = new List<SearchParamPublishedToWebStatusDTO>
            {
                new SearchParamPublishedToWebStatusDTO { Label = "All machines", Value = null },
                new SearchParamPublishedToWebStatusDTO { Label = "Only Published to Web", Value = true },
                new SearchParamPublishedToWebStatusDTO { Label = "Not Published to Web", Value = false },
            };

            return new SearchParamsDTO
            {
                Phases = phases,
                Voltages = voltages,
                Categories = categories,
                Statuses = statuses,
                PublishedToWebStatuses = publishedToWebStatuses,
                Conditions = GetSearchParamConditions(),
                Initiations = GetSearchParamInitiations(),
                Locations = locations
            };
        }

        private static IEnumerable<SearchParamPhaseDTO> GetSearchParamPhases()
        {
            var phases =
                ((Phase[])Enum.GetValues(typeof(Phase))).Select(
                    elem => new SearchParamPhaseDTO { Label = elem.GetEnumDescription(), Value = (int)elem }).ToList();
            phases.Insert(0, new SearchParamPhaseDTO { Label = "All phases", Value = null });

            return phases;
        }

        private static IEnumerable<SearchParamVoltageDTO> GetSearchParamVoltages()
        {
            var voltages =
                ((Voltage[])Enum.GetValues(typeof(Voltage))).Select(
                    elem => new SearchParamVoltageDTO { Label = elem.GetEnumDescription(), Value = (int)elem }).ToList();

            return voltages;
        }

	private static IEnumerable<SearchParamStatusDTO> GetSearchParamStatuses()
	{
	    var statuses =
		    ((Status[])Enum.GetValues(typeof(Status))).Select(
		    elem => new SearchParamStatusDTO { Label = elem.GetEnumDescription(), Value = (int)elem }).ToList();
	    return statuses;
	}

        private static IEnumerable<SearchParamConditionDTO> GetSearchParamConditions()
        {
            var contitions =
                ((Condition[])Enum.GetValues(typeof(Condition))).Select(
                    elem => new SearchParamConditionDTO { Label = elem.GetEnumDescription(), Value = (int)elem }).ToList();

            return contitions;
        }

        private static IEnumerable<SearchParamInitiationDTO> GetSearchParamInitiations()
        {
            var initiations =
                ((Initiation[])Enum.GetValues(typeof(Initiation))).Select(
                    elem => new SearchParamInitiationDTO { Label = elem.GetEnumDescription(), Value = (int)elem }).ToList();

            return initiations;
        } 
    }
}
