﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json;
using TjSnow.Application.Services.AzureStorage;
using TjSnow.Application.Services.Machine;
using TjSnow.DTO.Machine;
using TjSnow.DTO.MachineImage;
using TjSnow.DTO.MachineVideo;
using TjSnow.Infrastructure.Logging.Base;
using TjSnow.Web.WebServices.Providers;

namespace TjSnow.Web.WebServices
{
    [System.Web.Http.Authorize]
    public class MachinesController : ApiController
    {
        private readonly IMachinesService _machinesService;
        private readonly IAzureStorageService _azureStorageService;

        public MachinesController(IMachinesService machinesService, IAzureStorageService azureStorageService)
        {
            _machinesService = machinesService;
            _azureStorageService = azureStorageService;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/machines")]
        public IEnumerable<MachineDTO> GetMachines()
        {
            return _machinesService.RetrieveMachines();
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/machines/{id}")]
        public MachineDTO GetMachineById(int id)
        {
            return _machinesService.GetMachineById(id);
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/machines/{id}")]
        public void DeleteMachine(int id)
        {
            _machinesService.DeleteMachine(id);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/machines")]
        public async Task<MachineDTO> CreateMachine()
        {
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }

                var provider =
                    await
                        Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(
                            new InMemoryMultipartFormDataStreamProvider());

                FormCollection formData = provider.FormData;

                var fileDataList = provider.GetFiles();

                var files = await fileDataList;

                if (formData["machine"] == null)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }

                var machine = JsonConvert.DeserializeObject<NewMachineDTO>(formData["machine"]);

                var machineWithMediaDto = new NewMachineWithMediaDTO
                {
                    LocationId = machine.LocationId,
                    KVA = machine.KVA,
                    Name = machine.Name,
                    ThroatDepth = machine.ThroatDepth,
                    Manufacturer = machine.Manufacturer,
                    SecAmps = machine.SecAmps,
                    SecVolts = machine.SecVolts,
                    AirType = machine.AirType,
                    CylinderDiameter = machine.CylinderDiameter,
                    MaxForce = machine.MaxForce,
                    HolderDiameter = machine.HolderDiameter,
                    Control = machine.Control,
                    Service = machine.Service,
                    PlatenUpper = machine.PlatenUpper,
                    PlatenLower = machine.PlatenLower,
                    TSlotSpacing = machine.TSlotSpacing,
                    TapSwitch = machine.TapSwitch,
                    CylinderStroke = machine.CylinderStroke,
                    ArmDiaLower = machine.ArmDiaLower,
                    ArmDiaUpper = machine.ArmDiaUpper,
                    HeadType = machine.HeadType,
                    UpperDrive = machine.UpperDrive,
                    LowerDrive = machine.LowerDrive,
                    WheelUpper = machine.WheelUpper,
                    WheelLower = machine.WheelLower,
                    Model = machine.Model,
                    Serial = machine.Serial,
                    FastShip = machine.FastShip,
                    Transformer = machine.Transformer,
                    PurchasedDate = machine.PurchasedDate,
                    PurchasedFrom = machine.PurchasedFrom,
                    Cost = machine.Cost,
                    Freight = machine.Freight,
                    Feautures = machine.Feautures,
                    CheckedBy = machine.CheckedBy,
                    Price = machine.Price,
                    SoldTo = machine.SoldTo,
                    DateSold = machine.DateSold,
                    UEDFilePath = machine.UEDFilePath,
                    WebDescription = machine.WebDescription,
                    StockNumber = machine.StockNumber,
                    P21ItemId = machine.P21ItemId,
                    PublishedToWeb = machine.PublishedToWeb,
                    Condition = machine.Condition,
                    Initiation = machine.Initiation,
                    Phase = machine.Phase,
                    Status = machine.Status,
                    Voltage = machine.Voltage,
                    Categories = machine.Categories,
                    InternalNotes = machine.InternalNotes,
                    ExcludeFromReconciliation = machine.ExcludeFromReconciliation
                };

                if (files != null)
                {
                    var imageDataDtos = new List<MachineImageDataDTO>();

                    foreach (var file in files)
                    {
                        imageDataDtos.Add(new MachineImageDataDTO
                        {
                            ContentType = file.ContentType,
                            Data = file.Data,
                            FileName = file.FileName
                        });
                    }

                    machineWithMediaDto.Images = _azureStorageService.UploadImagesToStorage(imageDataDtos);

                    for (int i = 0; i < machineWithMediaDto.Images.Count(); i++)
                    {
                        machineWithMediaDto.Images.ElementAt(i).IsPublished = machine.ImageFiles.ElementAt(i).IsPublished;
                        machineWithMediaDto.Images.ElementAt(i).SortOrder = machine.ImageFiles.ElementAt(i).SortOrder;
                    }
                }

                if (machine.Videos != null)
                {
                    machineWithMediaDto.Videos = machine.Videos.Select(video => new MachineVideoDTO
                    {
                        Url = video
                    });
                }

                return _machinesService.CreateMachine(machineWithMediaDto);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/machines/{id}")]
        public async Task UpdateMachine([FromUri] int id)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            var provider =
                await
                    Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(
                        new InMemoryMultipartFormDataStreamProvider());

            FormCollection formData = provider.FormData;

            var fileDataList = provider.GetFiles();

            var files = await fileDataList;

            if (formData["machine"] == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            var machine = JsonConvert.DeserializeObject<MachineDTO>(formData["machine"]);

            machine.Images = machine.Images.Where(image => image.Id != 0).ToList();

            machine.Id = id;

            if (files != null && files.Any())
            {
                var imageDataDtos = new List<MachineImageDataDTO>();

                foreach (var file in files)
                {
                    imageDataDtos.Add(new MachineImageDataDTO
                    {
                        ContentType = file.ContentType,
                        Data = file.Data,
                        FileName = file.FileName
                    });
                }

                var newImages = _azureStorageService.UploadImagesToStorage(imageDataDtos).ToList();

                for (int i = 0; i < newImages.Count; i++)
                {
                    newImages[i].IsPublished = machine.ImageFiles.ElementAt(i).IsPublished;
                    newImages[i].SortOrder = machine.ImageFiles.ElementAt(i).SortOrder;
                }

                machine.Images = machine.Images.Concat(newImages);
            }

            _machinesService.UpdateMachine(machine);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/machines/stocknumber")]
        public HttpResponseMessage ValidateStockNumber(string value)
        {
           var result = _machinesService.GetMachineByStockNumber(value) == null;

            return this.Request.CreateResponse(HttpStatusCode.OK, new { isValid = result, value = value });
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/machines/{machineId}/stocknumber")]
        public HttpResponseMessage ValidateStockNumber(string value, int machineId)
        {
           var machine = _machinesService.GetMachineByStockNumber(value);

            var result = machine == null || machine.Id == machineId;

            return this.Request.CreateResponse(HttpStatusCode.OK, new { isValid = result, value = value });
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/machines/{machineId}/related")]
        public IEnumerable<MachineDTO> GetLastRelatedMachines(int machineId)
        {
            return _machinesService.GetLastRelatedMachines(machineId);
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/machines/{id}/images")]
        public async Task UpdateMachinesImages([FromUri] int id)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            var provider =
                await
                    Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(
                        new InMemoryMultipartFormDataStreamProvider());

            FormCollection formData = provider.FormData;

            var fileDataList = provider.GetFiles();

            var files = await fileDataList;

            if (formData["machine"] == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            var machine = JsonConvert.DeserializeObject<MachineDTO>(formData["machine"]);

            machine.Images = machine.Images.Where(image => image.Id != 0).ToList();

            machine.Id = id;

            if (files != null && files.Any())
            {
                var imageDataDtos = new List<MachineImageDataDTO>();

                foreach (var file in files)
                {
                    imageDataDtos.Add(new MachineImageDataDTO
                    {
                        ContentType = file.ContentType,
                        Data = file.Data,
                        FileName = file.FileName
                    });
                }

                var newImages = _azureStorageService.UploadImagesToStorage(imageDataDtos).ToList();

                for (int i = 0; i < newImages.Count; i++)
                {
                    newImages[i].IsPublished = machine.ImageFiles.ElementAt(i).IsPublished;
                    newImages[i].SortOrder = machine.ImageFiles.ElementAt(i).SortOrder;
                }

                machine.Images = machine.Images.Concat(newImages);
            }

            _machinesService.UpdateMachineImages(machine);
        }
    }
}