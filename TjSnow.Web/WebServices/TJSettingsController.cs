﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TjSnow.Application.Services.Settings;

namespace TjSnow.Web.WebServices
{
    public class TJSettingsController:ApiController
    {
        private readonly ISettingsService _settingsService;

        public TJSettingsController(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        [HttpGet]
        [Route("api/settings")]
        public HttpResponseMessage GetTJSettings()
        {
            try
            {
                var settings = _settingsService.GetTjSettings();

                return Request.CreateResponse(HttpStatusCode.OK, settings,"application/json");
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new {ErrorMessage = e.Message},"application/json");
            }
        }
    }
}