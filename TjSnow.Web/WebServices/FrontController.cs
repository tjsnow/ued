﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using hbehr.recaptcha;
using Newtonsoft.Json.Linq;
using TjSnow.Application.Exceptions;
using TjSnow.Application.Services.Category;
using TjSnow.Application.Services.Email;
using TjSnow.Application.Services.Search;
using TjSnow.DTO.Category;
using TjSnow.DTO.Search;
using TjSnow.Infrastructure.Utils;
using TjSnow.Model.Entities;
using TjSnow.Web.Models.Api.Request;
using TjSnow.DTO.Machine;
using TjSnow.Application.Services.Machine;
using TjSnow.DTO.Email;

namespace TjSnow.Web.WebServices
{
    public class FrontController : ApiController
    {
        private readonly ISearchService _searchService;
        private readonly ICategoryService _categoryService;
        private readonly IEmailService _emailService;
        private readonly IMachinesService _machinesService;

        public FrontController(
            ISearchService searchService,
            ICategoryService categoryService,
            IEmailService emailService,
            IMachinesService machinesService)
        {
            _searchService = searchService;
            _categoryService = categoryService;
            _emailService = emailService;
            _machinesService = machinesService;
        }

        [HttpGet]
        [Route("api/categories")]
        public IEnumerable<CategoryShortDTO> GetCategories()
        {
            var categories =_categoryService.RetrieveCategories();
            return categories;
        }

        [HttpGet]
        [Route("api/public/machines/{id}")]
        public MachineDTO GetMachineById(int id)
        {
            return _machinesService.GetMachineById(id);
        }

        [HttpPost]
        [Route("api/feedback")]
        public async Task<HttpResponseMessage> SendRequest(FeedbackRequest request)
        {
            string userResponse = HttpContext.Current.Request.Params["g-recaptcha-response"];

            bool validCaptcha = ReCaptcha.ValidateCaptcha(userResponse);

            if (validCaptcha)
            {
                try
                {
                    var requestDTO = new FeedbackRequestDTO
                    {
                        City = request.City,
                        Company = request.Company,
                        Email = request.Email,
                        LastName = request.LastName,
                        MachineStockNumber = request.MachineStockNumber,
                        Message = request.Message,
                        Phone = request.Phone,
                        State = request.State,
                        PageUrl = Request.Headers.Referrer.AbsoluteUri
                    };

                    await _emailService.SendFeedback(requestDTO);
                    await _emailService.SendFeedbackConfiramation(requestDTO);

                    return Request.CreateResponse(HttpStatusCode.OK, string.Empty);
                }
                catch (Exception e)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Please confirm that you are not a robot.");
            }

            
        }
    }
}