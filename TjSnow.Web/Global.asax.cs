﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TjSnow.Web.App_Start;
using System;

namespace TjSnow.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = false;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (Request.CurrentExecutionFilePathExtension == ".html")
            {
                Response.AddHeader("Cache-Control", "max-age=0,no-cache,no-store,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "Tue, 01 Jan 1970 00:00:00 GMT");
            }
        }
    }
}
