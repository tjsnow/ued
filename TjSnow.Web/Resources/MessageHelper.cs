﻿namespace TjSnow.Web.Resources
{
    public static class MessageHelper
    {
        public static readonly string ModelNotValid = "Model not valid.";

        public static readonly string EmailIsAlreadyTaken = "Email is already taken.";

    }
}