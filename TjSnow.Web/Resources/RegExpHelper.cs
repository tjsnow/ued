﻿namespace TjSnow.Web.Resources
{
    public static class RegExpHelper
    {
        //public const string Password = @"((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W).{6,20})";
        public const string Password = @"((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";
    }
}