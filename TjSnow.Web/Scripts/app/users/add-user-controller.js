﻿angular.module('TjSnow.Users', ['ngRoute', 'ng-breadcrumbs', 'TjSnow.Api'])

.config(function ($routeProvider) {
    $routeProvider.when('/users/add', {
        controller: 'AddUserController',
        templateUrl: '/Scripts/app/users/add-user.html',
        label: 'Add user'
    });
})

.controller('AddUserController', ['$scope', 'breadcrumbs', '$location', 'ApiService',
  function ($scope, breadcrumbs, $location, api) {
      $scope.breadcrumbs = breadcrumbs;
      $scope.user = {};

      $scope.save = function () {
          if ($scope.user.password != $scope.user.password2) {
              $scope.error = "Passwords do not match.";
              return;
          }
          
          $('#add-user-form').fadeTo('slow', 0.5).spin();
          $('#add-user-form button').button('loading');

          api.post('/user', $scope.user).then(
              function (data) {
                  $('#add-user-form').fadeTo('slow', 1).spin(false);
                  $('#add-user-form button').button('reset');
                  
                  //alert("User added");
                  $scope.info = 'User successfully added';

                  setTimeout(function () { $location.path('/'); }, 4000);
              },
              function (er) {
                  $('#add-user-form').fadeTo('slow', 1).spin(false);
                  $('#add-user-form button').button('reset');

                  $scope.error = er.message ? er.message : er.Message;
              });
      }

      $scope.changed = function () {
          if ($scope.error) {
              $scope.error = null;
          }
      }
  }
]);