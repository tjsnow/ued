﻿app.directive('isNumber', function () {
    return {
        require: "^ngModel",
        scope: {
            ngModel: "=",
        },
        link: function (scope, element, attrs, ngModel) {
            scope.$watch(
                function () {
                    return ngModel.$modelValue;
                },
                function (newValue, oldValue) {
                    var arr = String(newValue).split("");
                    
                    if (arr.length === 0) {
                        return;
                    }
                    
                    if (arr.length === 1 && arr[0] === '.') {
                        return;
                    }
                    
                    if (isNaN(newValue)) {
                        scope.ngModel = oldValue;
                    } else {
                        scope.ngModel = Number(newValue);
                    }
                });
        }
    };
});