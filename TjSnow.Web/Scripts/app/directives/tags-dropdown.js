﻿app.directive('tagsDropdown', function () {
    return {
        restrict: "E",
        require: "^ngModel",
        scope: {
            items: "=",
            ngModel: "=",
            placeholder: "@",
            altPlaceholder: "@",
            isvalidatable: "=",
            isnotvalid: "="
        },
        template: [
            "<div class='btn-group tags'>",
                "<div type='button' class='btn btn-block btn-default dropdown-toggle' ng-class='{fieldrequirederror:(tags == null || tags.length == 0) &&  isvalidatable && isnotvalid}' data-toggle='dropdown' aria-expanded='false'>",
                    "<span class='tag' ng-repeat='(idx, tag) in tags track by $index'>{{tag.label}}<b ng-click='delete(idx, $event)' class='tag__close semibold'>×</b></span>",
                    "<span class='tags__placeholder' ng-class='{alt: tags.length > 0}'>{{tags.length > 0 ? altPlaceholder : placeholder}}</span>",
                    "<span class='caret'></span>",
                "</div>",
                "<ul class='dropdown-menu' role='menu'>",
                    "<li ng-repeat='item in items | filter:alreadyAdded'><a href='#' ng-click='add(item, $event)'>{{item.label}}</a></li>",
                "</ul>",
            "</div>",
            "<label class=\"error-message\" ng-show=\"(tags == null || tags.length == 0) &&  isvalidatable && isnotvalid\">This field is required and must be filled in</label>"
        ].join(""),
        link: function (scope, element, attrs, ngModel) {
            if (scope.ngModel) {
                scope.tags = scope.ngModel;
            } else {
                scope.tags = [];
            }

            scope.add = function (item, e) {
                e.preventDefault();
                scope.tags.push(item);
                scope.ngModel = scope.tags;
            };

            scope.delete = function(idx, e) {
                e.stopPropagation();
                scope.tags.splice(idx, 1);
            };

            scope.alreadyAdded = function (item) {

                return (scope.tags.indexOf(item) == -1);
            };
            
            scope.$watch(
                function () {
                    if (!!ngModel.$modelValue) {

                       if (scope.tags.length > 0) {
                           return ngModel.$modelValue;
                       }
                       
                       for (var i = 0; i < ngModel.$modelValue.length; i++) {
                           scope.tags.push(ngModel.$modelValue[i]);
                       }
                       scope.ngModel = scope.tags;
                    }
                    return ngModel.$modelValue;
                },
                function (newValue) {
                    if (newValue == null) { // reset support
                        scope.tags = [];
                    }
                }
            );
        }
    };
});