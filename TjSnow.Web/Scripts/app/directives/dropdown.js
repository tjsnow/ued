﻿app.directive('dropdown', function () {
    return {
        restrict: "E",
        require: "^ngModel",
        scope: {
            items: "=",
            ngModel: "=",
            placeholder: "@",
            btnClass: "@",
            type: "@",
            isvalidatable: "=",
            isnotvalid: "=",
            btnTabIndex: "@",
            onChanged:"&"

        },
        template: [
            "<div ng-blur=\"onBlur()\" class='btn-group select' ng-class=\"{'select_inline': type == 'split', 'select_empty': isEmpty, 'dropdownbuttonfieldrequirederror' :isnotvalid && isEmpty && isvalidatable}\">",
                "<button type='button' class='btn {{btnClass}}' ng-class=\"{true: 'dropdown-toggle'}[type != 'split']\" ng-attr-data-toggle=\"{{type != 'split' ? 'dropdown' : ''}}\" ng-attr-aria-expanded=\"{{type != 'split' ? 'false' : ''}}\">{{label}}<span class='caret' ng-if=\"type != 'split'\"></span></button>",
                "<button type='button' tabindex='{{btnTabIndex}}' class='btn {{btnClass}} dropdown-toggle' data-toggle='dropdown' aria-expanded='false' ng-if=\"type == 'split'\"><span class='caret'></span><span class='sr-only'>Toggle Dropdown</span></button>",
                "<ul class='dropdown-menu' role='menu'>",
                    "<li ng-repeat='item in items'><a style='min-height:26px;' href='#' ng-click='select(item, $event)'>{{item.label}}</a></li>",
                "</ul>",
            "</div>",
            "<div ><label class=\"error-message\" ng-show=\"isnotvalid && isEmpty && isvalidatable\">This field is required and must be filled in</label></div>"
        ].join(""),
        link: function (scope, element, attrs, ngModel) {
            scope.isEmpty = true;

            if (scope.placeholder) {
                scope.label = scope.placeholder;
            } else {
                if (scope.ngModel) {
                    for (var i = 0; i < scope.items.length; i++) {
                        if (scope.items[i].value == scope.ngModel) {
                            scope.label = scope.items[i].label;
                            break;
                        }
                    }
                } else {
                    scope.label = scope.items[0].label;
                    scope.ngModel = scope.items[0].value;
                }
                scope.isEmpty = false;
            }

            scope.select = function (item, e) {
                if (e) {
                    e.preventDefault();
                }

                if (item.value === null) {

                    if (item.label && item.label.length>0) {
                        scope.isEmpty = false;
                        scope.label = item.label;
                        scope.ngModel = null;
                    } else {
                        scope.isEmpty = true;
                        scope.label = scope.placeholder;
                        scope.ngModel = null;
                    }

                    if (scope.onChanged) {
                        scope.onChanged({ param: null });
                    }
                    
                } else {
                    scope.ngModel = item.value;
                    scope.label = item.label;
                    scope.isEmpty = false;
                    if (scope.onChanged) {
                        scope.onChanged({ param: scope.ngModel });
                    }
                }
            };

            scope.onBlur = function() {

                scope.isValid = !scope.isEmpty;
                if (!scope.isEmpty) {
                    scope.isnotvalid = false;
                }
            };

            scope.$watch(
                function () {
                     if (!!ngModel.$modelValue) {

                         scope.ngModel = ngModel.$modelValue;

                         for (var i =0;i< scope.items.length; i++) {
                             if (scope.items[i].value == ngModel.$modelValue) {
                                 scope.label = scope.items[i].label;
                                 break;
                             }
                         }
                         
                         scope.isEmpty = false;
                     }
                     
                     return ngModel.$modelValue;
                },
                function (newValue) {
                    if (newValue == null && !!scope.items) { // reset support

                        if (scope.items[0].label && scope.items[0].label.length > 0) {
                            scope.label = scope.items[0].label;
                            scope.ngModel = scope.items[0].value;
                        } else {
                            scope.label = scope.placeholder;//scope.items[0].label;
                            scope.ngModel = null;//scope.items[0].value;
                        }

                        
                    }

                    if (!scope.items || scope.items.filter(function (elem) {
                        return elem.value == newValue;
                    }).length <= 0) {
                        scope.isEmpty = true;
                        scope.label = scope.placeholder;
                        scope.ngModel = null;
                    }
                }
            );
        }
    };
});

