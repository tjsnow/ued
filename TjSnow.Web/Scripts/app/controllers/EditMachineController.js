﻿app.controller('EditMachineController', ['$scope', '$sce', 'searchService', 'fileService', '$http', 'breadcrumbs', '$routeParams', '$location', '$timeout','$rootScope','$window','CommonService',
    function ($scope, $sce, searchService, fileService, $http, breadcrumbs, $routeParams, $location, $timeout,$rootScope,$window,commonService) {
    $scope.machineId = $routeParams.machineId;
    $scope.machine = null;

    $scope.isPrimaryCollapsed = false;
    $scope.isCylinderSpecsCollapsed = false;
    $scope.isAdditionalMachineSpecsCollapsed = false;
    $scope.isSeamWelderCollapsed = false;
    $scope.isInternalInfoCollapsed = false;

    $scope.isMachinChanged = false;

    $scope.breadcrumbs = breadcrumbs;

    $scope.images = [];

    $scope.videos = [];

    $scope.currentVideoUrl = null;
    
    $scope.doFade = false;

    var updateImages = function() {

        var files = new Array;

        var imageFiles = new Array;

        var machine = {};

        machine.id = $scope.machine.id;

        for (var k = 0; k < $scope.machine.images.length; k++) {
            if (!$scope.machine.images[k].id) {
                imageFiles.push({ FileName: $scope.machine.images[k].name, IsPublished: $scope.machine.images[k].ispublished, SortOrder: k });
                files.push($scope.machine.images[k].file);

            } else {
                $scope.machine.images[k].sortorder = k;
            }
            $scope.machine.images[k].file = null;
        }

        machine.imagefiles = imageFiles;
        machine.images = $scope.machine.images;

        if (machine.images) {
            angular.forEach(machine.images, function(value) {

                if (value.imageSrc) {
                    value.imageSrc = null;
                }
            });
        }

        $http({
            method: 'PUT',
            url: "api/machines/" + $scope.machine.id + "/images",
            headers: { 'Content-Type': undefined },
            transformRequest: function () {
                var formData = new FormData();
                formData.append("machine", angular.toJson(machine));

                for (var m = 0; m < files.length; m++) {

                    formData.append("file" + m, files[m]);
                }
                return formData;
            },

            data: { machine: machine, files: files }
        }).
            success(function (data, status, headers, config) {

                $http.get('api/machines/' + $routeParams.machineId).
                    success(function (data, status, headers, config) {

                        var machineWithUpdatedImages = data.data;

                        $scope.machine.images = machineWithUpdatedImages.images;
                    }).
                    error(function (data, status, headers, config) {

                    });
            }).
            error(function (data, status, headers, config) {
                alert("failed!");
            });
    };

    angular.element(document).ready(function() {

        $("#imageUploader").change(function () {
            var reader = new FileReader();
            fileService.readMultiFiles($("#imageUploader")[0].files, reader, $scope.machine.images, function () { $scope.$apply(); }, function () { updateImages(); });
        });

    });

    searchService.getSearchParams(
            function (data) {
                if (data.success) {
                    data.data.statuses.shift();
                    data.data.phases.shift();
                    //data.data.voltages.shift();

                    //$scope.phases = data.data.phases;
                    //$scope.voltages = data.data.voltages;
                    $scope.categories = data.data.categories;
                    $scope.statuses = data.data.statuses;
                    //$scope.conditions = data.data.conditions;
                    //$scope.initiations = data.data.initiations;

                    $scope.locations = [{ label: '', value: null }].concat(data.data.locations);
                    
                    $scope.phases = [{ label: '', value: null }].concat(data.data.phases);

                    $scope.voltages = [{ label: '', value: null }].concat(data.data.voltages);

                    $scope.conditions = [{ label: '', value: null }].concat(data.data.conditions);

                    $scope.initiations = [{ label: '', value: null }].concat(data.data.initiations);

                } else {
                    $scope.error = data.error;
                }
            },
            function (data) {
                $scope.error = data.message;
            }
        );
    
    $http.get('api/machines/' + $routeParams.machineId).
        success(function (data, status, headers, config) {

            $scope.machine = data.data;

            commonService.setTitle($scope.machine.stocknumber + ' - ' + $scope.machine.name);

            var categories = [];

            for (var i = 0; i < $scope.machine.categories.length; i++) {
                categories.push({ label: $scope.machine.categories[i].name, value: $scope.machine.categories[i].id });
            }

            for (i = 0; i < $scope.machine.videos.length; i++) {
                $scope.machine.videos[i].url = $sce.trustAsResourceUrl($scope.machine.videos[i].url);
            }

            $scope.machine.categoriesForBind = categories.slice();

        }).
        error(function (data, status, headers, config) {
            
        });

    $scope.addImage = function () {
        $scope.isMachinChanged = false;

        $("#imageUploader").val(null);

        setTimeout(function () {
            $("#imageUploader").click();
        }, 200);
    };

    $scope.removeImage = function(fileName) {
        for (i = 0; i < $scope.machine.images.length; i++) {
            if ($scope.machine.images[i].filename == fileName) {
                $scope.machine.images.splice(i, 1);
                break;
            }
        }
        $scope.isMachinChanged = false;
    };

    $scope.addVideo = function () {
        $scope.isMachinChanged = false;
        if ($scope.currentVideoUrl) {

            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            var match = $scope.currentVideoUrl.match(regExp);

            var videoUrl = '//www.youtube.com/embed/' + match[2];

            $scope.machine.videos.push({
                url: $sce.trustAsResourceUrl(videoUrl),
                machineId:$scope.machine.id
            });
            $scope.$apply();
        }

        $scope.currentVideoUrl = null;
    }

    $scope.removeVideo = function (url) {

        for (i = 0; i < $scope.machine.videos.length; i++) {
            if ($scope.machine.videos[i].url == url) {
                $scope.machine.videos.splice(i, 1);
                $scope.$apply();
                break;
            }
        }
        $scope.isMachinChanged = false;
    }

    $scope.updateMachine = function(isValid) {
        if (!validateInput() || !isValid) {
            return;
        }

        var categories = new Array;

        for (var j = 0; j < $scope.machine.categoriesForBind.length; j++) {
            categories.push({ id: $scope.machine.categoriesForBind[j].value });
        }

        $scope.machine.categories = categories;

        var files = new Array;

        var imageFiles = new Array;
        
        for (var k = 0; k < $scope.machine.images.length; k++) {
            if (!$scope.machine.images[k].id) {
                imageFiles.push({ FileName: $scope.machine.images[k].name, IsPublished: $scope.machine.images[k].ispublished, SortOrder: k });
                files.push($scope.machine.images[k].file);

            } else {
                $scope.machine.images[k].sortorder = k;
            }
        }

        $scope.machine.imagefiles = imageFiles;

        for (k = 0; k < $scope.machine.videos.length; k++) {
            $scope.machine.videos[k].url = $sce.getTrustedUrl($scope.machine.videos[k].url);
        }

        $http({
            method: 'PUT',
            url: "api/machines/" + $scope.machine.id,
            headers: { 'Content-Type': undefined },
            transformRequest: function() {
                var formData = new FormData();
                formData.append("machine", angular.toJson($scope.machine));

                for (var m = 0; m < files.length; m++) {

                    formData.append("file" + m, files[m]);
                }
                return formData;
            },

            data: { machine: $scope.machine, files: files }
        }).
            success(function(data, status, headers, config) {
                $scope.isMachinChanged = true;

                $http.get('api/machines/' + $routeParams.machineId).
                    success(function(data, status, headers, config) {

                        $scope.machine = data.data;

                        var categories = [];

                        for (var i = 0; i < $scope.machine.categories.length; i++) {
                            categories.push({ label: $scope.machine.categories[i].name, value: $scope.machine.categories[i].id });
                        }

                        for (i = 0; i < $scope.machine.images.length; i++) {

                            var index = $scope.machine.images[i].url.lastIndexOf("/") + 1;
                            var filename = $scope.machine.images[i].url.substr(index);

                            $scope.machine.images[i].name = filename;
                        }

                        $scope.machine.categoriesForBind = categories.slice();

                        for (i = 0; i < $scope.machine.videos.length; i++) {
                            $scope.machine.videos[i].url = $sce.trustAsResourceUrl($scope.machine.videos[i].url);
                        }

                        //$scope.apply();

                    }).
                    error(function(data, status, headers, config) {

                    });
                
                $scope.doFade = false;

                $timeout(function () {
                    $scope.doFade = true;
                }, 3000);
            }).
            error(function(data, status, headers, config) {
                alert("failed!");
            });
    };
    
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    $scope.disabled = function (date, mode) {
        return false;
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.openDateSold = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.openedDateSold = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['MM.dd.yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.onMachineParamChanged = function() {
        $scope.isMachinChanged = false;
    };
    
    function validateInput() {
        var isValid = true;

        if ($scope.machine.name == null || $scope.machine.name == '') {
            $scope.nameIsNotValid = true;
            isValid &= false;
        }

        if ($scope.machine.categories == null || $scope.machine.categories.length == 0) {
            $scope.isCategoriesNotValid = true;
            isValid &= false;
        }

        return isValid;
    }
    
    $scope.onNameBlur = function () {
        if ($scope.machine.name == '' || $scope.machine.name == null) {
            $scope.nameIsNotValid = true;
        } else {
            $scope.nameIsNotValid = false;
        }
    };

    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    var getImageId = function() {

        return (S4() + S4() + S4() + S4().substr(0, 3) + S4() + S4() + S4() + S4()).toLowerCase();
    };

    $scope.showImage = function(imageSrc) {

        var myImage = new Image;
        myImage.src = imageSrc;
        myImage.style.border = 'none';
        myImage.style.outline = 'none';
        myImage.style.position = 'fixed';
        myImage.style.left = '0';
        myImage.style.top = '0';
        myImage.onload = function () {
            var newWindow = window.open("", "_blank");
            newWindow.document.write(myImage.outerHTML);
        }
    };

    $scope.sortableOptions = {
        stop: function (e, ui) {

            updateImages();
        }
    };
}]);