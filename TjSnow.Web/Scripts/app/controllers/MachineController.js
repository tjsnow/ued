﻿app.controller('MachineController', ['$scope', '$sce', 'searchService', 'fileService', '$http', 'breadcrumbs', '$location', '$timeout','SettingsService','CommonService',
    function ($scope, $sce, searchService, fileService, $http, breadcrumbs, $location, $timeout,settingsService,commonService) {

    var self = this;

    $scope.isPrimaryCollapsed = false;
    $scope.isCylinderSpecsCollapsed = false;
    $scope.isAdditionalMachineSpecsCollapsed = false;
    $scope.isSeamWelderCollapsed = false;
    $scope.isInternalInfoCollapsed = false;
    $scope.isMachineCreating = false;

    commonService.setTitle(null);

    self.settings = null;

    settingsService.getTJSettings().then(function(data) {

        self.settings = data.data.data;
    });

    $scope.breadcrumbs = breadcrumbs;

    $scope.images = [];

    $scope.videos = [];

    $scope.currentVideoUrl = null;

    angular.element(document).ready(function() {

        $("#imageUploader").change(function () {
            var reader = new FileReader();
            fileService.readMultiFiles($("#imageUploader")[0].files, reader, $scope.images, function () { $scope.$apply(); });
        });

    });

    $scope.success = false;

    $scope.machine =
    {
        phase: null,
        date: null,
        voltage: null,
        status: 1,
        stocknumber: null,
        categories: [],
        name: null,
        description: null,
        kva: null,
        throat: null,
        secamps: null,
        secvolts: null,
        manufacturer: null,
        arm: null,
        armdiaupper: null,
        fastship: false,
        plattenupper1: null,
        plattenupper2: null,
        tslot: null,
        publishedtoweb: false,
        holderdiameter: null,
        tapswitch: null,
        control: null,
        service: null,
        uedfilepath: null,
        headtype: null,
        upperdrive: null,
        lowerdriver: null,
        wheelupper: null,
        wheellower: null,
        serial: null,
        model: null,
        initiation: null,
        airtype: null,
        cylinderdiameter: null,
        cylinderstroke: null,
        maxforce: null,
        transformer:null,
        entereddate:null,
        purchasedfrom: null,
        condition: null,
        cost: null,
        freight: null,
        feautures: null,
        location: 1,
        checkedby: null,
        price: null,
        soldto: null,
        datesold: null,
        excludefromreconciliation: null,
        p21link:null
    };

    searchService.getSearchParams(
            function (data) {
                if (data.success) {
                    data.data.statuses.shift();
                    data.data.phases.shift();
                    //data.data.voltages.shift();

                    $scope.categories = data.data.categories;
                    $scope.statuses = data.data.statuses;
                    
                    $scope.locations = [{ label: '', value: null }].concat(data.data.locations);
                    
                    $scope.phases = [{ label: '', value: null }].concat(data.data.phases);

                    $scope.voltages = [{ label: '', value: null }].concat(data.data.voltages);

                    $scope.conditions = [{ label: '', value: null }].concat(data.data.conditions);

                    $scope.initiations = [{ label: '', value: null }].concat(data.data.initiations);

                } else {
                    $scope.error = data.error;
                }
            },
            function (data) {
                $scope.error = data.message;
            }
        );

    $scope.addImage = function () {

        $("#imageUploader").val(null);

        setTimeout(function () {
            $("#imageUploader").click();
        }, 200);
    };

    $scope.removeImage = function(fileName) {
        for (i = 0; i < $scope.images.length; i++) {
            if ($scope.images[i].filename == fileName) {
                $scope.images.splice(i, 1);
                break;
            }
        }
    };

    $scope.addVideo = function() {

        if ($scope.currentVideoUrl) {

            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            var match = $scope.currentVideoUrl.match(regExp);

            var videoUrl = '//www.youtube.com/embed/' + match[2];

            $scope.videos.push({
                url: $sce.trustAsResourceUrl(videoUrl)
            });
            $scope.$apply();
        }

        $scope.currentVideoUrl = null;
    };

    $scope.removeVideo = function(url) {

        for (i = 0; i < $scope.videos.length; i++) {
            if ($scope.videos[i].url == url) {
                $scope.videos.splice(i, 1);
                $scope.$apply();
                break;
            }
        }
    };

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    };

    $scope.createMachine = function(isValid) {
        $scope.submitted = true;
        
        if (!validateInput() || !isValid) {
            return;
        }

        $scope.isMachineCreating = true;

        var newMachine = {
            phase: $scope.machine.phase,
            voltage: $scope.machine.voltage,
            status: $scope.machine.status,
            stocknumber: $scope.machine.stocknumber,
            categories: [],
            name: $scope.machine.name,
            webdescription: $scope.machine.description,
            kva: $scope.machine.kva,
            throatdepth: $scope.machine.throat,
            manufacturer: $scope.machine.manufacturer,
            armdialower: $scope.machine.arm,
            secamps: $scope.machine.secamps,
            fastship: $scope.machine.fastship,
            platenupper: $scope.machine.plattenupper1,
            tslotspacing: $scope.machine.tslot,
            platenlower: $scope.machine.plattenupper2,
            purchaseddate: $scope.machine.date,
            publishedtoweb: $scope.machine.publishedtoweb,
            internalnotes: $scope.machine.internalnotes,
            uedfilepath: $scope.machine.uedfilepath,
            secvolts: $scope.machine.secvolts,
            model: $scope.machine.model,
            serial: $scope.machine.serial,
            initiation: $scope.machine.initiation,
            airtype: $scope.machine.airtype,
            cylinderdiameter: $scope.machine.cylinderdiameter,
            cylinderstroke: $scope.machine.cylinderstroke,
            maxforce: $scope.machine.maxforce,
            tapswitch: $scope.machine.tapswitch,
            armdiaupper: $scope.machine.armdiaupper,
            holderdiameter: $scope.machine.holderdiameter,
            control: $scope.machine.control,
            service: $scope.machine.service,
            headtype: $scope.machine.headtype,
            upperdrive: $scope.machine.upperdrive,
            lowerdrive: $scope.machine.lowerdrive,
            wheelupper: $scope.machine.wheelupper,
            wheellower: $scope.machine.wheellower,
            transformer: $scope.machine.transformer,
            purchasedfrom: $scope.machine.purchasedfrom,
            condition: $scope.machine.condition,
            cost: $scope.machine.cost,
            freight: $scope.machine.freight,
            feautures: $scope.machine.feautures,
            locationid: $scope.machine.location,
            checkedby: $scope.machine.checkedby,
            price: $scope.machine.price,
            soldto: $scope.machine.soldto,
            datesold: $scope.machine.datesold,
            excludefromreconciliation: $scope.machine.excludefromreconciliation,
            p21itemid:$scope.machine.p21link
        };

        var videos = new Array;

        for (var i = 0; i < $scope.videos.length; i++) {
            videos.push($sce.getTrustedUrl($scope.videos[i].url));
        }

        newMachine.videos = videos;

        var categories = new Array;

        for (var j = 0; j < $scope.machine.categories.length; j++) {
            categories.push($scope.machine.categories[j].value);
        }

        newMachine.categories = categories;

        var files = new Array;

        var imageFiles = new Array;

        for (var k = 0; k < $scope.images.length; k++) {

            files.push($scope.images[k].file);

            imageFiles.push({ FileName: $scope.images[k].name, IsPublished: $scope.images[k].ispublished, SortOrder: k });
        }

        newMachine.imagefiles = imageFiles;

        $http({
            method: 'POST',
            url: "api/machines",
            headers: { 'Content-Type': undefined },
            transformRequest: function() {
                var formData = new FormData();
                formData.append("machine", angular.toJson(newMachine));

                for (var m = 0; m < files.length; m++) {

                    formData.append("file" + m, files[m]);
                }
                return formData;
            },
            data: { machine: newMachine, files: files }
            }).
            success(function(data, status, headers, config) {
                $scope.success = true;
                $scope.clearAll();
                $scope.isMachineCreating = false;

                $location.path("/admin/editmachine/" + data.data.id);

                $timeout(function () {
                    $scope.success = false;
                }, 3000);
            }).
            error(function(data, status, headers, config) {

                $scope.isMachineCreating = false;

                alert("failed!");
            });
    };

    $scope.clearAll = function () {
        $scope.machine.phase = null;
        $scope.machine.voltage = null;
        
        $scope.machine.status = null;
        $scope.machine.stocknumber = null;
        $scope.machine.categories = null;
        $scope.machine.name = null;
        $scope.machine.description = null;
        $scope.machine.kva = null;
        $scope.machine.throat = null;
        $scope.machine.secamps = '';
        $scope.machine.manufacturer = null;
        $scope.machine.arm = null;
        $scope.machine.date = null;
        $scope.machine.fastship = false;
        $scope.machine.plattenupper1 = null;
        $scope.machine.plattenupper2 = null;
        $scope.machine.tslot = null;
        $scope.machine.internalnotes = null;
        $scope.machine.publishedtoweb = false;

        $scope.images = [];
        $scope.videos = [];

        $scope.machine.uedfilepath = null;
        $scope.currentVideoUrl = null;
        $scope.machine.secvolts = null;
        $scope.machine.model = null;
        $scope.machine.serial = null;
        $scope.machine.initiation = null;
        $scope.machine.airtype = null;
        $scope.machine.cylinderdiameter = null;
        $scope.machine.cylinderstroke = null;
        $scope.machine.maxforce = null;
        $scope.machine.tapswitch = null;
        $scope.machine.armdiaupper = null;
        $scope.machine.holderdiameter = null;
        $scope.machine.control = null;
        $scope.machine.service = null;
        $scope.machine.excludefromreconciliation = false;
        $scope.machine.headtype = null;
        $scope.machine.upperdrive = null;
        $scope.machine.lowerdriver = null;
        $scope.machine.wheelupper = null;
        $scope.machine.wheellower = null;
        $scope.machine.transformer = null;
        $scope.machine.purchasedfrom = null;
        $scope.machine.condition = null;
        $scope.machine.cost = null;
        $scope.machine.freight = null;
        $scope.machine.feautures = null;
        $scope.machine.location = null;
        $scope.machine.checkedby = null;
        $scope.machine.price = null;
        $scope.machine.soldto = null;
        $scope.machine.datesold = null;
    };

    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    $scope.disabled = function (date, mode) {
        return false;
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.openDateSold = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.openedDateSold = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['MM.dd.yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];

    function validateInput() {
        var isValid = true;

        if ($scope.machine.categories == null || $scope.machine.categories.length == 0) {
            $scope.isCategoriesNotValid = true;
            isValid &= false;
        }

        if ($scope.machine.status == null) {
            $scope.isStatusNotValid = true;
            isValid &= false;
        }

        return isValid;
    }

    $scope.showImage = function (imageSrc) {

        var myImage = new Image;
        myImage.src = imageSrc;
        myImage.style.border = 'none';
        myImage.style.outline = 'none';
        myImage.style.position = 'fixed';
        myImage.style.left = '0';
        myImage.style.top = '0';
        myImage.onload = function () {
            var newWindow = window.open("", "_blank");
            newWindow.document.write(myImage.outerHTML);
        }
    };

    $scope.onStockNumberFilledOut = function() {

        $scope.machine.uedfilepath = self.settings.uedmachinebasefilepath + $scope.machine.stocknumber;
        $scope.machine.p21link = self.settings.p21template.replace('[Stock#]', $scope.machine.stocknumber);
    };

    $scope.goTo21File = function() {

        if ($scope.machine.p21link) {

            window.location.href = $scope.machine.p21link;
        }

    };
    }]);

function preventP21LinkFires(event) {
    
    if (!$('#p21link') ||!$('#p21link').val()|| $('#p21link').val().length == 0) {
        event.preventDefault();
    }
};