﻿mainModule.controller('ResultsController', ['$scope', '$location', 'breadcrumbs', 'searchService', 'searchResultService', '$rootScope','CommonService','$filter',
    function ($scope, $location, breadcrumbs, searchService, searchResultService, $rootScope,commonService,$filter) {

        var EXCLUDED_STATUSES = { Sold: 2, Scrapped: 4 };

        $scope.message = 'ResultsController';
        $scope.findMessageVisible = false;
        $scope.sortparams = [{ label: "KVA", value: "kva" }, { label: "Voltage", value: "voltage" }];
        $scope.selectedSortParam = "kva";
        $scope.selectedStatus = null;
        $scope.isBusy = false;

        commonService.setTitle(null);
       
        var searchResult = searchResultService.getResult();
        
        if (!searchResult) {
            $location.path("/admin");
        }

        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 100;

        $scope.breadcrumbs = breadcrumbs;

        $scope.machines = [];

        if (searchResult != null) {
            $scope.machinesFoundCount = searchResult.result.data.totalcount;
            $scope.totalItems = searchResult.result.data.totalcount;
            $scope.machines = searchResult.result.data.machines;

            //if ($scope.machines && $scope.machines.length > 1) {
            //    $scope.machines.sort(function(machine1, machine2) {

            //        if (machine1[$scope.selectedSortParam] && !machine2[$scope.selectedSortParam]) {
            //            return 1;
            //        }

            //        if (!machine1[$scope.selectedSortParam] && machine2[$scope.selectedSortParam]) {
            //            return -1;
            //        }

            //        if (!machine1[$scope.selectedSortParam] && !machine2[$scope.selectedSortParam]) {
            //            return 0;
            //        }

            //        return machine1[$scope.selectedSortParam] - machine2[$scope.selectedSortParam];
            //    });
            //}

            $scope.phases = searchResult.data.phases;
            $scope.voltages = searchResult.data.voltages;
            $scope.categories = searchResult.data.categories;
            $scope.statuses = $filter('filter')(searchResult.data.statuses, function(value, index) {return value.value!==EXCLUDED_STATUSES.Scrapped && value.value!==EXCLUDED_STATUSES.Sold});//searchResult.data.statuses;
            $scope.publishedtowebstatuses = searchResult.data.publishedtowebstatuses;

            $scope.searchrequest = searchResult.request;

            $scope.machinesFoundCount = searchResult.result.data.totalcount;
            $scope.totalItems = searchResult.result.data.totalcount;
            $scope.machines = searchResult.result.data.machines;

        } else {
            $scope.phases = [{ label: "All phases", value: null }];
            $scope.voltages = [{ label: "All voltages", value: null }];
            $scope.statuses = [{ label: "All statuses", value: null }];
            $scope.publishedtowebstatuses = [{ label: "All machines", value: null }];

            $scope.searchrequest = {
                keywords: null,
                includeSold: false,
                includeScrapped: false,
                minkva: '',
                maxkva: '',
                voltages: null,
                phase: null,
                categories: null,
                publishedtowebstatus: null
            };

            $scope.machinesFoundCount = 0;
        }

        $scope.doSearch = function () {

            $scope.currentPage = 1;

            $scope.findMessageVisible = false;

            performSearch();
        };

        $scope.clearKeywords = function () {
            $scope.searchrequest.keywords = null;
        };

        $scope.clearAll = function ($event) {

            $event.preventDefault();

            $scope.searchrequest.keywords = null;
            $scope.searchrequest.includeSold = false,
            $scope.searchrequest.includeScrapped = false;

            $scope.searchrequest.minkva = '';
            $scope.searchrequest.maxkva = '';
            $scope.searchrequest.voltages = null;
            $scope.searchrequest.phase = null;
            $scope.searchrequest.categories = null;
            $scope.searchrequest.publishedtowebstatus = null;
            $scope.machinesFoundCount = 0;
            $scope.machines = null;
            $scope.findMessageVisible = true;
            $scope.isBusy = false;
        };

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {

            performSearch();
        };

        function performSearch() {
            $scope.isBusy = true;

            $scope.searchrequest.skip = $scope.itemsPerPage * ($scope.currentPage - 1);
            $scope.searchrequest.take = $scope.itemsPerPage;
            $scope.searchrequest.isfastship = $scope.fastShipOnly;
            $scope.searchrequest.orderby = $scope.selectedSortParam;
            $scope.searchrequest.status = $scope.selectedStatus;

            searchService.search(
                $scope.searchrequest,
                function (result) {
                    $scope.isBusy = false;
                    if (result != null) {

                        $scope.machinesFoundCount = result.data.totalcount;
                        $scope.totalItems = result.data.totalcount;
                        $scope.machines = result.data.machines;
                    }
                },
                function () {
                    $scope.isBusy = false;
                });
        }

        $scope.maxSize = 5;

        $scope.orderByFunction = function (machine) {

            if ($scope.selectedSortParam == "kva") {
                return machine.kva;
            }

            if ($scope.selectedSortParam == "voltage") {
                return machine.voltage;
            }
        };

        $scope.filterFunction = function (machine) {
            if ($scope.fastShipOnly && !machine.fastship) {
                return false;
            }
            
            if ($scope.selectedStatus == null) {
                return true;
            }

            if (machine.status == $scope.selectedStatus) {
                return true;
            }

            return false;
        };

        $scope.editMachine = function (machineId) {
            $location.path("/admin/editmachine/" + machineId);
        };

        $scope.seeInPublicView = function(machineId) {
            alert('See in public view called for machineId = ' + machineId);
        };

        $scope.searchCriteriaFieldsEnterKeyPressed = function($event) {

            var keyCode = $event.which || $event.keyCode;
            if (keyCode === 13) {
                $scope.doSearch();
            }
        };

        $scope.goToPublicView = function(machineId) {

            window.location.href = "/Machines/Index/" + machineId;
        };

        $scope.orderedSearch = function(param) {

            $scope.selectedSortParam = param;

            $scope.doSearch();
        };

        $scope.statusSearch = function(param) {

            $scope.selectedStatus = param;

            $scope.doSearch();
        };

        $scope.getStatusColor = function(status) {

            var backgroundColor = 'rgb(238,238,238)';

            switch (status) {
                case 1:
                    backgroundColor = '#B0E57C'; break;
                case 8:
                    backgroundColor = '#FFF0AA'; break;
                case 2:
                    backgroundColor = "#B4D8E7"; break;
                case 4:
                    backgroundColor = "#FFAEAE";break;
            }

            return { 'background-color': backgroundColor };
        };
    }]);