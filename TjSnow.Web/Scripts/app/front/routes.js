﻿

angular.module('TjSnowFront').config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/Scripts/app/front/main/frontMainController.html',
            controller: 'frontMainController'
        })
        .when('/search/:textSearch', {
            templateUrl: '/Scripts/app/front/search/searchController.html',
            controller: 'searchController'
        })
        .when('/search/category/:category', {
            templateUrl: '/Scripts/app/front/search/searchController.html',
            controller: 'searchController'
        })
        .when('/details/:id', {
            templateUrl: '/Scripts/app/front/machine/machineController.html',
            controller: 'machineController'
        })
       // .when('', '/')
        .otherwise({ redirectTo: '/' });
    $routeProvider.caseInsensitiveMatch = true;
    $locationProvider.html5Mode(true);
});