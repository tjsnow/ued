﻿(function (module){
   
    module.service('backendApi', backendApi);
    module.$inject = ['$http'];

    function backendApi($http) {
        var keyStorageRecent = 'TJ.Recent',
            maxCountRecent = 5,
            defaultIconUrl = '/Content/images/content/machine1.jpg';
        var recent = JSON.parse(localStorage.getItem(keyStorageRecent)) || [];
        
        this.getCategories = function () {
            return $http.get('api/categories').then(
                function (data) {
                    return data.data.data;
                });
        }

        this.search = function (request) {

            return $http.get('api/publicsearch', { params: request }).then(
                function (data) {
                    return data.data.data;
                });
        }

        this.getMachine = function (id) {

            return $http.get('api/public/machines/'+id).then(
                function (data) {
                    var wasFound = false;
                    for (var i = 0; i < recent.length; i++) {
                        if (recent[i].id != id) continue;
                        wasFound = true;
                        break;
                    }
                    if (!wasFound) {
                        var url = data.data.data.images && data.data.data.images.length > 0 ? data.data.data.images[0].url : defaultIconUrl;
                        recent.unshift({
                            id: id,
                            url:url
                        });
                        localStorage.setItem(keyStorageRecent, JSON.stringify(recent));

                    }
                    return data.data.data;

                });
        }

        this.feedback = function (model) {
            return $http.post('api/feedback',model).then(
                    function (data) {

                    }
                )

        }
        this.getRecent = function () {
            return recent;
        };

    };

})(angular.module('TjSnowFront'));

