﻿(function (module) {
    module.controller('searchController', searchController);
    searchController.$inject = ['$scope', '$routeParams', 'backendApi', 'searchFactory'];
    function searchController($scope, $routeParams, backendApi, searchFactory) {
        var countOnPage=18;
        $scope.model = {
            searchResponse:{
                Machines: [],
                
            },
            splitMachines: [],
            filters: {
                voltages:[],
                phases:[],
                kva: '',
                phase: '',
                voltage: ''
            },

            rows: false
        };



        var searchText = $routeParams['textSearch']?$routeParams['textSearch']:'',
            category = $routeParams['category'] ? $routeParams['category'] : '';
        
        $scope.categoryName = searchFactory.category.name?searchFactory.category.name:'Result search'

        $scope.changeFilter = function () {
            search();
        }

        $scope.next = function (step) {
            $scope.currentPage += step;
            splitMachines();
        }

        function splitMachines() {

            $scope.model.splitMachines.length = 0;
            var tempArr=$scope.model.searchResponse.machines.slice(($scope.currentPage-1)*6*3,($scope.currentPage-1)*6*3+18);
            var countRows = Math.ceil(tempArr.length / 6);
            for (var i = 0; i < countRows; i++) {
                $scope.model.splitMachines.push(tempArr.slice(i * 6, i * 6 + 6));
            }
        }



        function search() {
            var request;
            if (!$scope.wasSearch) {
                request = {
                    Keywords: searchText,
                    Category:category,
                    Take: countOnPage,
                    Skip:0
                }
            } else
            {
                request= {
                    Keywords: '',
                    Category: category,
                    Take: countOnPage,
                    Skip: ($scope.currentPage - 1) * countOnPage,
                    Phase:$scope.model.filters.phase,
                    Voltages:[$scope.model.filters.voltage]

                }


            }

            $scope.isLoading = true;
            backendApi.search(request).then(
                    function (data) {
                        $scope.model.searchResponse = data;
                        $scope.currentPage = 1;
                        $scope.totalPage = Math.ceil(data.totalcount / countOnPage);
                        splitMachines();
                        if (!$scope.wasSearch) {
                            $scope.model.filters.voltages = $scope.model.searchResponse.voltages;
                            $scope.model.filters.phases = $scope.model.searchResponse.phases;
                            $scope.wasSearch = true;
                        }
                        
                    }
                ).finally(
                    function () {
                        $scope.isLoading = false;
                    }
                )
            

        };
        
        search();

    };
})(angular.module('TjSnowFront'));