﻿(function (module) {
    module.controller('machineController', machineController);
    machineController.$inject = ['$scope','$routeParams','backendApi'];
    function machineController($scope,$routeParams, backendApi) {
        angular.element(document).ready(
                function () {
                    var jcarousel = $('.jcarousel');

                    jcarousel
                        .on('jcarousel:reload jcarousel:create', function () {
                            var carousel = $(this),
                                width = carousel.innerWidth();

                            if (width >= 400) {
                                width = width / 4;
                            } else if (width >= 150) {
                                width = width / 2;
                            }

                            carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                        })
                        .jcarousel({
                            wrap: 'circular'
                        });

                    $('.jcarousel-control-prev')
                        .jcarouselControl({
                            target: '-=1'
                        });

                    $('.jcarousel-control-next')
                        .jcarouselControl({
                            target: '+=1'
                        });
                }
            );


        $scope.model = {
            machine: {},
            user: {
                lastName: '',
                firstName: '',
                title: '',
                company: '',
                phone: '',
                email: '',
                city: '',
                state: '',
                details: '',
                machine: {}
            },
        };



        var id = $routeParams['id'];

        $scope.resetForm = function () {
            $scope.form.$setPristine();
            $scope.form.$setUntouched();
            $scope.model.user = {
                lastName: '',
                firstName: '',
                title: '',
                company: '',
                phone: '',
                email: '',
                city: '',
                state: '',
                details: ''
            }
        };

        $scope.sendFeedback = function () {

            if ($scope.form.$invalid) return;

            $scope.isSending = true;
            $scope.model.user.machine = $scope.model.machine;
            backendApi.feedback($scope.model.user).finally(
                function () {
                    $scope.resetForm();
                    $scope.isSending = false;
                });

        };


        backendApi.getMachine(id).then(
                function (data) {
                    $scope.model.machine = data;
                    $scope.recent = backendApi.getRecent();
                }
            )





    };
})(angular.module('TjSnowFront'));