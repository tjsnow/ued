﻿$(function () {
    var currentPage = 1,
        isTable = false,
        totalCount = $('#content_search').data('total');
    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
            return null;
        }
        else {
            return results[1] || 0;
        }
    }


    $('.sel_category').change(function () {

        var categoryId = $(this).val();

        if (categoryId != -5) {
            window.location.href = '/home/search/' + categoryId;
        } else {
            window.location.href = '/home/search/?fastship=true';
        }
    });

    if ($("#categorySelector")) {
        $("#categorySelector").change(function() {

            var categoryId = $(this).val();

            if (categoryId != -5) {
                window.location.href = '/home/search/' + categoryId;
            } else {
                window.location.href = '/home/search/?fastship=true';
            }
        });
    }

    function search(event) {
        var el = $(this);
        if (!el.val() || event.keyCode != 13) return;

        window.location.href = '/home/search?keyword=' + el.val();
    }
    $('#search_keyword_input_header').keyup(function(event) {
        var el = $(this);
        if (event.keyCode != 13) {
            return;
        }

        if (!el.val()) {
            alert('Please enter a search term before searching.');
            return;
        }

        window.location.href = '/home/search?keyword=' + el.val();
    });
    $('#search_keyword_input_main').keyup(function(event) {
        var el = $(this);
        if (event.keyCode != 13) {
            return;
        }

        if (!el.val()) {
            alert('Please enter a search term before searching.');
            return;
        }

        window.location.href = '/home/search?keyword=' + el.val();
    });
    $('#search_keyword_input_search').keyup(search);

    $('#search_keyword_button_header').click(function() {
        var val = $('#search_keyword_input_header').val();

        if (!val) {

            alert('Please enter a search term before searching.');
            return;
        }

        window.location.href = '/home/search?keyword=' + val;
    });

    $('#search_keyword_button_main').click(function () {
        var val = $('#search_keyword_input_main').val();

        if (!val) {

            alert('Please enter a search term before searching.');
            return;
        }

        window.location.href = '/home/search?keyword=' + val;
    })
    $('#search_keyword_button_search').click(function () {
        var val = $('#search_keyword_input_search').val();

        if (!val) {

            alert('Please enter a search term before searching.');
            return;
        }

        window.location.href = '/home/search?keyword=' + val;
    })

    //$("#feedback").on("submit", function (event) {
    //    var $this = $(this);
    //    var frmValues = $this.serialize();
    //    $.ajax({
    //        type: $this.attr('method'),
    //        url: $this.attr('action'),
    //        data: frmValues
    //    })
    //    .done(function () {
    //        $this[0].reset();
    //    })
    //    .fail(function () {

    //    });
    //    event.preventDefault();
    //});

    $(".additional_details_search").hide();

    $('#list_button').click(function () {
        $('#content_search').removeClass('content_search_cat_list').addClass('content_search_cat')
        $(".additional_details_search").hide();
        isTable = false;
    });
    $('#tabel_button').click(function () {
        $('#content_search').removeClass('content_search_cat').addClass('content_search_cat_list')
        $(".additional_details_search").show();
        isTable = true;
    });


    function search() {

        var url = window.location.href.replace(/search/gi, 'SearchFilter');
        countOnPage=$('#countOnPage').val();
        $.ajax({
            type: 'get',
            url: url,
            data: {
                countOnPage: countOnPage,
                phase: $('#phase_sel').val(),
                voltage: $('#voltage_sel').val(),
                currentPage: currentPage - 1,
                maxkva: $('#maxKva').val(),
                minkva: $('#minKva').val()
    }
        })
        .done(function (data) {
            var el = $.parseHTML(data);
            totalCount = $(el).data('total');
            if (isTable) {
                $(el).removeClass('content_search_cat').addClass('content_search_cat_list')
                $(".additional_details_search", el).show();
            } else {


                $(el).removeClass('content_search_cat_list').addClass('content_search_cat')
                $(".additional_details_search",el).hide();
            }
           // $('#pagination').remove();
            $('#content_search').replaceWith(el);
            createPagination()

        })
        .fail(function (e) {
            alert(e);
            alert('Sorry, error of request');
        });
        event.preventDefault();
    }

    function createPagination(){
        var countOnPage=$('#countOnPage').val();
        var totalPages =Math.ceil(totalCount / countOnPage),
            startIndex = 0
        endIndex = totalPages;

        if (totalPages > 10) {
            startIndex = currentPage - 5;
            endIndex = currentPage + 5;
            if (startIndex < 0) {
                startIndex = 0;
                endIndex = startIndex + 10;
            }
            if (endIndex > totalPages) {
                endIndex = totalPages;
                startIndex = totalPages - 10;
            }
        }
        if (currentPage == 1) {
            $('.prev_arrow').hide();
        } else {
            $('.prev_arrow').show();
        }
        if (currentPage == totalPages || totalPages==10) {
            $('.next_arrow').hide();
        } else {
            $('.next_arrow').show();
        }
        $('.pagination_number .page_number').remove();
        var elements = [];
        for (var i=startIndex;i<endIndex;i++)
        {
            if (currentPage==i+1){
                elements.push($('<span class="page_number">' + (i + 1) + '</span>'));
            }else{
                elements.push($('<a href="#" class="page_number" data-page="'+(i + 1)+'">' + (i + 1) + '</a>').click(
                    function (event) {

                        event.preventDefault();

                        var $this = $(this);
                        currentPage = $this.data('page');
                        search();
                    }
                    ));
            }

        }
        $('.pagination_number .next_arrow').before(elements)
    }
    



    $('#countOnPage').change(function () {
        currentPage = 1;
        search();
    });
    $('#voltage_sel').change(function () {
        currentPage = 1;
        search();
    });
    $('#phase_sel').change(function () {
        currentPage = 1;
        search();
    });

    $('.next_arrow').click(function() {
        currentPage++;
        search();
    });

    $('.prev_arrow').click(function() {
        currentPage--;
        search();
    });

    $('#minKva').change(function() {
        currentPage = 1;
        search();
    });

    $('#maxKva').change(function () {
        currentPage = 1;
        search();
    });


    createPagination();
})
