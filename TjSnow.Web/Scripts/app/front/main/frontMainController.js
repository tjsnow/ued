﻿(function (module) {
    module.controller('frontMainController', frontMainController);
    frontMainController.$inject =['$scope','backendApi','$location','searchFactory'];
    function frontMainController($scope, backendApi, $location, searchFactory) {
        $scope.model = {
            categories: [],
            splitCategories: [],
            user: {
                lastName: '',
                firstName: '',
                title: '',
                company: '',
                phone: '',
                email: '',
                city: '',
                state: '',
                details:''
            },
            search:{
                text:''
            }


        }

        $scope.selectCategory = function (item) {
            $scope.model.selectedCategory = item;
            $scope.searchCategory();
        };

        $scope.searchCategory = function () {
            $location.path('/search/category/' + $scope.model.selectedCategory.id);
            searchFactory.category.name = $scope.model.selectedCategory.name;
        };

        $scope.getRows = function () {
            var countRows = Math.ceil($scope.model.categories.length / 3);
            var result=[];
            for (var i = 0; i <= countRows; i++) {
                result.push(i);
            }
            return result;
        };

        $scope.getCols = function (row) {
            return $scope.model.categories.slice(row * 3 - 3, row * 3 - 1);
        };


        $scope.resetForm=function(){
            $scope.form.$setPristine();
            $scope.form.$setUntouched();
            $scope.model.user ={
                lastName: '',
                firstName: '',
                title: '',
                company: '',
                phone: '',
                email: '',
                city: '',
                state: '',
                details:''
            }
        };

        $scope.sendForm = function () {

            //if (!$scope.model.user.firstName) {
            //    wasError = true;
            //}
            //if (!$scope.model.user.company) {
            //    wasError = true;
            //}
            //if (!$scope.model.user.email) {
            //    wasError = true;
            //}
            //if (!$scope.model.user.city) {
            //    wasError = true;

            //}
            //if (!$scope.model.user.state) {
            //    wasError = true;

            //}


            if ($scope.form.$invalid) return;

            //$scope.model.user.firstName = 'Azat';
            //$scope.model.user.lastName = 'Khalilov';
            //$scope.model.user.title = 'Ms';
            //$scope.model.user.phone = '323232';
            //$scope.model.user.email = 'azat.halilov@nitka.com';
            //$scope.model.user.city = 'ufa';
            //$scope.model.user.state = 'none';
            //$scope.model.user.message = 'testin message';

            $scope.isSending = true;

            backendApi.feedback($scope.model.user).finally(
                function () {
                    $scope.resetForm();
                    $scope.isSending = false;
                });

        };


        $scope.search = function () {
            if (!$scope.model.search.text) return;
            searchFactory.category.name = '';
            $location.path('/search/' + $scope.model.search.text);

        };

        backendApi.getCategories().then(
            function (items) {
                $scope.model.categories = items;
                $scope.model.splitCategories.length=0;
                var countRows = Math.ceil($scope.model.categories.length / 3);
                for (var i = 0; i <countRows; i++) {
                    $scope.model.splitCategories.push($scope.model.categories.slice(i * 3,i*3+3))
                }

            }
        )
    };
})(angular.module('TjSnowFront'));