﻿angular.module('TjSnow.Machines.GridView', ['ngRoute', 'ng-breadcrumbs', 'datatables', 'datatables.bootstrap', 'datatables.fixedcolumns','datatables.columnfilter','datatables.tabletools', 'TjSnow.Api'])

.directive('onLastRepeat', function () {
    return function (scope, element, attrs) {
        if (scope.$last) setTimeout(function () {
            scope.$emit('onRepeatLast', element, attrs);
        }, 1);
    };
})

.config(function ($routeProvider, $compileProvider) {
    $routeProvider.when('/admin/grid-view', {
        controller: 'GridViewController',
        templateUrl: '/Scripts/app/machines/grid-view.html',
        label: 'Grid view'
    });

    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|pxxiurlhandler):/);
})

.run(function ($rootScope, $location) {
    $rootScope.location = $location;
})

.controller('GridViewController', ['$scope', 'breadcrumbs', '$location', '$filter', '$window', '$timeout', 'DTOptionsBuilder', 'DTInstances', 'ApiService', 'searchService','DTColumnBuilder','CommonService',
  function ($scope, breadcrumbs, $location, $filter, $window, $timeout, DTOptionsBuilder, DTInstances, api, searchService, DTColumnBuilder,commonService) {
      $scope.breadcrumbs = breadcrumbs;
      $scope.machines = [];

      commonService.setTitle(null);

      $scope.filter = {
          published: -1,
          //unpublished: ,
          uipublished: -1,
          //uiunpublished: true,
          available: true,
          uiavailable: true,
          scrapped: false,
          uiscrapped: false,
          uiminkva: null,
          uimaxkva: null,
          uivoltages: []
      };

      $scope.publishedStates = [
          { label: 'Any', value: -1 },
          { label: 'Published', value: 1 },
          { label: 'Unpublished', value: 0 }
      ];

      $scope.phases = [{ label: "All phases", value: null }];

      searchService.getSearchParams(
          function (data) {
              if (data.success) {
                  $scope.phases = data.data.phases;
                  $scope.voltages = data.data.voltages;
                  $scope.statuses = data.data.statuses;                

              }
          });

      api.get('/machines').then(function (res) {
          $scope.machines = res.data;
      });

      $scope.filterData = function (machine) {
          return (
              // published & unpublished
              ($scope.filter.published != 0 && machine.publishedtoweb) || ($scope.filter.published != 1 && !machine.publishedtoweb))
              // status
              && (
                ($scope.filter.available && machine.status == 1) ||
                ($scope.filter.sold && machine.status == 2) ||
                ($scope.filter.scrapped && machine.status == 4) ||
                ($scope.filter.rented && machine.status == 8) ||
                ($scope.filter.consigned && machine.status == 16)
              )
              // phase
              && ($scope.filter.phase == null || ($scope.filter.phase == 1 && machine.phase == 1) || ($scope.filter.phase == 2 && machine.phase == 2))
              // voltage
              && (!$scope.filter.voltages || $scope.filter.voltages.length == 0 || ($.grep($scope.filter.voltages, function (v) { return v.value == machine.voltage; }).length > 0))
              // kva
              && ((!$scope.filter.minkva && !$scope.filter.maxkva)
                  || ($scope.filter.minkva && !$scope.filter.maxkva && machine.kva && machine.kva >= $scope.filter.minkva)
                  || (!$scope.filter.minkva && $scope.filter.maxkva && machine.kva && machine.kva <= $scope.filter.maxkva)
                  || ($scope.filter.minkva && $scope.filter.maxkva && machine.kva && machine.kva >= $scope.filter.minkva && machine.kva <= $scope.filter.maxkva));
      };

      DTInstances.getLast().then(function (dtInstance) {
          $scope.dtInstance = dtInstance;
      });

      $scope.reload = function () {
          $scope.dtInstance.reloadData();
      };

      $scope.refresh = function () {
          $scope.dtInstance.rerender(); // we forced to do it because of FixedColumns behavour in angular-datatables.net
      };

      $scope.applyFilter = function () {
          $scope.filter.published = $scope.filter.uipublished;
          //$scope.filter.unpublished = $scope.filter.uiunpublished;

          $scope.filter.available = $scope.filter.uiavailable;
          $scope.filter.sold = $scope.filter.uisold;
          $scope.filter.scrapped = $scope.filter.uiscrapped;
          $scope.filter.rented = $scope.filter.uirented;
          $scope.filter.consigned = $scope.filter.uiconsigned;

          $scope.filter.phase = $scope.filter.uiphase;
          $scope.filter.voltages = $scope.filter.uivoltages.slice();
          $scope.filter.minkva = $scope.filter.uiminkva;
          $scope.filter.maxkva = $scope.filter.uimaxkva;


          $scope.refresh();
          $timeout(function() {
              angular.element($window).resize();
              $scope.dtOptions.withFixedColumns({ leftColumns: 2 });
          }, 400);
          
      };

      var height = $window.innerHeight - 260 - angular.element('.row-filter')[0].offsetHeight;
      if (height < 100) {
          height = 100;
      }

      var toolsContainer = $(".DTTT");

      var columnsSpecification = [
          {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "200px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth:"120px"
              
          }, {
              type: 'select',
              bRegex: false,
              values: ['Available', 'Scrapped', 'Consigned', 'Sold', 'Rented']
          }, {
              type: 'select',
              bRegex: false,
              values: ['Bench', 'Butt Welders', 'Coil Joining Welders', 'Dial Indexer', 'Flash Butt', 'Micro', 'Miscellaneous', 'Portable Gun', 'Press Spot', 'Projections & Projection Spot Welders', 'Robot & Fixture Guns', 'Rocker Arm', 'Seam', 'Special Design', 'Three Phase'],
              sWidth:"500px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth:"500px"

          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "300px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "80px"
          }, {
              type: 'select',
              bRegex: false,
              values: ['120', '208', '220/230/240', '380', '440/460/480', 'Dual220/440', '550', '600'],
              sWidth: "80px"
          }, {
              type: 'select',
              bRegex: false,
              values: ['Single', 'Three'],
              sWidth: "80px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "200px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "120px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "120px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "120px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth:"80px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "80px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "80px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "80px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "300px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "80px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "80px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "300px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "80px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "80px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "80px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "280px"
          }, {
              type: 'text',
              bRegex: true,
              bSmart: true,
              sWidth: "150px"
          }, {
              type: 'checkbox',
              bRegex: true}
      ];

      $scope.dtOptions = DTOptionsBuilder.newOptions()
          .withBootstrap()
          .withOption('scrollX', '700%')
          .withOption('scrollY', height + 'px')
          .withOption('oLanguage', { "sEmptyTable": " " })
          .withOption('lengthMenu', [[-1, 1000, 100, 50, 25, 10], ['All', 1000, 100, 50, 25, 10]])
          .withOption('paging', false)
          .withOption('bInfo', false)
          .withOption('bAutoWidth',false)
          //.withOption('pageLength',100)
      .withColumnFilter({
          //sPlaceHolder:"head:after",
          aoColumns:columnsSpecification 
      })
      .withTableTools('/Content/DataTables/swf/copy_csv_xls.swf')
      .withTableToolsButtons(['xls']);

      angular.element($window).on('resize', function() {
          var tableHeight = $window.innerHeight - 260 - angular.element('.row-filter')[0].offsetHeight - 32;
          if (tableHeight <= 132) {
              tableHeight = 132;
          }
          $(".dataTables_wrapper").find(".dataTables_scrollBody").height(tableHeight);
          $(".dataTables_wrapper").find(".DTFC_LeftBodyWrapper").height(tableHeight);
         
      });

      angular.element($window).resize();

      $scope.$on('onRepeatLast', function (scope, element, attrs) {
          $scope.dtOptions
              .withFixedColumns({
                  leftColumns: 2
              });          
      });
  }
])

.filter('status', ['searchService', function (searchService) {
    var statuses = [];

    searchService.getSearchParams(function (res) {
        statuses = res.data.statuses;
    });

    return function (value) {
        var status = $.grep(statuses, function (s) { return s.value == value; })[0];
        return status ? status.label : value;
    };
}])

.filter('voltage', function () {
    
    return function (value,voltages) {
        if (voltages == null || voltages.length == 0) {

            return null;
        }

        var voltage = $.grep(voltages, function (v) { return v.value == value; })[0];
        return voltage ? voltage.label : value;
    };
})

.filter('phase', function () {
    
    return function (value,phases) {
        var phase = $.grep(phases, function (p) { return p.value == value; })[0];
        return phase ? phase.label : value;
    };
})

.filter('categories', function () {
    return function (value) {
        var names = value.map(function (category) {
            return category.name;
        });

        names.sort(function (a, b) {
            var aLower = a.toLowerCase();
            var bLower = b.toLowerCase();

            if (aLower < bLower) return -1;
            if (aLower > bLower) return 1;
            return 0;
        });

        return names.join(", ");
    };
})

.filter('data', function () {
    return function (value, scope) {
        return scope.filterData(value);
    };
})

.filter('boolToWord',function() {
    return function(value) {
        if (value) {
            return 'yes';
        } else {
            return 'no';
        }
    };
})

.filter('maxCharacters',function() {
    return function(value, length) {
        if (value) {
            if (value.length <= length) {
                return value;
            } else {
                return value.substr(0, length);
            }
        }
    };
})