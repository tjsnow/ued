﻿var mainModule = angular.module('TjSnow.Main', ['ngRoute', 'ng-breadcrumbs', 'ui.sortable'])
    .config(function ($routeProvider, $locationProvider,$compileProvider) {
    $routeProvider
        .when('/admin', {
            controller: 'MainController',
            templateUrl: '/Scripts/app/main/view.html',
            label: 'Dashboard'
        })
        .when('/admin/results', {
            controller: 'ResultsController',
            templateUrl: '/Scripts/app/views/results.html',
            label: 'Results'
        })
        .when('/admin/addmachine', {
            templateUrl: '/Scripts/app/views/machine/add.html',
            controller: 'MachineController',
            label: 'Add machine'
        })
        .when('/admin/editmachine/:machineId', {
            templateUrl: '/Scripts/app/views/machine/edit.html',
            controller: 'EditMachineController',
            label: 'Edit machine'
        })
        .when('/admin/logout', {
            redirectTo: function() {
                window.location = 'admin/logout';
            }
        });
        
    $locationProvider.html5Mode(true);

    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|pxxiurlhandler):/);
})

.controller('MainController', ['$scope', '$location', 'breadcrumbs', 'searchService', 'searchResultService','$rootScope','CommonService',
    function ($scope, $location, breadcrumbs, searchService, searchResultService,$rootScope,commonService) {
        $scope.breadcrumbs = breadcrumbs;

        commonService.setTitle(null);

        $scope.searchrequest = {
            take: 100,
            minkva: '',
            maxkva: ''
        };

        $scope.phases = [{ label: "All phases", value: null }];
        $scope.kvas = [{ label: "All sizes", value: null }];
        $scope.publishedtowebstatuses = [{ label: "All machines", value: null }];

        searchService.getSearchParams(
            function (data) {
                if (data.success) {
                    $scope.phases = data.data.phases;
                    $scope.voltages = data.data.voltages;
                    $scope.categories = data.data.categories;
                    $scope.statuses = data.data.statuses;
                    $scope.publishedtowebstatuses = data.data.publishedtowebstatuses;
                } else {
                    $scope.error = data.error;
                }
            },
            function (data) {
                $scope.error = data.message;
            }
        );

        $scope.inAction = false;

        $scope.doSearch = function () {
            $scope.error = null;
            $scope.inAction = true;

            $scope.searchrequest.orderby = "kva";
            
            searchService.search(
                $scope.searchrequest,
                function(result) {
                    searchResultService.setResult(
                        {
                            result: result,
                            request: $scope.searchrequest,
                            data: {
                                phases: $scope.phases,
                                voltages : $scope.voltages,
                                categories: $scope.categories,
                                statuses: $scope.statuses,
                                publishedtowebstatuses: $scope.publishedtowebstatuses
                            }
                        }
                    );

                    $scope.inAction = false;
                    $location.path('admin/results');
                },
                function (data) {
                    $scope.inAction = false;
                    $scope.error = data.message;
                }); 
        };
        
        $scope.clearAll = function ($event) {
            $event.preventDefault();

            $scope.searchrequest.keywords = null;
            $scope.searchrequest.includeSold = false,
            $scope.searchrequest.includeScrapped = false;

            $scope.searchrequest.minkva = '';
            $scope.searchrequest.maxkva = '';
            $scope.searchrequest.voltages = null;
            $scope.searchrequest.phase = null;
            $scope.searchrequest.categories = null;
            $scope.searchrequest.publishedtowebstatus = null;

            $scope.error = null;
        };
        
        // handle document level key-pressed
        $scope.$on('keypress:13', function (onEvent, keypressEvent) {
            $scope.doSearch();
        });
    }
]);