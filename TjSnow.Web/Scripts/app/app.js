﻿var app = angular.module('TjSnow', ['ngRoute', 'ng-breadcrumbs','ui.bootstrap',
        'TjSnow.Api',
        'TjSnow.Main',
        'TjSnow.Users',
        'TjSnow.Machines.GridView'
])

.config(function($sceDelegateProvider, $compileProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        '**youtube.com/**'
    ]);
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|http|mailto|tjsnowfilehandler|pxxiurlhandler):/);
});
/*
app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/Scripts/app/views/index.html',
            controller: 'DashboardController'
        })
        .when('/results', {
            templateUrl: '/Scripts/app/views/results.html',
            controller: 'ResultsController'
        })
        .when('/machine/:id', {
            templateUrl: '/Scripts/app/views/machine/view.html',
            controller: 'MachineController'
        })
        .when('/machine/:id/edit', {
            templateUrl: '/Scripts/app/views/machine/edit.html',
            controller: 'MachineController'
        })
        .when('/add', {
            templateUrl: '/Scripts/app/views/machine/add.html',
            controller: 'MachineController'
        })
        .otherwise({ redirectTo: '/' });
});*/