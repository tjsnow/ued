﻿mainModule.service('CommonService', ['$window', function ($window) {

    var self = this;

    self.setTitle = function(title) {

        if (title) {
            $window.document.title = title;
        } else {
            $window.document.title = 'Dashboard';
        }
    };

}]);