﻿angular.module('TjSnow.Api', ['TjSnow.Config'])
  .service('ApiService', ['$http', '$q', '$location', '$window', 'API_HOST', 'LOGIN_PAGE',
    function ($http, $q, $location, $window, apiHost, loginUrl) {
        var self = this;

        function urlFor(url) {
            return apiHost + url;
        }

        function toLoginPage() {
            var currentUrl = encodeURIComponent($window.location.href);
            $window.location.href = loginUrl + '?url=' + currentUrl;
        }

        this.get = function (url, params) {
            var defer = $q.defer();

            $http({
                method: 'GET',
                url: urlFor(url),
                withCredentials: true,
                params: params
            })
                .success(function (d) { defer.resolve(d); })
                .error(function (er, status) {
                    if (status == 401) toLoginPage();
                    else defer.reject(er);
                });

            return defer.promise;
        };

        this.post = function (url, data) {
            var defer = $q.defer();

            $http({
                method: 'POST',
                url: urlFor(url),
                withCredentials: true,
                data: data
            })
                .success(function (d) { defer.resolve(d); })
                .error(function (er, status) {
                    if (status == 401) toLoginPage();
                    else defer.reject(er);
                });

            return defer.promise;
        }

        this.put = function (url, data) {
            var defer = $q.defer();

            this.$http({
                method: 'PUT',
                url: urlFor(url),
                withCredentials: true,
                data: data
            })
                .success(function (d) { defer.resolve(d); })
                .error(function (er, status) {
                    if (status == 401) toLoginPage();
                    else defer.reject(er);
                });

            return defer.promise;
        }

        this.delete = function (url, params, data) {
            var defer = $q.defer();

            $http({
                method: 'DELETE',
                url: urlFor(url),
                withCredentials: true,
                params: params,
                data: data
            })
                .success(function (d) { defer.resolve(d); })
                .error(function (er, status) {
                    if (status == 401) toLoginPage();
                    else defer.reject(er);
                });

            return defer.promise;
        }
    }
  ]);