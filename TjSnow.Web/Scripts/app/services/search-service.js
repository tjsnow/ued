﻿mainModule.factory('searchService', ['$http', function($http) {
    return {
        getSearchParams: function(success, error) {
            $http.get('api/search/params').success(success).error(error);
        },
        search: function (searchRequest, success, error) {
            var httpRequest = {
                keywords: searchRequest.keywords,
                voltages: searchRequest.voltages,
                phase: searchRequest.phase,
                includeSold: searchRequest.includeSold,
                includeScrapped: searchRequest.includeScrapped,
                take: searchRequest.take,
                skip: searchRequest.skip,
                publishedtoweb: searchRequest.publishedtowebstatus,
                minkva: searchRequest.minkva != '' ? searchRequest.minkva : null,
                maxkva: searchRequest.maxkva != '' ? searchRequest.maxkva : null,
                orderby: searchRequest.orderby,
                isfastship: searchRequest.isfastship,
               // status:searchRequest.status
            };
            
            if (searchRequest.voltages) {
                httpRequest.voltages = searchRequest.voltages.map(function (elem) {
                    return elem.value;
                });
            } else {
                httpRequest.voltages = [];
            }

            if (searchRequest.categories) {
                httpRequest.categories = searchRequest.categories.map(function(elem) {
                    return elem.value;
                });
            } else {
                httpRequest.categories = [];
            }
	   //if(searchRequest.status != 1) {
		//return;
	   //}
//
            $http.get('api/search', { params: httpRequest }).success(success).error(error);
        }
    };
}]);

