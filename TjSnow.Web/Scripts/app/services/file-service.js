﻿mainModule.factory('fileService', function () {
    return {
        readMultiFiles: function(files, reader, destArray, singleFileReadCallback,readCompletedCallback) {
            var self = this;
            self.destArray = destArray;

            for (var i = 0; i < files.length; i++) {
                
                for (var j = 0; j < destArray.length; j++) {
                    
                    if (files[i].name == destArray[j].filename) {
                        
                        alert("Image with name '" + files[i].name + "' already exists.");

                        return;
                    }
                }
            }

            function readFile(index) {
                if (index >= files.length) {

                    if (readCompletedCallback) {
                        readCompletedCallback();
                    }

                    return;
                }

                var file = files[index];

                reader.onload = function(e) {
                    self.destArray.push({
                        filename: files[index].name,
                        file: files[index],
                        imageSrc: e.target.result,
                        ispublished:true
                    });

                    if (singleFileReadCallback) {
                        singleFileReadCallback();
                    }

                    readFile(index + 1);
                };

                reader.readAsDataURL(file);
            }

            readFile(0);
        }
    };
});