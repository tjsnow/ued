﻿app.service('SettingsService', ['$http', function($http) {

    var self = this;

    self.getTJSettings = function() {

        return $http.get('api/settings');
    };

}]);