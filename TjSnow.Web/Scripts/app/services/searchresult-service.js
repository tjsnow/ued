﻿mainModule.service('searchResultService', function () {
    var result = null;

    var setResult = function(newResult) {
        result = newResult;
    };

    var getResult = function() {
        return result;
    };

    return {
        setResult: setResult,
        getResult: getResult
    };
});