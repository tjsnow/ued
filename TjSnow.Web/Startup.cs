﻿using System.Web.Mvc;
using Autofac;
using Microsoft.Owin;
using Owin;
using TjSnow.Application.Services.Search;
using TjSnow.Infrastructure.Async;
using TjSnow.Infrastructure.Environment;
using TjSnow.Web.Infrastructure;

[assembly: OwinStartupAttribute(typeof(TjSnow.Web.Startup))]
namespace TjSnow.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ContainerInitializer.Initialize(app);

            ConfigureAuth(app);

            RebuildSearchIndex();
        }

        /// <summary>
        /// Rebuilds search index if necessary
        /// </summary>
        private static void RebuildSearchIndex()
        {
            var searchService = (ISearchService)DependencyResolver.Current.GetService(typeof(ISearchService));

            if (ConfigurationSettings.Get<bool>("SearchServiceRebuildIndex"))
            {
                searchService.Drop();
                AsyncHelper.RunSync(searchService.EnsureSearchConfigured);
            }
        }
    }
}
