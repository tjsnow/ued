﻿namespace TjSnow.Web.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Application.Exceptions;
    using Application.Services.Security;
    using Application.Services.User;
    using Persistence.Repositories.User;

    [Authorize]
    public class AdminController : Controller
    {
        private readonly ISecurityService _securityService;
        private readonly IUserService _userService; 

        public AdminController(
            ISecurityService securityService,
            IUserService userService)
        {
            _securityService = securityService;
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var user = await _userService.FindUserAsync(_securityService.CurrentUserId);

            if (user == null)
            {
                throw ExceptionHelper.CreateNotFoundException("User not found");
            }

            ViewBag.Username = string.Format("{0} {1}", user.FirstName, user.LastName);
            return View("Dashboard");
        }
    }
}
