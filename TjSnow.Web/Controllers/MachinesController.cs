﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TjSnow.Application.Services.Category;
using TjSnow.Application.Services.Email;
using TjSnow.Application.Services.Machine;
using TjSnow.DTO.Machine;
using TjSnow.DTO.Search;
using TjSnow.DTO.Status;
using TjSnow.Infrastructure.Utils;
using TjSnow.Model.Entities;
using TjSnow.Web.Models;
using TjSnow.Web.Models.Front.Request;

namespace TjSnow.Web.Controllers
{
    public class MachinesController : Controller
    {
        private const string RecentCookiesKey = "TJ.Recent";
        
        private readonly IMachinesService _machinesService;
        private readonly IEmailService _emailService;
        private readonly ICategoryService _categoryService;

        private void SetRecentCookie(int id)
        {
            var cookie = HttpContext.Request.Cookies[RecentCookiesKey];

            if (cookie == null)
            {
                HttpContext.Response.Cookies.Add(new HttpCookie(RecentCookiesKey,JsonConvert.SerializeObject(new List<int>{id})));
            }
            else
            {
                var cookieValue = cookie.Value;

                if (string.IsNullOrWhiteSpace(cookieValue))
                {
                    cookie.Value = JsonConvert.SerializeObject(new List<int> {id});

                    HttpContext.Response.Cookies.Set(cookie);
                }
                else
                {
                    var recentIds = JsonConvert.DeserializeObject<List<int>>(cookieValue);

                    if (!recentIds.Contains(id))
                    {
                        recentIds.Insert(0,id);

                        cookie.Value = JsonConvert.SerializeObject(recentIds);

                        HttpContext.Response.Cookies.Set(cookie);
                    }
                }
            }
        }

        private IEnumerable<int> GetRecentMachineIds()
        {
            var cookie = HttpContext.Request.Cookies[RecentCookiesKey];

            if (cookie == null || string.IsNullOrWhiteSpace(cookie.Value))
            {
                return Enumerable.Empty<int>();
            }

            return JsonConvert.DeserializeObject<List<int>>(cookie.Value);
        }

        private static IEnumerable<SearchParamPhaseDTO> GetSearchParamPhases()
        {
            var phases =
                ((Phase[])Enum.GetValues(typeof(Phase))).Select(
                    elem => new SearchParamPhaseDTO { Label = elem.GetEnumDescription(), Value = (int)elem }).ToList();
            phases.Insert(0, new SearchParamPhaseDTO { Label = "All phases", Value = null });

            return phases;
        }

        private static IEnumerable<SearchParamVoltageDTO> GetSearchParamVoltages()
        {
            var voltages =
                ((Voltage[])Enum.GetValues(typeof(Voltage))).Select(
                    elem => new SearchParamVoltageDTO { Label = elem.GetEnumDescription(), Value = (int)elem }).ToList();

            return voltages;
        }

        public MachinesController(IMachinesService machinesService, IEmailService emailService, ICategoryService categoryService)
        {
            _machinesService = machinesService;
            _emailService = emailService;
            _categoryService = categoryService;
        }

        //
        // GET: /Machines/
        public ActionResult Index(int? id)
        {
            MachineDTO machine = null;
            
            if (id.HasValue)
            {
                machine = _machinesService.GetMachineById(id.Value);
            }

            if (machine == null ||!machine.PublishedToWeb || machine.Status == StatusDTO.Sold || machine.Status == StatusDTO.Scrapped || machine.Status == StatusDTO.Rented)
            {
                SearchViewModel searchModel = new SearchViewModel();
                searchModel.searchResponse = new PublicSearchResponse();
                searchModel.categories = _categoryService.RetrieveCategories();
                searchModel.phases = GetSearchParamPhases();
                searchModel.voltages = GetSearchParamVoltages();
                
                return View("~/Views/Home/SearchNoResult.cshtml",searchModel);
            }

            IEnumerable<MachineDTO> relatedMachines = _machinesService.GetLastRelatedMachines(id.Value);

            IEnumerable<MachineDTO> resentMachines = _machinesService.GetMachineByIds(GetRecentMachineIds().ToArray());

            SetRecentCookie(id.Value);

            var model = new MachineDetailsViewModel
            {
                Machine = machine,
                RelatedMachines = relatedMachines,
                RecentMachines = resentMachines
            };

            ViewBag.Title = string.Format("{0} - {1} | TJ SNOW Resistance Welders", machine.StockNumber, machine.Name);
            ViewBag.MetaDescription = string.Format("TJ SNOW stock {0} - {1}. View the nation's largest resistance welding machine inventory online.",machine.StockNumber, machine.Name);

            return View(model);
        }


        public ActionResult ShareMachinePage(int machineId)
        {
            var model = new ShareMachinePageRequest
            {
                MachineId = machineId
            };

            return PartialView("ShareMachinePage",model);
        }

        [HttpPost]
        public async Task<ActionResult> ShareMachinePage(ShareMachinePageRequest request)
        {
            if (ModelState.IsValid)
            {
                var baseUrl = Request.Url.GetLeftPart(UriPartial.Authority);
                var machine = _machinesService.GetMachineById(request.MachineId);

                await _emailService.SharePage(request.PersonsName, request.Email, machine, request.AdditionalInformation,baseUrl);
            }

            return RedirectToAction("Index","Machines");
            
        }

        public ActionResult Print(int id)
        {
            MachineDTO machine = _machinesService.GetMachineById(id);

            var model = new MachineDetailsViewModel
            {
                Machine = machine,
                RecentMachines = Enumerable.Empty<MachineDTO>(),
                RelatedMachines = Enumerable.Empty<MachineDTO>()
            };

            ViewBag.Title = string.Format("{0} - {1} | TJ SNOW Resistance Welders", machine.StockNumber, machine.Name);
            ViewBag.MetaDescription = string.Format("TJ SNOW stock {0} - {1}. View the nation's largest resistance welding machine inventory online.", machine.StockNumber, machine.Name);

            return View("Print",model);
        }
	}
}