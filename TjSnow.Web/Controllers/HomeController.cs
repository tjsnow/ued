﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TjSnow.Application.Exceptions;
using TjSnow.Application.Services.Category;
using TjSnow.Application.Services.Search;
using TjSnow.DTO.Category;
using TjSnow.DTO.Search;
using TjSnow.Infrastructure.Logging.Base;
using TjSnow.Model.Entities;
using TjSnow.Web.Models;
using TjSnow.Infrastructure.Utils;
using TjSnow.DTO.Phase;
using TjSnow.DTO.Voltage;

namespace TjSnow.Web.Controllers
{
    public class HomeController : Controller
    {
        private const string HomePageTitle = "TJ SNOW - New and Used Resistance Welding Equipment Search";
        private const string HomePageDescription = "New and used spot welders, rebuilt resistance welders, and more. Search the nation's largest resistance welding machine inventory right now.";
        private const int FastShipCategoryId = -5;
        
        private readonly ISearchService _searchService;
        private readonly ICategoryService _categoryService;
        private static readonly ILogger Logger = LoggerFactory.CreateLog();
        private static IEnumerable<SearchParamPhaseDTO> GetSearchParamPhases()
        {
            var phases =
                ((Phase[])Enum.GetValues(typeof(Phase))).Select(
                    elem => new SearchParamPhaseDTO { Label = elem.GetEnumDescription(), Value = (int)elem }).ToList();
            phases.Insert(0, new SearchParamPhaseDTO { Label = "All phases", Value = null });

            return phases;
        }

        private static IEnumerable<SearchParamVoltageDTO> GetSearchParamVoltages()
        {
            var voltages =
                ((Voltage[])Enum.GetValues(typeof(Voltage))).Select(
                    elem => new SearchParamVoltageDTO { Label = elem.GetEnumDescription(), Value = (int)elem }).ToList();

            return voltages;
        }

        private SearchViewModel PrivateSearch(PublicSearchRequest request, bool isTable = false)
        {
            SearchViewModel model = new SearchViewModel();
            
            model.categories = _categoryService.RetrieveCategories();
            model.phases = GetSearchParamPhases();
            model.voltages = GetSearchParamVoltages();

            try
            {
                model.searchResponse = _searchService.PublicSearch(request);
            }
            catch (BadRequestException e)
            {
                model.searchResponse = null;
            }

            return model;
        } 

        public HomeController(ISearchService searchService,ICategoryService categoryService)
        {
            _searchService = searchService;
            _categoryService = categoryService;
        }

        public ActionResult Index()
        {
            
            FrontViewModel model = new FrontViewModel();
            var categories = _categoryService.RetrieveCategories().ToList();

            categories.Add(new CategoryShortDTO
            {
                Id = -5,
                Name = "Fast Ship"
            });

            model.categories = categories;

            ViewBag.Title = HomePageTitle;
            ViewBag.MetaDescription = HomePageDescription;

            return View(model);
        }

        public ActionResult Search(SearchFilter Filter)
        {
            PublicSearchRequest request = new PublicSearchRequest();
            request.Keywords = Filter.Keyword;
            request.Category = Filter.Id;
            request.FastShip = Filter.FastShip;
            request.Take = 100;
            request.Skip = 0;
            
            ViewBag.SearchFilter = Filter;
            
            SearchViewModel Model = PrivateSearch(request);

            if (Filter.Id.HasValue)
            {
                var category = Model.categories.FirstOrDefault(c => c.Id == Filter.Id.Value);

                if (category != null)
                {
                    ViewBag.Title = string.Format("{0} | TJ SNOW Resistance Welders", category.Name);
                    ViewBag.MetaDescription =string.Format("{0} resistance welding machines. TJ SNOW stocks new, used, and re-manufactured resistance welding machines. Search our inventory now.",category.Name);
                }
            }

            if (Model.searchResponse != null && Model.searchResponse.Machines.Count > 0)
            {
                return View("~/Views/Home/SearchResult.cshtml", Model);
            }
            return View("~/Views/Home/SearchNoResult.cshtml", Model);
        }

        public PartialViewResult SearchFilter(SearchFilter Filter)
        {
            PublicSearchRequest request = new PublicSearchRequest();
            request.Keywords = Filter.Keyword;
            request.Category = Filter.Id;
            request.Take = Filter.CountOnPage;
            request.MaxKva = Filter.MaxKva;
            request.MinKva = Filter.MinKva;
            request.FastShip = Filter.FastShip;
            
            if (Filter.Phase != null && Filter.Phase != 0)
            {
                request.Phase = (PhaseDTO)Filter.Phase;
            }
            if (Filter.Voltage != null && Filter.Voltage!=0)
            {
                request.Voltages.Add((VoltageDTO)Filter.Voltage);
            }
            request.Skip = (Filter.CurrentPage??0)*Filter.CountOnPage;
            ViewBag.SearchFilter = Filter;
            SearchViewModel Model = PrivateSearch(request);
            if (Model.searchResponse.Machines.Count > 0)
            {
                return PartialView("~/Views/Home/SearchResultPartial.cshtml", Model);
            }
            return PartialView("~/Views/Home/SearchFilterNoResult.cshtml", Model);
        }

        public PartialViewResult FeedbackForm()
        {
            return PartialView("~/Views/Home/FeedbackForm.cshtml");
        }
    }
}