﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TjSnow.Application.Services.Category;
using TjSnow.DTO.Category;
using TjSnow.Web.Models;

namespace TjSnow.Web.Controllers
{
    public class HeaderController : Controller
    {
        private const int FastShipCategoryId = -5;

        private readonly ICategoryService _categoryService;
        public HeaderController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        
        public PartialViewResult HeaderCategory()
        {
            FrontViewModel model = new FrontViewModel();
            var categories = _categoryService.RetrieveCategories().ToList();

            categories.Add(new CategoryShortDTO
            {
                Id = FastShipCategoryId,
                Name = "Fast Ship"
            });

            model.categories = categories;

            return PartialView("~/Views/Header/HeaderSelectCategory.cshtml", model);
        }
	}
}