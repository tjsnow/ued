﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TjSnow.DTO.Category;
using TjSnow.DTO.Search;

namespace TjSnow.Web.Models
{
    public class SearchFilter
    {
        public int CountOnPage { get; set; }
        public int Phase { get; set; }
        public int Voltage { get; set; }
        public int? Id { get; set; }
        public string Keyword{ get; set; }
        public bool IsTable { get; set; }
        public int? CurrentPage { get; set; }

        public double? MaxKva { get; set; }

        public double? MinKva { get; set; }

        public bool? FastShip { get; set; }
    }
}