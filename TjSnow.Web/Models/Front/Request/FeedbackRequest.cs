﻿namespace TjSnow.Web.Models.Api.Request
{
    using System.ComponentModel.DataAnnotations;
    using Resources;
    using TjSnow.DTO.Machine;

    public class FeedbackRequest
    {
        [MaxLength(64)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(100)]
        public string Company { get; set; }

        [Required]
        [MaxLength(100)]
        public string City { get; set; }

        [Required]
        [MaxLength(100)]
        public string State { get; set; }

        [Required]
        [EmailAddress]
        [MaxLength(64)]
        public string Email { get; set; }

        [MaxLength(64)]
        public string Phone { get; set; }

        
        [MaxLength(300)]
        public string Message { get; set; }

        [MaxLength(300)]
        public string MachineStockNumber { get; set; }

    }
}