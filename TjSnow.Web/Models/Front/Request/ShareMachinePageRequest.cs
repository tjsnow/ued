﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TjSnow.Web.Models.Front.Request
{
    public class ShareMachinePageRequest
    {
        [Required(ErrorMessage = "Required")]
        public string PersonsName { get; set; }

        [Required(ErrorMessage = "Required")]
        [EmailAddress(ErrorMessage = "Please, enter valid email")]
        public string Email { get; set; }

        public int MachineId { get; set; }

        public string AdditionalInformation { get; set; }
    }
}