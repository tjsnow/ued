﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TjSnow.DTO.Category;
using TjSnow.DTO.Machine;
using TjSnow.DTO.Search;

namespace TjSnow.Web.Models
{
    public class FrontViewModel
    {
        public IEnumerable<CategoryShortDTO> categories { get; set; }
    }

    public class SearchViewModel
    {
        public PublicSearchResponse searchResponse { get; set; } 
        public IEnumerable<SearchParamPhaseDTO> phases {get;set;}
        public IEnumerable<SearchParamVoltageDTO> voltages { get; set; }
        public IEnumerable<CategoryShortDTO> categories { get; set; }

    }

    public class MachineDetailsViewModel
    {
        public MachineDTO Machine { get; set; }

        public IEnumerable<MachineDTO> RelatedMachines { get; set; }

        public IEnumerable<MachineDTO> RecentMachines { get; set; } 
    }
}