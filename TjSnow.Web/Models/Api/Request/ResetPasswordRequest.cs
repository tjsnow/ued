﻿namespace TjSnow.Web.Models.Api.Request
{
    using System.ComponentModel.DataAnnotations;

    public class ResetPasswordRequest
    {
        [Required]
        public string Email { get; set; }
    }
}