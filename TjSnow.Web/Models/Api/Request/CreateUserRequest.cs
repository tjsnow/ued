﻿namespace TjSnow.Web.Models.Api.Request
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class CreateUserRequest
    {
        [Required]
        [MaxLength(32)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(32)]
        public string LastName { get; set; }

        [EmailAddress]
        [MaxLength(64)]
        public string Email { get; set; }

        [RegularExpression(RegExpHelper.Password)]
        public string Password { get; set; }

    }
}