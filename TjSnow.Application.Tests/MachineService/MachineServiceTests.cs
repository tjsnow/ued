﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TjSnow.Application.Exceptions;
using TjSnow.Application.Mappers.Base;
using TjSnow.Application.Mappers.Category;
using TjSnow.Application.Mappers.Condition;
using TjSnow.Application.Mappers.Importancy;
using TjSnow.Application.Mappers.Initiation;
using TjSnow.Application.Mappers.Machine;
using TjSnow.Application.Mappers.MachineImage;
using TjSnow.Application.Mappers.MachineVideo;
using TjSnow.Application.Mappers.Phase;
using TjSnow.Application.Mappers.Status;
using TjSnow.Application.Mappers.Voltage;
using TjSnow.Application.Services.Machine;
using TjSnow.DTO.Machine;
using TjSnow.DTO.MachineImage;
using TjSnow.DTO.Status;
using TjSnow.Infrastructure.Logging.Base;
using TjSnow.Infrastructure.Logging.NLog;
using TjSnow.Infrastructure.Messaging.Base;
using TjSnow.Model.Contracts.Events.MachineAgg;
using TjSnow.Model.Entities;
using TjSnow.Persistence.Repositories.Machine;
using TjSnow.Persistence.UnitOfWork.Base;

namespace TjSnow.Application.Tests.MachineService
{
    [TestClass]
    public class MachineServiceTests
    {
        private IObjectMapper _mapper;

        [ClassInitialize]
        public static void SetUp(TestContext context)
        {
            LoggerFactory.SetCurrent(new OnPremisesNLogFactory());
        }

        [ClassCleanup]
        public static void TearDown()
        {
            LoggerFactory.SetCurrent(null);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _mapper = new ObjectMapper();
            _mapper.RegisterConverter(new MachineToDtoConverter());
            _mapper.RegisterConverter(new MachineImageToDtoConverter());
            _mapper.RegisterConverter(new MachineVideoToDtoConverter());
            _mapper.RegisterConverter(new StatusToDtoConverter());
            _mapper.RegisterConverter(new CategoryToDtoConverter());
            _mapper.RegisterConverter(new ConditionToDtoConverter());
            _mapper.RegisterConverter(new ImportancyToDtoConverter());
            _mapper.RegisterConverter(new InitiationToDtoConverter());
            _mapper.RegisterConverter(new LocationToDtoConverter());
            _mapper.RegisterConverter(new PhaseToDtoConverter());
            _mapper.RegisterConverter(new VoltageToDtoConverter());
            _mapper.RegisterConverter(new NewMachineWithMediaDtoToEntityConverter());
            _mapper.RegisterConverter(new StatusDtoToEntityConverter());
        }
        
        [TestMethod]
        public void RetrieveMachines_Returns_Machines()
        {
            var machineRepositoryMock = new Mock<IMachineRepository>();
            machineRepositoryMock.Setup(x => x.RetrieveMachines()).Returns(new[]
            {
                new Machine {Status = Status.Available},
                new Machine {Status = Status.Customer}
            });

            var unitOfWorkMock = new Mock<IUnitOfWork>();

            unitOfWorkMock.Setup(x => x.MachineRepository).Returns(machineRepositoryMock.Object);

            var machineService = new MachinesService(unitOfWorkMock.Object, _mapper, null,null);

            var result = machineService.RetrieveMachines();

            Assert.IsNotNull(result);

            Assert.IsInstanceOfType(result,typeof(IEnumerable<MachineDTO>));


        }

        [TestMethod]
        [ExpectedException(typeof(NotFoundException))]
        public void RemoveMachine_MachineNotFound_ThrowsException()
        {
            var machineRepositoryMock = new Mock<IMachineRepository>();
            machineRepositoryMock.Setup(x => x.GetMachineById(It.IsAny<int>())).Returns((Machine) null);

            var unitOfWorkMock = new Mock<IUnitOfWork>();

            unitOfWorkMock.Setup(x => x.MachineRepository).Returns(machineRepositoryMock.Object);

            var machineService = new MachinesService(unitOfWorkMock.Object, _mapper, null,null);

            machineService.DeleteMachine(1);
        }

        [TestMethod]
        public void RemoveMachine_RemovesMachine()
        {
            var machine = new Machine(){Id = 1};

            int machineForDeleteId = 0;

            var machineRepositoryMock = new Mock<IMachineRepository>();

            machineRepositoryMock.Setup(x => x.GetMachineById(It.IsAny<int>())).Returns(machine);
            machineRepositoryMock.Setup(x => x.DeleteMachine(It.IsAny<int>())).Callback<int>(m => machineForDeleteId = m);

            var eventBusMock = new Mock<IEventBus>();

            eventBusMock.Setup(x => x.Publish(It.IsAny<MachineDeleted>()));

            var unitOfWorkMock = new Mock<IUnitOfWork>();

            unitOfWorkMock.Setup(x => x.MachineRepository).Returns(machineRepositoryMock.Object);
            unitOfWorkMock.Setup(x => x.Save());

            var machineService = new MachinesService(unitOfWorkMock.Object, _mapper, eventBusMock.Object,null);

            machineService.DeleteMachine(1);

            machineRepositoryMock.Verify(m=>m.DeleteMachine(It.IsAny<int>()),Times.Once);
            unitOfWorkMock.Verify(m=>m.Save(),Times.Once);

            Assert.AreEqual(1,machineForDeleteId);

        }

        [TestMethod]
        [ExpectedException(typeof(TJSApplicationException))]
        public void GetMachineById_NotValidId_ThrowsException()
        {
            var machineRepositoryMock = new Mock<IMachineRepository>();
            machineRepositoryMock.Setup(x => x.GetMachineById(It.IsAny<int>())).Returns((Machine) null);

            var unitOfWorkMock = new Mock<IUnitOfWork>();

            unitOfWorkMock.Setup(x => x.MachineRepository).Returns(machineRepositoryMock.Object);

            var machineService = new MachinesService(unitOfWorkMock.Object, _mapper, null,null);

            var result = machineService.GetMachineById(1);
        }

        [TestMethod]
        public void GetMachineById_ValidId_ReturnsMachine()
        {
            var machineRepositoryMock = new Mock<IMachineRepository>();
            machineRepositoryMock.Setup(x => x.GetMachineById(It.IsAny<int>())).Returns(new Machine { Status = Status.Available });

            var unitOfWorkMock = new Mock<IUnitOfWork>();

            unitOfWorkMock.Setup(x => x.MachineRepository).Returns(machineRepositoryMock.Object);

            var machineService = new MachinesService(unitOfWorkMock.Object, _mapper, null,null);

            var result = machineService.GetMachineById(1);

            Assert.IsNotNull(result);

            Assert.IsInstanceOfType(result, typeof(MachineDTO));
        }

        [TestMethod]
        public void CreateMachine_ReturnsCreatedMachine()
        {
            var machineRepositoryMock = new Mock<IMachineRepository>();

            machineRepositoryMock.Setup(x => x.InsertMachine(It.IsAny<Machine>())).Callback<Machine>(m => m.Id = 1);

            var eventBusMock = new Mock<IEventBus>();

            eventBusMock.Setup(x => x.Publish(It.IsAny<MachineCreated>()));

            var unitOfWorkMock = new Mock<IUnitOfWork>();

            unitOfWorkMock.Setup(x => x.MachineRepository).Returns(machineRepositoryMock.Object);
            unitOfWorkMock.Setup(x => x.Save());

            var machineService = new MachinesService(unitOfWorkMock.Object, _mapper, eventBusMock.Object,null);

            var result = machineService.CreateMachine(new NewMachineWithMediaDTO
            {
                Status = StatusDTO.Available
            });

            machineRepositoryMock.Verify(m => m.InsertMachine(It.IsAny<Machine>()), Times.Once);
            unitOfWorkMock.Verify(m => m.Save(), Times.Once);

            Assert.AreEqual(1,result.Id);
        }

        [TestMethod]
        [ExpectedException(typeof (NotFoundException))]
        public void UpdateMachine_MachineNotFound_ThrowsException()
        {
            var machineRepositoryMock = new Mock<IMachineRepository>();
            machineRepositoryMock.Setup(x => x.GetMachineById(It.IsAny<int>())).Returns((Machine)null);

            var unitOfWorkMock = new Mock<IUnitOfWork>();

            unitOfWorkMock.Setup(x => x.MachineRepository).Returns(machineRepositoryMock.Object);

            var machineService = new MachinesService(unitOfWorkMock.Object, _mapper, null,null);

            machineService.UpdateMachine(new MachineDTO());
        }

        [TestMethod]
        public void UpdateMachine()
        {
            var machineRepositoryMock = new Mock<IMachineRepository>();

            machineRepositoryMock.Setup(x => x.UpdateMachine(It.IsAny<Machine>())).Callback<Machine>(m => { });
            machineRepositoryMock.Setup(x => x.GetMachineById(It.IsAny<int>())).Returns(new Machine());

            var eventBusMock = new Mock<IEventBus>();

            eventBusMock.Setup(x => x.Publish(It.IsAny<MachineCreated>()));

            var unitOfWorkMock = new Mock<IUnitOfWork>();

            unitOfWorkMock.Setup(x => x.MachineRepository).Returns(machineRepositoryMock.Object);
            unitOfWorkMock.Setup(x => x.Save());

            var machineService = new MachinesService(unitOfWorkMock.Object, _mapper, eventBusMock.Object,null);

            machineService.UpdateMachine(new MachineDTO
            {
                Status = StatusDTO.Available
            });

            machineRepositoryMock.Verify(m => m.UpdateMachine(It.IsAny<Machine>()), Times.Once);
            unitOfWorkMock.Verify(m => m.Save(), Times.Once);
        }
    }
}
