﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TjSnow.Application.Exceptions;
using TjSnow.Application.Services.Machine;
using TjSnow.DTO.Machine;
using TjSnow.Web.WebServices;

namespace TjSnow.Web.Tests.Controllers
{
    [TestClass]
    public class MachineControllerTests
    {
        [TestMethod]
        public void GetMachines_Returns_Machines()
        {
            var machineServiceMock = new Mock<IMachinesService>();

            machineServiceMock.Setup(x => x.RetrieveMachines()).Returns(new List<MachineDTO>
            {
                new MachineDTO(),
                new MachineDTO()
            });

            var machinesController = new MachinesController(machineServiceMock.Object,null)
            {
                Request = new HttpRequestMessage()
                {
                    Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } }
                }
            };

            var result = machinesController.GetMachines();

            Assert.IsNotNull(result);

            Assert.IsInstanceOfType(result,typeof(IEnumerable<MachineDTO>));
        }

        [TestMethod]
        public void GetMachineById_Returns_Machine()
        {
            var machineServiceMock = new Mock<IMachinesService>();

            machineServiceMock.Setup(x => x.GetMachineById(It.IsAny<int>())).Returns(new MachineDTO());

            var machinesController = new MachinesController(machineServiceMock.Object, null)
            {
                Request = new HttpRequestMessage()
                {
                    Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } }
                }
            };

            var result = machinesController.GetMachineById(1);

            Assert.IsNotNull(result);

            Assert.IsInstanceOfType(result, typeof(MachineDTO));
        }

        [TestMethod]
        public void DeleteMachine()
        {
            var machineServiceMock = new Mock<IMachinesService>();

            machineServiceMock.Setup(x => x.DeleteMachine(It.IsAny<int>()));

            var machinesController = new MachinesController(machineServiceMock.Object, null)
            {
                Request = new HttpRequestMessage()
                {
                    Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } }
                }
            };

            machinesController.DeleteMachine(1);

            machineServiceMock.Verify(m=>m.DeleteMachine(It.IsAny<int>()),Times.Once);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task UpdateMachine_machineNotInRequest_ThrowsException()
        {
            var machineServiceMock = new Mock<IMachinesService>();

            machineServiceMock.Setup(x => x.UpdateMachine(It.IsAny<MachineDTO>()));
            
            var machinesController = new MachinesController(machineServiceMock.Object, null)
            {
                Request = new HttpRequestMessage()
                {
                    Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } },
                    Content = new MultipartContent()
                }
            };

            await machinesController.UpdateMachine(1);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task CreateMachine_machineNotInRequest_ThrowsException()
        {
            var machineServiceMock = new Mock<IMachinesService>();

            machineServiceMock.Setup(x => x.CreateMachine(It.IsAny<NewMachineWithMediaDTO>()));

            var machinesController = new MachinesController(machineServiceMock.Object, null)
            {
                Request = new HttpRequestMessage()
                {
                    Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } },
                    Content = new MultipartContent()
                }
            };

            await machinesController.CreateMachine();
        }
    }
}
