﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TjSnow.Application.Services.Search;
using TjSnow.Infrastructure.Logging.Base;
using TjSnow.Infrastructure.Logging.NLog;
using TjSnow.Web;
using TjSnow.Web.Controllers;

namespace TjSnow.Web.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [ClassInitialize]
        public static void SetUp(TestContext context)
        {
            LoggerFactory.SetCurrent(new OnPremisesNLogFactory());
        }

        [ClassCleanup]
        public static void TearDown()
        {
            LoggerFactory.SetCurrent(null);
        }

        //[TestMethod]
        //public void Index()
        //{
        //    // Arrange
        //    var searchServiceMock = new Mock<ISearchService>();

        //    HomeController controller = new HomeController(searchServiceMock.Object);

        //    // Act
        //    ViewResult result = controller.Index() as ViewResult;

        //    // Assert
        //    Assert.IsNotNull(result);
        //}

        //[TestMethod]
        //public void About()
        //{
        //    // Arrange
        //    var searchServiceMock = new Mock<ISearchService>();

        //    HomeController controller = new HomeController(searchServiceMock.Object);

        //    // Act
        //    ViewResult result = controller.About() as ViewResult;

        //    // Assert
        //    Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        //}

        //[TestMethod]
        //public void Contact()
        //{
        //    // Arrange
        //    HomeController controller = new HomeController();

        //    // Act
        //    ViewResult result = controller.Contact() as ViewResult;

        //    // Assert
        //    Assert.IsNotNull(result);
        //}
    }
}
