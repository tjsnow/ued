﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using TjSnow.Model.Entities;
using TjSnow.Model.Entities.UserAgg;
using TjSnow.Persistence.UnitOfWork.Base;

namespace TjSnow.Persistence
{
    public class EFDbContext : IdentityDbContext<User, Role, int, UserLogin, UserRole, UserClaim>, IDbContext
    {
        public EFDbContext()
            : base("DefaultConnection")
        {
        }

        public IDbSet<TEntity> DbSet<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Users

            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Role>().ToTable("Roles");
            modelBuilder.Entity<UserClaim>().ToTable("UserClaims");
            modelBuilder.Entity<UserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<UserRole>().ToTable("UserRoles");
            modelBuilder.Entity<UserProfile>().ToTable("UserProfiles");

            modelBuilder.Entity<User>()
                .HasRequired(p => p.Profile)
                .WithRequiredPrincipal();

            #endregion

            #region Category

            modelBuilder.Entity<Category>().ToTable("Categories").HasKey(x => x.Id);
            modelBuilder.Entity<Category>().Property(p => p.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Category>().Property(p => p.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(512);
            modelBuilder.Entity<Category>().Property(p => p.Voltage).HasColumnName("Voltage").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Phase).HasColumnName("Phase").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.KVA).HasColumnName("KVA").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.ThroatDepth).HasColumnName("ThroatDepth").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Manufacturer).HasColumnName("Manufacturer").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.SecAmps).HasColumnName("SecAmps").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.SecVolts).HasColumnName("SecVolts").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.AirType).HasColumnName("AirType").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.CylinderDiameter).HasColumnName("CylinderDiameter").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.MaxForce).HasColumnName("MaxForce").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.HolderDiameter).HasColumnName("HolderDiameter").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Control).HasColumnName("Control").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Service).HasColumnName("Service").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.PlatenUpper).HasColumnName("PlatenUpper").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.PlatenLower).HasColumnName("PlatenLower").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.TSlotSpacing).HasColumnName("TSlotSpacing").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.TapSwitch).HasColumnName("TapSwitch").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.CylinderStroke).HasColumnName("CylinderStroke").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.ArmDiaLower).HasColumnName("ArmDiaLower").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.ArmDiaUpper).HasColumnName("ArmDiaUpper").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.HeadType).HasColumnName("HeadType").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.UpperDrive).HasColumnName("UpperDrive").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.LowerDrive).HasColumnName("LowerDrive").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.WheelUpper).HasColumnName("WheelUpper").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.WheelLower).HasColumnName("WheelLower").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Model).HasColumnName("Model").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Serial).HasColumnName("Serial").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Initiation).HasColumnName("Initiation").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.FastShip).HasColumnName("FastShip").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Transformer).HasColumnName("Transformer").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.PurchasedDate).HasColumnName("PurchasedDate").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.EnteredDate).HasColumnName("EnteredDate").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.PurchasedFrom).HasColumnName("PurchasedFrom").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Condition).HasColumnName("Condition").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Cost).HasColumnName("Cost").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Freight).HasColumnName("Freight").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Feautures).HasColumnName("Feautures").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Location).HasColumnName("Location").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.CheckedBy).HasColumnName("CheckedBy").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Price).HasColumnName("Price").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.SoldTo).HasColumnName("SoldTo").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.DateSold).HasColumnName("DateSold").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.WebDescription).HasColumnName("WebDescription").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.UEDFilePath).HasColumnName("UEDFilePath").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Photo).HasColumnName("Photo").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.Video).HasColumnName("Video").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.StockNumber).HasColumnName("StockNumber").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.P21ItemId).HasColumnName("P21ItemId").IsRequired();
            modelBuilder.Entity<Category>().Property(p => p.CategoryImage).HasColumnName("CategoryImage").IsOptional().HasMaxLength(256);

            #endregion

            #region Machine

            modelBuilder.Entity<Machine>().ToTable("Machines").HasKey(x => x.Id);
            modelBuilder.Entity<Machine>().Property(p => p.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Machine>().Property(p => p.Name).HasColumnName("Name").IsRequired().HasMaxLength(512);
            modelBuilder.Entity<Machine>().Property(p => p.KVA).HasColumnName("KVA").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.LocationId).HasColumnName("LocationId").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.ThroatDepth).HasColumnName("ThroatDepth").IsOptional().HasMaxLength(64).IsUnicode(false);
            modelBuilder.Entity<Machine>().Property(p => p.Manufacturer).HasColumnName("Manufacturer").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.SecAmps).HasColumnName("SecAmps").IsOptional().HasMaxLength(1024);
            modelBuilder.Entity<Machine>().Property(p => p.SecVolts).HasColumnName("SecVolts").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.AirType).HasColumnName("AirType").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.CylinderDiameter).HasColumnName("CylinderDiameter").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.MaxForce).HasColumnName("MaxForce").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.HolderDiameter).HasColumnName("HolderDiameter").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.Control).HasColumnName("Control").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.Service).HasColumnName("Service").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.PlatenUpper).HasColumnName("PlatenUpper").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.PlatenLower).HasColumnName("PlatenLower").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.TSlotSpacing).HasColumnName("TSlotSpacing").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.TapSwitch).HasColumnName("TapSwitch").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.CylinderStroke).HasColumnName("CylinderStroke").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.ArmDiaLower).HasColumnName("ArmDiaLower").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.ArmDiaUpper).HasColumnName("ArmDiaUpper").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.HeadType).HasColumnName("HeadType").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.UpperDrive).HasColumnName("UpperDrive").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.LowerDrive).HasColumnName("LowerDrive").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.WheelUpper).HasColumnName("WheelUpper").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.WheelLower).HasColumnName("WheelLower").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.Model).HasColumnName("Model").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.Serial).HasColumnName("Serial").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.FastShip).HasColumnName("FastShip").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.Transformer).HasColumnName("Transformer").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.PurchasedDate).HasColumnName("PurchasedDate").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.PurchasedFrom).HasColumnName("PurchasedFrom").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.Cost).HasColumnName("Cost").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.Freight).HasColumnName("Freight").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.Feautures).HasColumnName("Feautures").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.CheckedBy).HasColumnName("CheckedBy").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.Price).HasColumnName("Price").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.SoldTo).HasColumnName("SoldTo").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.DateSold).HasColumnName("DateSold").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.UEDFilePath).HasColumnName("UEDFilePath").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.WebDescription).HasColumnName("WebDescription").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.EnteredDate).HasColumnName("EnteredDate").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            modelBuilder.Entity<Machine>().Property(p => p.StockNumber).HasColumnName("StockNumber").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.P21ItemId).HasColumnName("P21ItemId").IsOptional().HasMaxLength(64);
            modelBuilder.Entity<Machine>().Property(p => p.PublishedToWeb).HasColumnName("PublishedToWeb").IsRequired();
            modelBuilder.Entity<Machine>().Property(p => p.Status).HasColumnName("Status").IsRequired();
            modelBuilder.Entity<Machine>().Property(p => p.Condition).HasColumnName("Condition").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.Initiation).HasColumnName("Initiation").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.Phase).HasColumnName("Phase").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.Voltage).HasColumnName("Voltage").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.InternalNotes).HasColumnName("InternalNotes").IsOptional();
            modelBuilder.Entity<Machine>().Property(p => p.ExcludeFromReconciliation).HasColumnName("ExcludeFromReconciliation").IsOptional();
            
            modelBuilder.Entity<Machine>().HasOptional(a => a.Location).WithMany(b => b.Machines).HasForeignKey(c => c.LocationId);
            modelBuilder.Entity<Machine>().HasMany(a => a.Categories)
                .WithMany(b => b.Machines)
                .Map(cm =>
                {
                    cm.MapLeftKey("MachineId");
                    cm.MapRightKey("CategoryId");
                    cm.ToTable("MachineCategory");
                });

            #endregion

            #region MachineImage

            modelBuilder.Entity<MachineImage>().ToTable("MachineImages").HasKey(x => x.Id);
            modelBuilder.Entity<MachineImage>().Property(p => p.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<MachineImage>().Property(p => p.MachineId).HasColumnName("MachineId").IsRequired();
            modelBuilder.Entity<MachineImage>().Property(p => p.Url).IsRequired().HasColumnName("Url").HasMaxLength(128);
            modelBuilder.Entity<MachineImage>().Property(p => p.ContentType).HasColumnName("ContentType").IsRequired().HasMaxLength(64);
            modelBuilder.Entity<MachineImage>().Property(p => p.IsPublished).HasColumnName("IsPublished").IsRequired();
            modelBuilder.Entity<MachineImage>().Property(p => p.SortOrder).HasColumnName("SortOrder").IsRequired();
            modelBuilder.Entity<MachineImage>().Property(p => p.FileName).HasColumnName("FileName").IsRequired().HasMaxLength(128);

            modelBuilder.Entity<MachineImage>().HasRequired(a => a.Machine).WithMany(b => b.Images).HasForeignKey(c => c.MachineId);

            #endregion

            #region MachineVideo

            modelBuilder.Entity<MachineVideo>().ToTable("MachineVideos").HasKey(x => x.Id);
            modelBuilder.Entity<MachineVideo>().Property(p => p.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<MachineVideo>().Property(p => p.MachineId).HasColumnName("MachineId").IsRequired();
            modelBuilder.Entity<MachineVideo>().Property(p => p.Url).IsRequired().HasColumnName("Url").HasMaxLength(128);

            modelBuilder.Entity<MachineVideo>().HasRequired(a => a.Machine).WithMany(b => b.Videos).HasForeignKey(c => c.MachineId);

            #endregion
        }
    }
}