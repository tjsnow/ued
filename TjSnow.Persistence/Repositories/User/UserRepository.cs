﻿namespace TjSnow.Persistence.Repositories.User
{
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;
    using Identity;
    using Model.Entities.UserAgg;

    public class UserRepository : IUserRepository<User, int>
    {
        private readonly ApplicationUserManager _manager;

        public UserRepository(ApplicationUserManager manager)
        {
            _manager = manager;
        }

        public Task<IdentityResult> RegisterUserAsync(User user, string password)
        {
            return _manager.CreateAsync(user, password);
        }

        public Task<User> FindUserAsync(string username, string password)
        {
            return _manager.FindAsync(username, password);
        }

        public Task<User> FindUserAsync(int userId)
        {
            return _manager.FindByIdAsync(userId);
        }

        public Task<User> FindUserAsync(string username)
        {
            return _manager.FindByNameAsync(username);
        }

        public Task UpdateUserAsync(User user)
        {
            return _manager.UpdateAsync(user);
        }

        public Task<bool> IsEmailConfirmedAsync(int userId)
        {
            return _manager.IsEmailConfirmedAsync(userId);
        }

        public Task<string> GeneratePasswordResetTokenAsync(int userId)
        {
            return _manager.GeneratePasswordResetTokenAsync(userId);
        }

        public Task SendEmailAsync(int userId, string subject, string body)
        {
            return _manager.SendEmailAsync(userId, subject, body);
        }

        public Task<IdentityResult> ResetPasswordAsync(int userId, string code, string password)
        {
            return _manager.ResetPasswordAsync(userId, code, password);
        }

        public async Task<IdentityResult> SetPasswordAsync(int userId, string password)
        {
            await _manager.RemovePasswordAsync(userId);
            return await _manager.AddPasswordAsync(userId, password);
        }
    }
}
