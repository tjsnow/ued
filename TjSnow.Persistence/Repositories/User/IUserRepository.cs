﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace TjSnow.Persistence.Repositories.User
{
    public interface IUserRepository<TUser, TKey> where TUser : class
    {
        Task<IdentityResult> RegisterUserAsync(TUser user, string password);

        Task<TUser> FindUserAsync(string username, string password);

        Task<TUser> FindUserAsync(string username);

        Task<TUser> FindUserAsync(TKey userId);

        Task UpdateUserAsync(TUser user);

        Task<bool> IsEmailConfirmedAsync(TKey userId);

        Task<string> GeneratePasswordResetTokenAsync(TKey userId);

        Task SendEmailAsync(TKey userId, string subject, string body);

        Task<IdentityResult> ResetPasswordAsync(TKey userId, string code, string password);

        Task<IdentityResult> SetPasswordAsync(TKey userId, string password);
    }
}
