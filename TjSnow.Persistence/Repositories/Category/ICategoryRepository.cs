﻿using System.Collections.Generic;

namespace TjSnow.Persistence.Repositories.Category
{
    public interface ICategoryRepository
    {
        Model.Entities.Category GetById(int id);
        IEnumerable<Model.Entities.Category> GetAll();
    }
}