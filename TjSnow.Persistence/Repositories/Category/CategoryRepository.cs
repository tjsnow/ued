﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TjSnow.Model.Specifications;
using TjSnow.Persistence.Repositories.Base;
using TjSnow.Persistence.UnitOfWork.Base;

namespace TjSnow.Persistence.Repositories.Category
{
    public class CategoryRepository : BaseRepository<Model.Entities.Category>, ICategoryRepository
    {
        public CategoryRepository(IDbContext context) 
            : base(context)
        {
        }

        public Model.Entities.Category GetById(int id)
        {
            return AsQueryable().FirstOrDefault(CategorySpecifications.ById(id).SatisfiedBy());
        }

        public IEnumerable<Model.Entities.Category> GetAll()
        {
            return AsQueryable().AsNoTracking();
        }
    }
}