﻿using System.Collections.Generic;

namespace TjSnow.Persistence.Repositories.Machine
{
    public interface IMachineRepository
    {
        IEnumerable<Model.Entities.Machine> RetrieveMachines();

        void InsertMachine(Model.Entities.Machine machine);

        void DeleteMachine(int machineId);

        void UpdateMachine(Model.Entities.Machine machine);

        Model.Entities.Machine GetMachineById(int machineId);

        Model.Entities.Machine GetMachineByStockNumber(string stockNumber);

        IEnumerable<Model.Entities.Machine> GetMachinesByCategoryIds(int[] categoryIds);

        IEnumerable<Model.Entities.Machine> GetMachineByIds(int[] ids);
    }
}
