﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TjSnow.Model.Entities;
using TjSnow.Model.Specifications;
using TjSnow.Persistence.Repositories.Base;
using TjSnow.Persistence.UnitOfWork.Base;
using MachineEntity = TjSnow.Model.Entities.Machine;

namespace TjSnow.Persistence.Repositories.Machine
{
    public class MachineRepository : BaseRepository<MachineEntity>, IMachineRepository
    {
        private const int RelatedMachinesCount = 10;

        public MachineRepository(IDbContext context) 
            : base(context)
        {
        }

        public IEnumerable<MachineEntity> RetrieveMachines()
        {
            return AsQueryable().Include(m => m.Categories)
                .Include(m => m.Location)
                .Include(m => m.Images)
                .Include(m => m.Videos);
        }

        public void InsertMachine(MachineEntity machine)
        {
            Insert(machine);
        }

        public void DeleteMachine(int machineId)
        {
            DeleteById(machineId);
        }

        public void UpdateMachine(MachineEntity machine)
        {
            Update(machine);
        }

        public MachineEntity GetMachineById(int machineId)
        {
            return AsQueryable().Include(m => m.Categories)
                .Include(m => m.Location)
                .Include(m => m.Images)
                .Include(m => m.Videos)
                .FirstOrDefault(m => m.Id == machineId);
        }

        public MachineEntity GetMachineByStockNumber(string stockNumber)
        {
            return AsQueryable().FirstOrDefault(MachineSpecifications.ByStockNumber(stockNumber).SatisfiedBy());
        }

        public IEnumerable<MachineEntity> GetMachinesByCategoryIds(int[] categoryIds)
        {
            return  AsQueryable().Include(m => m.Categories)
                .Include(m => m.Location)
                .Include(m => m.Images)
                .Include(m => m.Videos)
                .Where(m => m.Categories.Select(category=>category.Id).Intersect(categoryIds).Any() && m.PublishedToWeb && m.Status!=Status.Sold && m.Status!=Status.Rented && m.Status!=Status.Scrapped)
                .OrderByDescending(m => m.Id)
                .Take(RelatedMachinesCount);
        }

        public IEnumerable<MachineEntity> GetMachineByIds(int[] ids)
        {
            return AsQueryable().Include(m => m.Categories)
                .Include(m => m.Location)
                .Include(m => m.Images)
                .Include(m => m.Videos)
                .Where(m => ids.Contains(m.Id));
        }
    }
}