﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TjSnow.Persistence.Repositories.Location
{
    public interface ILocationRepository
    {
        IEnumerable<Model.Entities.Location> RetrieveLocations();
    }
}
