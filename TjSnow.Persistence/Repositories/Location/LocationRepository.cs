﻿using System.Collections.Generic;
using TjSnow.Persistence.Repositories.Base;
using TjSnow.Persistence.UnitOfWork.Base;

namespace TjSnow.Persistence.Repositories.Location
{
    public class LocationRepository :BaseRepository<Model.Entities.Location>, ILocationRepository
    {
        public LocationRepository(IDbContext context) 
            : base(context)
        {
        }

        public IEnumerable<Model.Entities.Location> RetrieveLocations()
        {
            return AsQueryable();
        }
    }
}