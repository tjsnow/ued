﻿using System.Collections.Generic;
using TjSnow.Model.Specifications.Base;

namespace TjSnow.Persistence.Repositories.Base
{
    internal interface IReadRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> All(ISpecification<TEntity> specification = null);

        IEnumerable<TEntity> Find(params ISpecification<TEntity>[] specifications);

        TEntity FirstOrDefault(params ISpecification<TEntity>[] specifications);
    }
}
