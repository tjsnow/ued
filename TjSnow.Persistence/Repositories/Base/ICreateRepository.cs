﻿namespace TjSnow.Persistence.Repositories.Base
{
    internal interface ICreateRepository<TEntity> where TEntity : class
    {
        void Insert(TEntity entity);
    }
}
