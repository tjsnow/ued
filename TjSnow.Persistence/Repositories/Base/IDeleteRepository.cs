﻿namespace TjSnow.Persistence.Repositories.Base
{
    internal interface IDeleteRepository<TEntity> where TEntity : class
    {
        void DeleteById(object id);

        void Delete(TEntity entity);
    }
}
