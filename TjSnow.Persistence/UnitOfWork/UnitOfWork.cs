﻿using System;
using System.Net.NetworkInformation;
using TjSnow.Model.Entities;
using TjSnow.Persistence.Repositories.Base;
using TjSnow.Persistence.Repositories.Category;
using TjSnow.Persistence.Repositories.Machine;
using TjSnow.Persistence.UnitOfWork.Base;

namespace TjSnow.Persistence.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly IDbContext _context;

        #region Repositories
        
        private IMachineRepository _machineRepository;
        private ICategoryRepository _categoryRepository;
        private BaseRepository<Location> _locationRepository;
        private BaseRepository<MachineImage> _machineImageRepository;
        private BaseRepository<MachineVideo> _machineVideoRepository; 
        
        public IMachineRepository MachineRepository
        {
            get { return _machineRepository ?? (_machineRepository = new MachineRepository(_context)); }
        }
        
        public ICategoryRepository CategoryRepository 
        {
            get { return _categoryRepository ?? (_categoryRepository = new CategoryRepository(_context)); }
        }
        
        public BaseRepository<Location> LocationRepository 
        {
            get { return _locationRepository ?? (_locationRepository = new BaseRepository<Location>(_context)); }
        }

        public BaseRepository<MachineImage> MachineImageRepository 
        {
            get
            {
                return _machineImageRepository ?? (_machineImageRepository = new BaseRepository<MachineImage>(_context));
            }
        }

        public BaseRepository<MachineVideo> MachineVideoRepository
        {
            get
            {
                return _machineVideoRepository ?? (_machineVideoRepository = new BaseRepository<MachineVideo>(_context));
            }
        }

        #endregion

        public UnitOfWork(IDbContext context)
        {
            _context = context;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        #region Dispose

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
                _context.Dispose();

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}