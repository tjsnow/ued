﻿using TjSnow.Model.Entities;
using TjSnow.Persistence.Repositories.Base;
using TjSnow.Persistence.Repositories.Category;
using TjSnow.Persistence.Repositories.Machine;

namespace TjSnow.Persistence.UnitOfWork.Base
{
    public interface IUnitOfWork
    {
        void Save();

        #region Repositories

        IMachineRepository MachineRepository { get; }

        ICategoryRepository CategoryRepository { get; }

        BaseRepository<Location> LocationRepository { get; }

        BaseRepository<MachineImage> MachineImageRepository { get; }

        BaseRepository<MachineVideo> MachineVideoRepository { get; }

        #endregion
    }
}