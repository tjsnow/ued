﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace TjSnow.Persistence.UnitOfWork.Base
{
    public interface IDbContext
    {
        DbEntityEntry Entry(object entity);
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        IDbSet<TEntity> DbSet<TEntity>() where TEntity : class;
        int SaveChanges();
        void Dispose();
    }
}